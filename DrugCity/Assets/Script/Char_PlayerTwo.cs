﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Char_PlayerTwo : MonoBehaviour
{
    public List<Sprite> _Char;
    // Update is called once per frame
    void FixedUpdate()
    {
         if (GetComponent<SpriteRenderer>())
        {
            switch (PlayerScript.Instance._PlayerTwoClass)
            {
                case 0:
                    GetComponent<SpriteRenderer>().sprite = _Char[1];
                    this.transform.localPosition = new Vector3(this.transform.localPosition.x, 0f, this.transform.localPosition.z);
                    this.transform.localScale = new Vector3(0.2108241f, 0.2108241f, 0.2108241f);
                    break;
                case 1:
                    GetComponent<SpriteRenderer>().sprite = _Char[2];
                    this.transform.localPosition = new Vector3(this.transform.localPosition.x, 0.08f, this.transform.localPosition.z);
                    this.transform.localScale = new Vector3(0.1871359f, 0.1871359f, 0.1871359f);
                    break;
                case 2:
                    GetComponent<SpriteRenderer>().sprite = _Char[3];
                    this.transform.localPosition = new Vector3(this.transform.localPosition.x, 0f, this.transform.localPosition.z);
                    this.transform.localScale = new Vector3(0.2108241f, 0.2108241f, 0.2108241f);
                    break;
                default:
                    GetComponent<SpriteRenderer>().sprite = _Char[0];
                    this.transform.localPosition = new Vector3(this.transform.localPosition.x, 0f, this.transform.localPosition.z);
                    this.transform.localScale = new Vector3(0.2108241f, 0.2108241f, 0.2108241f);
                    break;
            }
        }else
        {
            switch (PlayerScript.Instance._PlayerTwoClass)
            {
                case 0:
                    GetComponent<Image>().sprite = _Char[1];
                    break;
                case 1:
                    GetComponent<Image>().sprite = _Char[2];
                    break;
                case 2:
                    GetComponent<Image>().sprite = _Char[3];
                    break;
                default:
                    GetComponent<Image>().sprite = _Char[0];
                    break;
            }
        }
        
    }
    
}
