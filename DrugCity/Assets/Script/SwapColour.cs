﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SwapColour : MonoBehaviour
{

    public Image _UI;
    public Color32 i;

    // Start is called before the first frame update
    void OnEnable()
    {
        //Invoke("_GetImage",0.1f);
    }

    public void _GetImage()
    {
        i = GetComponent<Image>().color;

        if (i.r == 245) // Orange
        {
            //Debug.Log("CHECK A" + GetComponent<Image>().color.r;
            _UI.sprite = Resources.Load<Sprite>("OrangeCard_Player");
        }
        else
        {
            //Debug.Log("CHECK B" + GetComponent<Image>().color.r + " : " + GetComponent<Image>().color.g + " : " + GetComponent<Image>().color.b);
            _UI.sprite = Resources.Load<Sprite>("BlueCard_Player");
        }
    }

    // Update is called once per frame
    void Update()
    {
        //GetComponent<Image>().color.r
    }
}
