﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class LogFilesScript : MonoBehaviour {

	// Use this for initialization

	void Start () {

	}

	public void _ResetLog(){
		PlayerPrefs.DeleteAll ();
	}
	
	// Update is called once per frame

	public bool _isStartCounting;
	float _PlayTimeCounting;
	void FixedUpdate () {
		if (_isStartCounting) {
			_PlayTimeCounting += Time.deltaTime;
		}

		if (_isStartCountPerTurn) {
			_TimePerTurn += Time.deltaTime;
		}
	}

	public bool _isFirstTurn;
	public void _StartCountMethod(){
			_isStartCountPerTurn = true;
	}

	public void _FirstTurnOnly(){
		if (!_isFirstTurn) {
			_isStartCountPerTurn = true;
			_isFirstTurn = true;
		}
	}

	public List<int> _CheckActionById;

	public int _NumberOfMajorGoal; //Check
	public int _NumberOfSubGoal; // Check
	public int _GameTurn; // Check
	public int _UseAbilty;
	//public int _PlayTime; //Check

	public float _Minutes;
	public float _Seconds;

	public int _ShareKnowLedge;
	public string _EndGameBy; // Check
	public string _GameResult;

	public List<float> _TimePerMove_P1;
	public List<float> _TimePerMove_P2;

	public float _TimePerTurn;
	public bool _isStartCountPerTurn;

	public void _AddTime(){
		_TimePerMove_P1.Add(_TimePerTurn);
		_TimePerTurn = 0;
		_isStartCountPerTurn = false;
	}

	public void _CalBeforeWrtie(){
		if (GetComponent<PlayerScript> ()._isCureType [1]) {
			_NumberOfSubGoal++;
		}

		if (GetComponent<PlayerScript> ()._isCureType [0]) {
			_NumberOfSubGoal++;
		}

		if (_NumberOfSubGoal == 2) {
			_NumberOfMajorGoal = 01; // Yes
		} else {
			_NumberOfMajorGoal = 00; // No
		}

		switch (_GameResult) {
		case "Win":
			_GameResult = 01.ToString("00");
			break;
		case "Lose":
			_GameResult = 02.ToString("00");
			break;
		case "Restart":
			_GameResult = 03.ToString("00");
			break;

		}

		_Minutes = Mathf.Floor(_PlayTimeCounting / 60);
		_Seconds = Mathf.RoundToInt(_PlayTimeCounting%60);

		if (!GetComponent<TutorialScript> ()._isOnTutorial && !GetComponent<Tutorial_Section02> ()._Section02) {
			_GameEndTime = System.DateTime.Now.ToString ("yyMMdd") + "_" + System.DateTime.Now.ToString ("hhmmss");
			WriteToLogFile ();
		}
	}

	public void _IncresseTurn(){
		if (!GetComponent<TutorialScript> ()._isOnTutorial && !GetComponent<Tutorial_Section02> ()._Section02) {
			_GameTurn++;

			foreach (int i in _CheckActionById) {
				if (i == 5) {
					_UseAbilty++;
				}

				if (i == 4) {
					_ShareKnowLedge++;
				}
			}

			_CheckActionById.Clear ();
		}
	}

	public void _MedicSkill(){
		_CheckActionById.Add (5);
	}

	string _mode;
	bool _OnWriteLog;
	string _GameStartTime;
	string _GameEndTime;
	public void _GetMode(string i){
		_mode = i;
		_GameStartTime = System.DateTime.Now.ToString ("yyMMdd") + "_" + System.DateTime.Now.ToString ("hhmmss");
		_OnWriteLog = true;
	}

		
	public int _Experiment_Number;
	public void WriteToLogFile(){

        //string LogName = "GameLog - "+ _mode+ " - "+ _GameResult +" ["+ System.DateTime.Now.Day+"-"+System.DateTime.Now.Month+"-"+System.DateTime.Now.Year+"] - ["+System.DateTime.Now.ToString("hh-mm") +"]" ; // Change to data & Time

        string LogName = _GameStartTime;

		Debug.Log (LogName);
		string Path = Application.persistentDataPath +"/"+LogName+".txt";
		Debug.Log ("CHECK");
		if (!File.Exists (Path)) {
			File.WriteAllText (Path, "");
		}

		_Experiment_Number = PlayerPrefs.GetInt ("_Experiment_Number", 0);

		int _Count=0;
		string x="";
		float minutes=0;
		float seconds=0;

		for (int i = 0; i < _GameTurn; i++) {
			_Count++;
			if (i == 0) {
				minutes = Mathf.Floor (_TimePerMove_P1 [i] / 60);
				seconds = Mathf.RoundToInt (_TimePerMove_P1 [i] % 60);
				x += _Count.ToString("00")+"^"+minutes.ToString("00")+":"+seconds.ToString("00"); // 01^1:33

				if ((i+1) ==_TimePerMove_P1.Count) {
					x += "^"+"00:00;"; //^02:30
					break;
				}
				minutes = Mathf.Floor(_TimePerMove_P1[i+1] / 60);
				seconds = Mathf.RoundToInt(_TimePerMove_P1[i+1]%60);
				x += "^"+minutes.ToString("00")+":"+seconds.ToString("00")+";"; //^02:30
			} else {

				if (i + _Count > _GameTurn) {
					break;
				}

				minutes = Mathf.Floor (_TimePerMove_P1 [i+_Count-1] / 60);
				seconds = Mathf.RoundToInt (_TimePerMove_P1 [i + _Count - 1] % 60);
				x += _Count.ToString("00")+"^"+minutes.ToString("00")+":"+seconds.ToString("00"); // 01^1:33


				Debug.Log (i+_Count);
				if ((i+_Count) ==_TimePerMove_P1.Count) {
					x += "^"+"00:00;"; //^02:30
					break;
				}
				minutes = Mathf.Floor(_TimePerMove_P1[i+_Count] / 60);
				seconds = Mathf.RoundToInt(_TimePerMove_P1[i+_Count]%60);
				x += "^"+minutes.ToString("00")+":"+seconds.ToString("00")+";"; //^02:30


			}
		}

		string Content = "<head>" + _Experiment_Number.ToString ("000") + ";" + _mode + ";" + _GameStartTime + "<playgame>" + x + "\n"
			+ "<head>" + _Experiment_Number.ToString ("000") + ";" + _mode + ";" + _GameStartTime + ";" + _GameEndTime + "<endgame>" + _GameResult + ";" + _GameTurn + ";" + _NumberOfMajorGoal.ToString ("00") + ";" + _NumberOfSubGoal + ";" + _UseAbilty + ";" + _ShareKnowLedge;


		File.AppendAllText (Path, Content);

		PlayerPrefs.SetInt ("_Experiment_Number", _Experiment_Number+1);
	}

	void OnApplicationQuit(){
		if (_OnWriteLog) {
			GetComponent<LogFilesScript> ()._GameResult = "Restart";
			GetComponent<LogFilesScript> ()._EndGameBy = "Restart";
			GetComponent<LogFilesScript> ()._isStartCounting = false;
			GetComponent<LogFilesScript> ()._CalBeforeWrtie ();
		}
	}
}
