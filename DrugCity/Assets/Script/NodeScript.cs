﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NodeScript : MonoBehaviour {

	public GameObject[] _ConnectNode;
	public GameObject[] _Cube;

    public GameObject _CubeStore;
    public Sprite _Locatce;
    public Sprite _CureBlue;
    public Sprite _CureOrange;

    public int _Type;
	public int _Node_Id;
	public int _Level;

	public Color[] _NodeColor;

	public GameObject[] _CubePrefab;

	public Material[] _Mat;

    public bool _isBuild;

    public string _NodeName;

	//public int _ById;

	public void _CheckById(){


		
		if (_Level < 0) {
			PlayerScript.Instance._BlueTreat.GetComponent<Button> ().interactable = false;
			PlayerScript.Instance._RedTreat.GetComponent<Button> ().interactable = false;
			return;
		}

		PlayerScript.Instance._BlueTreat.GetComponent<Button> ().interactable = false;
		PlayerScript.Instance._RedTreat.GetComponent<Button> ().interactable = false;

		foreach (GameObject i  in _Cube) {
			
			if (i.activeSelf) {
				//Debug.Log ("F");
				if (i.GetComponent<MeshRenderer> ().material.name == "BlueBox (Instance)"||i.GetComponent<MeshRenderer> ().material.name == "BlueBox") {
					//Debug.Log ("A");
					PlayerScript.Instance._BlueTreat.GetComponent<Button> ().interactable = true;
				} else {
					PlayerScript.Instance._BlueTreat.GetComponent<Button> ().interactable = false;
				}


				if (i.GetComponent<MeshRenderer> ().material.name == "OrBox"||i.GetComponent<MeshRenderer> ().material.name == "OrBox (Instance)") {
					//Debug.Log ("B");
					PlayerScript.Instance._RedTreat.GetComponent<Button> ().interactable = true;
				} else {
					PlayerScript.Instance._RedTreat.GetComponent<Button> ().interactable = false;
				}
			}
		}


	}

    public void _NameUpdate(string i){
		_NodeName = i;
        this.gameObject.transform.GetChild(3).gameObject.GetComponent<TextMesh>().text = i;
    }

    public Sprite _RedBuilding;
    public Sprite _BlueBuilding;

    public void FixedUpdate()
    {
        if (_isBuild && PlayerScript.Instance._isCureType[0]&&_Type==0)
        {
            _CubeStore.transform.GetChild(3).transform.GetComponent<SpriteRenderer>().sprite = _BlueBuilding;
        }
        else if (_isBuild && PlayerScript.Instance._isCureType[1] && _Type == 1)
        {
            _CubeStore.transform.GetChild(3).transform.GetComponent<SpriteRenderer>().sprite = _RedBuilding;
        }
        else
        {
            if (_isBuild)
            {
                _CubeStore.transform.GetChild(3).transform.GetComponent<SpriteRenderer>().sprite = _Locatce;
            }
            else
            {
                _CubeStore.transform.GetChild(3).transform.GetComponent<SpriteRenderer>().sprite = null;
            }
        }
    }

    // Use this for initialization
    void Start() {

        foreach (Transform i in _CubeStore.transform)
        {
            i.gameObject.GetComponent<SpriteRenderer>().sprite = null;
        }

        if (_isBuild)
        {
            _CubeStore.transform.GetChild(3).transform.GetComponent<SpriteRenderer>().sprite = _Locatce;
        }

        //_CubeStore = this.gameObject.transform.GetChild(4).gameObject;

		foreach (GameObject i in _Cube) {
			i.SetActive (false);
		}

		//GetComponent<SpriteRenderer> ().color = _NodeColor [_Type];
	}

	public void _ResetCode(){
		GetComponent<SpriteRenderer> ().color = _NodeColor [_Type];
	}

    public void _LevelUp(int _Count, int _Color) {
		
		if (_isOutBreak) {
			return;
		}

        _Level += _Count;

		if (_Level >= 4) {
	


			GetComponent<SpriteRenderer> ().color = _NodeColor [2];
			GameObject x = Instantiate (_CubePrefab [_Type], _CubePrefab [_Type].transform.position, Quaternion.identity);
			x.GetComponent<CubeEffect> ()._Obj = this.gameObject;

			CardManager.InfectionEvent _Event = new CardManager.InfectionEvent();
			_Event._InfectionLevel = 1;
			_Event._Node = this.gameObject;
			_Event._isOutBreak = true;
			PlayerScript.Instance.GetComponent<CardManager> ()._InfectionStore.Add (_Event);
           // PlayerScript.Instance._DeInfection(_Type);
			_Level=3;
            //_isFromOutBreak = false;
		} else {
			_ResetCode ();
			GameObject x = Instantiate (_CubePrefab [_Type], _CubePrefab [_Type].transform.position, Quaternion.identity);
			x.GetComponent<CubeEffect> ()._Obj = this.gameObject;
			PlayerScript.Instance._DeInfection(_Type,_Count);
			//Debug.Log (this.gameObject.name);
			//_InfectionOnly(_Color,_isEpidemic);
			for (int i = 0; i < _Level; i++) {
				if (!_Cube [i].activeSelf) {
					_Cube [i].GetComponent<MeshRenderer> ().material = _Mat [_Color];
                    if (_Color == 0)
                    {
                        _CubeStore.transform.GetChild(i).transform.GetComponent<SpriteRenderer>().enabled = true;
                        _CubeStore.transform.GetChild(i).transform.GetComponent<SpriteRenderer>().sprite = _CureBlue;
                    }
                    else
                    {
                        _CubeStore.transform.GetChild(i).transform.GetComponent<SpriteRenderer>().enabled = true;
                        _CubeStore.transform.GetChild(i).transform.GetComponent<SpriteRenderer>().sprite = _CureOrange;
                    }
                    
					_InfectionTypePerCube [i] = _Color;
					//_Cube [i].SetActive (true);
				}
			}

			Invoke ("_RefreshCube",1.2f);
		}

	}

	private void _RefreshCube(){
		for (int i = 0; i < _Level; i++) {
			_Cube [i].SetActive (true);
		}
	}

	public int[] _InfectionTypePerCube;

	public void _InfectionForSetup(int _Count,int _Color){
		_Level+=_Count;
		for (int i = 0; i < _Level; i++) {
			_Cube [i].GetComponent<MeshRenderer> ().material = _Mat[_Color];
			_InfectionTypePerCube [i] = _Color;
			_Cube [i].SetActive (true);

            if (_Color == 0)
            {
                _CubeStore.transform.GetChild(i).transform.GetComponent<SpriteRenderer>().enabled = true;
                _CubeStore.transform.GetChild(i).transform.GetComponent<SpriteRenderer>().sprite = _CureBlue;
            }
            else
            {
                _CubeStore.transform.GetChild(i).transform.GetComponent<SpriteRenderer>().enabled = true;
                _CubeStore.transform.GetChild(i).transform.GetComponent<SpriteRenderer>().sprite = _CureOrange;
            }
        }
	}

	public void _InfectionOnly(){
		for (int i = 0; i < _Level; i++) {
			if (!_Cube [i].activeSelf) {
				_Cube [i].GetComponent<MeshRenderer> ().material = _Mat [_InfectionTypePerCube [i]];
				_Cube [i].SetActive (true);

                if (_InfectionTypePerCube[i] == 0)
                {
                    _CubeStore.transform.GetChild(i).transform.GetComponent<SpriteRenderer>().enabled = true;
                    _CubeStore.transform.GetChild(i).transform.GetComponent<SpriteRenderer>().sprite = _CureBlue;
                }
                else
                {
                    _CubeStore.transform.GetChild(i).transform.GetComponent<SpriteRenderer>().enabled = true;
                    _CubeStore.transform.GetChild(i).transform.GetComponent<SpriteRenderer>().sprite = _CureOrange;
                }
            }
		}
	}
	int x;
	GameObject _CubeSelected;
	public void _Relocation(){
		for (int i = 0; i < 3; i++) {

			if (_Cube [i].activeSelf) {
				_CubeSelected = _Cube [i];
				x++;
			}
			
			switch (x) {
			case 1: 
				_CubeSelected.transform.localPosition = new Vector3 (-0.14f, -0.26f, 0);
				break;
			case 2: 
				_CubeSelected.transform.localPosition = new Vector3 (0f, -0.26f, 0);
				break;
			case 3: 
				_CubeSelected.transform.localPosition = new Vector3 (0.14f, -0.26f, 0);
				break;
			}
		}
	}

	public bool _isFromOutBreak;
	public bool _isOutBreak;
	public void _OutBreak(){

		if (_isOutBreak) {
			return;
		}

		_isOutBreak = true;

			GetComponentInParent<NodeManager> ()._OutBreak++;
			//_isFromOutBreak = true;
			foreach (GameObject i in _ConnectNode) {
				i.GetComponent<NodeScript> ()._LevelUp (1,_Type);
              
			}
		PlayerScript.Instance.GetComponent<CardManager> ()._OutBreakAndInfectionTap ();
			if (GetComponentInParent<NodeManager> ()._OutBreak >= 4) {
				Debug.Log ("Lose By OutBreak");
                
                PlayerScript.Instance.GetComponent<CardManager> ()._EndGame (false, "By OutBreak");
			}
			_Level = 3;
			_ResetCode ();
			
        PlayerScript.Instance.GetComponent<CardManager>()._OutBreakAndInfectionTap();
        Debug.Log ("OutBreak");
	}

    public int _TreatCount;
    public bool _isCured;
	public int _CountA;
	public int _CountB;
	public bool _isTreat;
    public void _Treat(bool _isMedic){

		_TreatCount = 0;
		_isTreat = false;
		_CountA = 0;
		_CountB = 0;

		if (PlayerScript.Instance._isCureType [0] ) {

			if (_Cube [0].activeSelf) {
				if (_InfectionTypePerCube [0] == 0) {
					_Cube [0].SetActive (false);
					PlayerScript.Instance._InInfection (_Type, 1);
					_TreatCount++;
					_isCured = true;
					_isTreat = true;
                    _CubeStore.transform.GetChild(0).transform.GetComponent<SpriteRenderer>().enabled=false;
                }
			}


			if (_Cube [1].activeSelf) {
				if (_InfectionTypePerCube [1] == 0) {
					_Cube [1].SetActive (false);
					PlayerScript.Instance._InInfection (_Type, 1);
					_TreatCount++;
					_isCured = true;
					_isTreat = true;
                    _CubeStore.transform.GetChild(1).transform.GetComponent<SpriteRenderer>().enabled = false;
                }
			}

			if (_Cube [2].activeSelf) {
				if (_InfectionTypePerCube [2] == 0) {
					_Cube [2].SetActive (false);
					PlayerScript.Instance._InInfection (_Type, 1);
					_TreatCount++;
					_isCured = true;
					_isTreat = true;
                    _CubeStore.transform.GetChild(2).transform.GetComponent<SpriteRenderer>().enabled = false;
                }
			}

			if (_isTreat) {
				_Level -= _TreatCount;
			} else {

				if (_isMedic) {

					Debug.Log ("A");


					if (_Cube [0].activeSelf) {
						if (_InfectionTypePerCube [0] == 0) {
							_CountA++; // 
						} else {
							_CountB++;
						}
					}

					if (_Cube [1].activeSelf) {
						if (_InfectionTypePerCube [1] == 0) {
							_CountA++; // 
						} else {
							_CountB++;
						}
					}

					if (_Cube [2].activeSelf) {
						if (_InfectionTypePerCube [2] == 0) {
							_CountA++; // 
						} else {
							_CountB++;
						}
					}



					Debug.Log (_CountA);
					Debug.Log (_CountB);

					if (_CountA >= _CountB) {

						Debug.Log ("B");

						if (_Cube [0].activeSelf) {
							Debug.Log ("B1");
							if (_InfectionTypePerCube [0] == 0) {
								Debug.Log ("B1.1");
								_Cube [0].SetActive (false);
								PlayerScript.Instance._InInfection (_Type, 1);
                                _CubeStore.transform.GetChild(0).transform.GetComponent<SpriteRenderer>().enabled=false;
							}
						}


						if (_Cube [1].activeSelf) {
							Debug.Log ("B2");
							if (_InfectionTypePerCube [1] == 0) {
								Debug.Log ("B2.2");
								_Cube [1].SetActive (false);
								PlayerScript.Instance._InInfection (_Type, 1);
                                _CubeStore.transform.GetChild(1).transform.GetComponent<SpriteRenderer>().enabled = false;
                                _TreatCount++;
							}
						}

						if (_Cube [2].activeSelf) {
							Debug.Log ("B3");
							if (_InfectionTypePerCube [2] == 0) {
								Debug.Log ("B3.3");
								_Cube [2].SetActive (false);
								PlayerScript.Instance._InInfection (_Type, 1);
                                _CubeStore.transform.GetChild(2).transform.GetComponent<SpriteRenderer>().enabled = false;
                                _TreatCount++;
							}
						}


						_Level -= _TreatCount;
					} else {

						Debug.Log ("C");

						if (_Cube [0].activeSelf) {
							if (_InfectionTypePerCube [0] == 1) {
								_Cube [0].SetActive (false);
								PlayerScript.Instance._InInfection (_Type, 1);
                                _CubeStore.transform.GetChild(0).transform.GetComponent<SpriteRenderer>().enabled = false;
                                _TreatCount++;
							}
						}

						if (_Cube [1].activeSelf) {

							if (_InfectionTypePerCube [1] == 1) {
								_Cube [1].SetActive (false);
								PlayerScript.Instance._InInfection (_Type, 1);
                                _CubeStore.transform.GetChild(1).transform.GetComponent<SpriteRenderer>().enabled = false;
                                _TreatCount++;
							}
						}

						if (_Cube [2].activeSelf) {

							if (_InfectionTypePerCube [2] == 1) {
								_Cube [2].SetActive (false);
								PlayerScript.Instance._InInfection (_Type, 1);
                                _CubeStore.transform.GetChild(2).transform.GetComponent<SpriteRenderer>().enabled = false;
                                _TreatCount++;
							}
						}


						_Level -= _TreatCount;
					}

				} else {
					PlayerScript.Instance._InInfection (_Type, 1);
					_Level--;

					//_LevelUp (-1, _Type);
					switch (_Level) {
					case 0: // 0 Cube
                            _Cube[0].SetActive(false);
                            _Cube[1].SetActive(false);
                            _Cube[2].SetActive(false);
                            _CubeStore.transform.GetChild(0).transform.GetComponent<SpriteRenderer>().enabled = false;
                            _CubeStore.transform.GetChild(1).transform.GetComponent<SpriteRenderer>().enabled = false;
                            _CubeStore.transform.GetChild(2).transform.GetComponent<SpriteRenderer>().enabled = false;
                            break;
					case 1:// 1 Cube
						_Cube [0].SetActive (true);
						_Cube [1].SetActive (false);
						_Cube [2].SetActive (false);
                            _CubeStore.transform.GetChild(0).transform.GetComponent<SpriteRenderer>().enabled = true;
                            _CubeStore.transform.GetChild(1).transform.GetComponent<SpriteRenderer>().enabled = false;
                            _CubeStore.transform.GetChild(2).transform.GetComponent<SpriteRenderer>().enabled = false;
                            break;
					case 2: // 2 Cube
						_Cube [0].SetActive (true);
						_Cube [1].SetActive (true);
						_Cube [2].SetActive (false);
                            _CubeStore.transform.GetChild(0).transform.GetComponent<SpriteRenderer>().enabled = true;
                            _CubeStore.transform.GetChild(1).transform.GetComponent<SpriteRenderer>().enabled = true;
                            _CubeStore.transform.GetChild(2).transform.GetComponent<SpriteRenderer>().enabled = false;
                            break;
					}
				}
			}



			//_Relocation ();

		}else if(PlayerScript.Instance._isCureType [1]){

			if (_Cube[0].activeSelf)
			{
				if (_InfectionTypePerCube[0] == 1)
				{
					_Cube[0].SetActive(false);
					PlayerScript.Instance._InInfection(_Type, 1);
					_TreatCount++;
					_isCured = true;
					_isTreat = true;
                    _CubeStore.transform.GetChild(0).transform.GetComponent<SpriteRenderer>().enabled = false;
                }
			}

			if (_Cube[1].activeSelf)
			{

				if (_InfectionTypePerCube[1] == 1)
				{
					_Cube[1].SetActive(false);
					PlayerScript.Instance._InInfection(_Type, 1);
					_TreatCount++;
					_isCured = true;
					_isTreat = true;
                    _CubeStore.transform.GetChild(1).transform.GetComponent<SpriteRenderer>().enabled = false;
                }
			}

			if (_Cube[2].activeSelf)
			{

				if (_InfectionTypePerCube[2] == 1)
				{
					_Cube[2].SetActive(false);
					PlayerScript.Instance._InInfection(_Type, 1);
					_TreatCount++;
					_isCured = true;
					_isTreat = true;
                    _CubeStore.transform.GetChild(2).transform.GetComponent<SpriteRenderer>().enabled = false;
                }
			}


			if (_isTreat) {
				_Level -= _TreatCount;
			} else {
				if (_isMedic) {

					Debug.Log ("A");


					if (_Cube [0].activeSelf) {
						if (_InfectionTypePerCube [0] == 0) {
							_CountA++; // 
						} else {
							_CountB++;
						}
					}

					if (_Cube [1].activeSelf) {
						if (_InfectionTypePerCube [1] == 0) {
							_CountA++; // 
						} else {
							_CountB++;
						}
					}

					if (_Cube [2].activeSelf) {
						if (_InfectionTypePerCube [2] == 0) {
							_CountA++; // 
						} else {
							_CountB++;
						}
					}



					Debug.Log (_CountA);
					Debug.Log (_CountB);

					if (_CountA >= _CountB) {

						Debug.Log ("B");

						if (_Cube [0].activeSelf) {
							Debug.Log ("B1");
							if (_InfectionTypePerCube [0] == 0) {
								Debug.Log ("B1.1");
								_Cube [0].SetActive (false);
                                _CubeStore.transform.GetChild(0).transform.GetComponent<SpriteRenderer>().enabled = false;
                                PlayerScript.Instance._InInfection (_Type, 1);
								_TreatCount++;
							}
						}


						if (_Cube [1].activeSelf) {
							Debug.Log ("B2");
							if (_InfectionTypePerCube [1] == 0) {
								Debug.Log ("B2.2");
								_Cube [1].SetActive (false);
                                _CubeStore.transform.GetChild(1).transform.GetComponent<SpriteRenderer>().enabled = false;
                                PlayerScript.Instance._InInfection (_Type, 1);
								_TreatCount++;
							}
						}

						if (_Cube [2].activeSelf) {
							Debug.Log ("B3");
							if (_InfectionTypePerCube [2] == 0) {
								Debug.Log ("B3.3");
								_Cube [2].SetActive (false);
                                _CubeStore.transform.GetChild(2).transform.GetComponent<SpriteRenderer>().enabled = false;
                                PlayerScript.Instance._InInfection (_Type, 1);
								_TreatCount++;
							}
						}


						_Level -= _TreatCount;
					} else {

						Debug.Log ("C");

						if (_Cube [0].activeSelf) {
							if (_InfectionTypePerCube [0] == 1) {
								_Cube [0].SetActive (false);
                                _CubeStore.transform.GetChild(0).transform.GetComponent<SpriteRenderer>().enabled = false;
                                PlayerScript.Instance._InInfection (_Type, 1);
								_TreatCount++;
							}
						}

						if (_Cube [1].activeSelf) {

							if (_InfectionTypePerCube [1] == 1) {
								_Cube [1].SetActive (false);
                                _CubeStore.transform.GetChild(1).transform.GetComponent<SpriteRenderer>().enabled = false;
                                PlayerScript.Instance._InInfection (_Type, 1);
								_TreatCount++;
							}
						}

						if (_Cube [2].activeSelf) {

							if (_InfectionTypePerCube [2] == 1) {
								_Cube [2].SetActive (false);
                                _CubeStore.transform.GetChild(2).transform.GetComponent<SpriteRenderer>().enabled = false;
                                PlayerScript.Instance._InInfection (_Type, 1);
								_TreatCount++;
							}
						}


						_Level -= _TreatCount;
					}

				} else {
					PlayerScript.Instance._InInfection (_Type, 1);
					_Level--;

                    //_LevelUp (-1, _Type);
                    switch (_Level)
                    {
                        case 0: // 0 Cube
                            _Cube[0].SetActive(false);
                            _Cube[1].SetActive(false);
                            _Cube[2].SetActive(false);

                            _CubeStore.transform.GetChild(0).transform.GetComponent<SpriteRenderer>().enabled = false;
                            _CubeStore.transform.GetChild(1).transform.GetComponent<SpriteRenderer>().enabled = false;
                            _CubeStore.transform.GetChild(2).transform.GetComponent<SpriteRenderer>().enabled = false;

                            break;
                        case 1:// 1 Cube
                            _Cube[0].SetActive(true);
                            _Cube[1].SetActive(false);
                            _Cube[2].SetActive(false);

                            _CubeStore.transform.GetChild(0).transform.GetComponent<SpriteRenderer>().enabled = true;
                            _CubeStore.transform.GetChild(1).transform.GetComponent<SpriteRenderer>().enabled = false;
                            _CubeStore.transform.GetChild(2).transform.GetComponent<SpriteRenderer>().enabled = false;
                            break;
                        case 2: // 2 Cube
                            _Cube[0].SetActive(true);
                            _Cube[1].SetActive(true);
                            _Cube[2].SetActive(false);

                            _CubeStore.transform.GetChild(0).transform.GetComponent<SpriteRenderer>().enabled = true;
                            _CubeStore.transform.GetChild(1).transform.GetComponent<SpriteRenderer>().enabled = true;
                            _CubeStore.transform.GetChild(2).transform.GetComponent<SpriteRenderer>().enabled = false;
                            break;
                    }
                }
			}

		}
		else
		{

			if (_isMedic)
			{

				Debug.Log ("A");


				if (_Cube [0].activeSelf) {
					if (_InfectionTypePerCube[0] == 0)
					{
						_CountA++; // 
					}
					else
					{
						_CountB++;
					}
				}

				if (_Cube [1].activeSelf) {
					if (_InfectionTypePerCube[1] == 0)
					{
						_CountA++; // 
					}
					else
					{
						_CountB++;
					}
				}

				if (_Cube [2].activeSelf) {
					if (_InfectionTypePerCube[2] == 0)
					{
						_CountA++; // 
					}
					else
					{
						_CountB++;
					}
				}



				Debug.Log (_CountA);
				Debug.Log (_CountB);

				if (_CountA >=_CountB )
				{

					Debug.Log ("B");

					if (_Cube[0].activeSelf)
					{
						Debug.Log ("B1");
						if (_InfectionTypePerCube[0] == 0)
						{
							Debug.Log ("B1.1");
							_Cube[0].SetActive(false);
                            _CubeStore.transform.GetChild(0).transform.GetComponent<SpriteRenderer>().enabled = false;
                            PlayerScript.Instance._InInfection(_Type, 1);
							_TreatCount++;
						}
					}


					if (_Cube[1].activeSelf)
					{
						Debug.Log ("B2");
						if (_InfectionTypePerCube[1] == 0)
						{
							Debug.Log ("B2.2");
							_Cube[1].SetActive(false);
                            _CubeStore.transform.GetChild(1).transform.GetComponent<SpriteRenderer>().enabled = false;
                            PlayerScript.Instance._InInfection(_Type, 1);
							_TreatCount++;
						}
					}

					if (_Cube[2].activeSelf)
					{
						Debug.Log ("B3");
						if (_InfectionTypePerCube[2] == 0)
						{
							Debug.Log ("B3.3");
							_Cube[2].SetActive(false);
                            _CubeStore.transform.GetChild(2).transform.GetComponent<SpriteRenderer>().enabled = false;
                            PlayerScript.Instance._InInfection(_Type, 1);
							_TreatCount++;
						}
					}


					_Level -= _TreatCount;
				}
				else
				{

					Debug.Log ("C");

					if (_Cube[0].activeSelf)
					{
						if (_InfectionTypePerCube[0] == 1)
						{
							_Cube[0].SetActive(false);
                            _CubeStore.transform.GetChild(0).transform.GetComponent<SpriteRenderer>().enabled = false;
                            PlayerScript.Instance._InInfection(_Type, 1);
							_TreatCount++;
						}
					}

					if (_Cube[1].activeSelf)
					{

						if (_InfectionTypePerCube[1] == 1)
						{
							_Cube[1].SetActive(false);
                            _CubeStore.transform.GetChild(1).transform.GetComponent<SpriteRenderer>().enabled = false;
                            PlayerScript.Instance._InInfection(_Type, 1);
							_TreatCount++;
						}
					}

					if (_Cube[2].activeSelf)
					{

						if (_InfectionTypePerCube[2] == 1)
						{
							_Cube[2].SetActive(false);
                            _CubeStore.transform.GetChild(2).transform.GetComponent<SpriteRenderer>().enabled = false;
                            PlayerScript.Instance._InInfection(_Type, 1);
							_TreatCount++;
						}
					}


					_Level -= _TreatCount;
				}

			}
			else
			{
				PlayerScript.Instance._InInfection(_Type, 1);
				_Level--;

				//_LevelUp (-1, _Type);
				switch (_Level)
				{
				case 0: // 0 Cube
					_Cube[0].SetActive(false);
					_Cube[1].SetActive(false);
					_Cube[2].SetActive(false);

                        _CubeStore.transform.GetChild(0).transform.GetComponent<SpriteRenderer>().enabled = false;
                        _CubeStore.transform.GetChild(1).transform.GetComponent<SpriteRenderer>().enabled = false;
                        _CubeStore.transform.GetChild(2).transform.GetComponent<SpriteRenderer>().enabled = false;
                        break;
				case 1:// 1 Cube
					_Cube[0].SetActive(true);
					_Cube[1].SetActive(false);
					_Cube[2].SetActive(false);

                        _CubeStore.transform.GetChild(0).transform.GetComponent<SpriteRenderer>().enabled = true;
                        _CubeStore.transform.GetChild(1).transform.GetComponent<SpriteRenderer>().enabled = false;
                        _CubeStore.transform.GetChild(2).transform.GetComponent<SpriteRenderer>().enabled = false;
                        break;
				case 2: // 2 Cube
					_Cube[0].SetActive(true);
					_Cube[1].SetActive(true);
					_Cube[2].SetActive(false);

                        _CubeStore.transform.GetChild(0).transform.GetComponent<SpriteRenderer>().enabled = true;
                        _CubeStore.transform.GetChild(1).transform.GetComponent<SpriteRenderer>().enabled = true;
                        _CubeStore.transform.GetChild(2).transform.GetComponent<SpriteRenderer>().enabled = false;
                        break;
				}
			}

		}	

	}


	public void _TreatById(bool _isMedic,int _Color){

		_TreatCount = 0;
		_isTreat = false;

		if (PlayerScript.Instance._isCureType [0] ) {

			if (_Cube [0].activeSelf) {
				if (_InfectionTypePerCube [0] == 0) {
					_Cube [0].SetActive (false);
                    _CubeStore.transform.GetChild(0).transform.GetComponent<SpriteRenderer>().enabled = false;
                    PlayerScript.Instance._InInfection (_Type, 1);
					_TreatCount++;
					_isCured = true;
					_isTreat = true;
				}
			}


			if (_Cube [1].activeSelf) {
				if (_InfectionTypePerCube [1] == 0) {
					_Cube [1].SetActive (false);
                    _CubeStore.transform.GetChild(1).transform.GetComponent<SpriteRenderer>().enabled = false;
                    PlayerScript.Instance._InInfection (_Type, 1);
					_TreatCount++;
					_isCured = true;
					_isTreat = true;
				}
			}

			if (_Cube [2].activeSelf) {
				if (_InfectionTypePerCube [2] == 0) {
					_Cube [2].SetActive (false);
                    _CubeStore.transform.GetChild(2).transform.GetComponent<SpriteRenderer>().enabled = false;
                    PlayerScript.Instance._InInfection (_Type, 1);
					_TreatCount++;
					_isCured = true;
					_isTreat = true;
				}
			}

			if (_isTreat) {
				_Level -= _TreatCount;
			} else {
				PlayerScript.Instance._InInfection(_Type, 1);
				_Level--;

				//_LevelUp (-1, _Type);
				switch (_Level)
				{
				case 0: // 0 Cube
					_Cube[0].SetActive(false);
					_Cube[1].SetActive(false);
					_Cube[2].SetActive(false);

                        _CubeStore.transform.GetChild(0).transform.GetComponent<SpriteRenderer>().enabled = false;
                        _CubeStore.transform.GetChild(1).transform.GetComponent<SpriteRenderer>().enabled = false;
                        _CubeStore.transform.GetChild(2).transform.GetComponent<SpriteRenderer>().enabled = false;
                        break;
				case 1:// 1 Cube
					_Cube[0].SetActive(true);
					_Cube[1].SetActive(false);
					_Cube[2].SetActive(false);

                        _CubeStore.transform.GetChild(0).transform.GetComponent<SpriteRenderer>().enabled = true;
                        _CubeStore.transform.GetChild(1).transform.GetComponent<SpriteRenderer>().enabled = false;
                        _CubeStore.transform.GetChild(2).transform.GetComponent<SpriteRenderer>().enabled = false;
                        break;
				case 2: // 2 Cube
					_Cube[0].SetActive(true);
					_Cube[1].SetActive(true);
					_Cube[2].SetActive(false);

                        _CubeStore.transform.GetChild(0).transform.GetComponent<SpriteRenderer>().enabled = true;
                        _CubeStore.transform.GetChild(1).transform.GetComponent<SpriteRenderer>().enabled = true;
                        _CubeStore.transform.GetChild(2).transform.GetComponent<SpriteRenderer>().enabled = false;
                        break;
				}
			}



			//_Relocation ();

		}else if(PlayerScript.Instance._isCureType [1]){

			if (_Cube[0].activeSelf)
			{
				if (_InfectionTypePerCube[0] == 1)
				{
					_Cube[0].SetActive(false);
                    _CubeStore.transform.GetChild(0).transform.GetComponent<SpriteRenderer>().enabled = false;
                    PlayerScript.Instance._InInfection(_Type, 1);
					_TreatCount++;
					_isCured = true;
					_isTreat = true;
				}
			}

			if (_Cube[1].activeSelf)
			{

				if (_InfectionTypePerCube[1] == 1)
				{
					_Cube[1].SetActive(false);
                    _CubeStore.transform.GetChild(1).transform.GetComponent<SpriteRenderer>().enabled = false;
                    PlayerScript.Instance._InInfection(_Type, 1);
					_TreatCount++;
					_isCured = true;
					_isTreat = true;
				}
			}

			if (_Cube[2].activeSelf)
			{

				if (_InfectionTypePerCube[2] == 1)
				{
					_Cube[2].SetActive(false);
                    _CubeStore.transform.GetChild(2).transform.GetComponent<SpriteRenderer>().enabled = false;
                    PlayerScript.Instance._InInfection(_Type, 1);
					_TreatCount++;
					_isCured = true;
					_isTreat = true;
				}
			}


			if (_isTreat) {
				_Level -= _TreatCount;
			} else {
				PlayerScript.Instance._InInfection(_Type, 1);
				_Level--;

				//_LevelUp (-1, _Type);
				switch (_Level)
				{
				case 0: // 0 Cube
					_Cube[0].SetActive(false);
					_Cube[1].SetActive(false);
					_Cube[2].SetActive(false);

                        _CubeStore.transform.GetChild(0).transform.GetComponent<SpriteRenderer>().enabled = false;
                        _CubeStore.transform.GetChild(1).transform.GetComponent<SpriteRenderer>().enabled = false;
                        _CubeStore.transform.GetChild(2).transform.GetComponent<SpriteRenderer>().enabled = false;
                        break;
				case 1:// 1 Cube
					_Cube[0].SetActive(true);
					_Cube[1].SetActive(false);
					_Cube[2].SetActive(false);

                        _CubeStore.transform.GetChild(0).transform.GetComponent<SpriteRenderer>().enabled = true;
                        _CubeStore.transform.GetChild(1).transform.GetComponent<SpriteRenderer>().enabled = false;
                        _CubeStore.transform.GetChild(2).transform.GetComponent<SpriteRenderer>().enabled = false;
                        break;
				case 2: // 2 Cube
					_Cube[0].SetActive(true);
					_Cube[1].SetActive(true);
					_Cube[2].SetActive(false);

                        _CubeStore.transform.GetChild(0).transform.GetComponent<SpriteRenderer>().enabled = true;
                        _CubeStore.transform.GetChild(1).transform.GetComponent<SpriteRenderer>().enabled = true;
                        _CubeStore.transform.GetChild(2).transform.GetComponent<SpriteRenderer>().enabled = false;
                        break;
				}
			}

		}
		else
		{

			if (_isMedic)
			{

				foreach(int x in _InfectionTypePerCube)
				{
					if (x == 0)
					{
						_CountA++;
					}
					else
					{
						_CountB++;
					}
				}

				if (_CountA >= _CountB)
				{
					if (_Cube[0].activeSelf)
					{
						if (_InfectionTypePerCube[0] == 0)
						{
							_Cube[0].SetActive(false);
                            _CubeStore.transform.GetChild(0).transform.GetComponent<SpriteRenderer>().enabled = false;
                            PlayerScript.Instance._InInfection(_Type, 1);
							_TreatCount++;
						}
					}


					if (_Cube[1].activeSelf)
					{
						if (_InfectionTypePerCube[1] == 0)
						{
							_Cube[1].SetActive(false);
                            _CubeStore.transform.GetChild(1).transform.GetComponent<SpriteRenderer>().enabled = false;
                            PlayerScript.Instance._InInfection(_Type, 1);
							_TreatCount++;
						}
					}

					if (_Cube[2].activeSelf)
					{
						if (_InfectionTypePerCube[2] == 0)
						{
							_Cube[2].SetActive(false);
                            _CubeStore.transform.GetChild(2).transform.GetComponent<SpriteRenderer>().enabled = false;
                            PlayerScript.Instance._InInfection(_Type, 1);
							_TreatCount++;
						}
					}


					_Level -= _TreatCount;
				}
				else
				{
					if (_Cube[0].activeSelf)
					{
						if (_InfectionTypePerCube[0] == 1)
						{
							_Cube[0].SetActive(false);
                            _CubeStore.transform.GetChild(0).transform.GetComponent<SpriteRenderer>().enabled = false;
                            PlayerScript.Instance._InInfection(_Type, 1);
							_TreatCount++;
						}
					}

					if (_Cube[1].activeSelf)
					{

						if (_InfectionTypePerCube[1] == 1)
						{
							_Cube[1].SetActive(false);
                            _CubeStore.transform.GetChild(1).transform.GetComponent<SpriteRenderer>().enabled = false;
                            PlayerScript.Instance._InInfection(_Type, 1);
							_TreatCount++;
						}
					}

					if (_Cube[2].activeSelf)
					{

						if (_InfectionTypePerCube[2] == 1)
						{
							_Cube[2].SetActive(false);
                            _CubeStore.transform.GetChild(2).transform.GetComponent<SpriteRenderer>().enabled = false;
                            PlayerScript.Instance._InInfection(_Type, 1);
							_TreatCount++;
						}
					}


					_Level -= _TreatCount;
				}

			}
			else
			{
				PlayerScript.Instance._InInfection(_Color, 1);
				_Level--;

				//_LevelUp (-1, _Type);
				switch (_Level)
				{
				case 0: // 0 Cube
					_Cube[0].SetActive(false);
					_Cube[1].SetActive(false);
					_Cube[2].SetActive(false);

                        _CubeStore.transform.GetChild(0).transform.GetComponent<SpriteRenderer>().enabled = false;
                        _CubeStore.transform.GetChild(1).transform.GetComponent<SpriteRenderer>().enabled = false;
                        _CubeStore.transform.GetChild(2).transform.GetComponent<SpriteRenderer>().enabled = false;
                        break;
				case 1:// 1 Cube
					_Cube[0].SetActive(true);
					_Cube[1].SetActive(false);
					_Cube[2].SetActive(false);

                        _CubeStore.transform.GetChild(0).transform.GetComponent<SpriteRenderer>().enabled = true;
                        _CubeStore.transform.GetChild(1).transform.GetComponent<SpriteRenderer>().enabled = false;
                        _CubeStore.transform.GetChild(2).transform.GetComponent<SpriteRenderer>().enabled = false;
                        break;
				case 2: // 2 Cube
					_Cube[0].SetActive(true);
					_Cube[1].SetActive(true);
					_Cube[2].SetActive(false);

                        _CubeStore.transform.GetChild(0).transform.GetComponent<SpriteRenderer>().enabled = true;
                        _CubeStore.transform.GetChild(1).transform.GetComponent<SpriteRenderer>().enabled = true;
                        _CubeStore.transform.GetChild(2).transform.GetComponent<SpriteRenderer>().enabled = false;
                        break;
				}
			}

		}	

	}

		
	public GameObject _TutorialGameObject;
	public GameObject _MainTutorial;

	void OnMouseDown(){

		if (PlayerScript.Instance._isMoveMode) {
			if (_HideArrow != null) {
				_HideArrow.SetActive (false);
			}
		}

        if (PlayerScript.Instance._isMoveMode)
        {
			if (GetComponent<SpriteRenderer>().color == Color.yellow||GetComponent<SpriteRenderer>().color == Color.green||GetComponent<SpriteRenderer>().color == Color.blue) {
                if (PlayerScript.Instance._isDispatcher_Ability)
                {
                    PlayerScript.Instance._MoveWithAbility(this.gameObject);
                }
                else
                {
                    PlayerScript.Instance._Move(this.gameObject);
                }
			}
		}

        if (PlayerScript.Instance._isDispatcher_Ability)
        {
			if (GetComponent<SpriteRenderer> ().color == Color.yellow || GetComponent<SpriteRenderer> ().color == Color.green) {
				PlayerScript.Instance._MoveWithAbility (this.gameObject);
			}
        }

        if (PlayerScript.Instance._isCanMove)
        {
            if (GetComponent<SpriteRenderer>().color == Color.magenta)
            {
                PlayerScript.Instance._ConfirmMove(this.gameObject);
            }

            if (GetComponent<SpriteRenderer>().color == Color.yellow)
            {
                PlayerScript.Instance._MoveWithAbility(this.gameObject);
            }
        }

		if (!_isShow) {
			if (_TutorialGameObject != null) {
				if (_MainTutorial.GetComponent<TutorialScript> ()._Step == 7) {
					_TutorialGameObject.SetActive (true);
					_isShow = true;
				}
			}
		}



		if (PlayerScript.Instance.GetComponent<TutorialScript> ()._isOnTutorial) {
			if (PlayerScript.Instance.GetComponent<TutorialScript> ()._Step == 12) {

				if (!_isCheck) {
					PlayerScript.Instance.GetComponent<TutorialScript> ()._CheckNode (this.gameObject);
				}
			}
		}

       }

	public bool _isCheck;

	public GameObject _HideArrow;
	//public GameObject _ShowArrowPanel;

	public bool _isShow;
    public Sprite _x;
    public void _Build()
    {
        GetComponent<SpriteRenderer>().sprite = _UnBuildImg;
        _CubeStore.transform.GetChild(3).transform.GetComponent<SpriteRenderer>().sprite = _Locatce;
        _isBuild = true;
    }
	public Sprite _UnBuildImg;
	public void _UnBuild()
	{
        _CubeStore.transform.GetChild(3).transform.GetComponent<SpriteRenderer>().sprite = null;
        GetComponent<SpriteRenderer>().sprite = _UnBuildImg;
		_isBuild = false;
	}

	public List<int> _TempInfectionCube;
	public int _TempLevel;
	bool _TempIsTreat;
	bool _TempIsCured;
	public void _SaveBeforeCure(){
		_TempLevel = _Level;
		_TempIsTreat = _isTreat;
		_TempIsCured = _isCured;

		if (_Cube[0].activeSelf)
		{
			if (_InfectionTypePerCube [0] == 1) {
				_TempInfectionCube[0] = 1;
			} else {
				_TempInfectionCube[0] = -1;
			}
		}

		if (_Cube[1].activeSelf)
		{
			if (_InfectionTypePerCube [1] == 1) {
				_TempInfectionCube[1] = 1;
			} else {
				_TempInfectionCube[1] = -1;
			}
		}

		if (_Cube[2].activeSelf)
		{
			if (_InfectionTypePerCube [2] == 1) {
				_TempInfectionCube[2] = 1;
			} else {
				_TempInfectionCube[2] = -1;
			}
		}

	}

	public void _UndoNode(){
		if (_isCured) {
			_Level = _TempLevel;
			_TempIsCured = _isCured;
			_TempIsTreat = _isTreat;

			if (_TempInfectionCube [0] == 1) {
				_Cube [0].SetActive (true);
				_InfectionTypePerCube [0] = 1;
				PlayerScript.Instance._DeInfection (_Type, 1);
			} else if (_TempInfectionCube [0] == -1) {
				_Cube [0].SetActive (true);
				PlayerScript.Instance._DeInfection (_Type, 1);
				_InfectionTypePerCube [0] = 0;
			}

			if (_TempInfectionCube [1] == 1) {
				_Cube [1].SetActive (true);
				_InfectionTypePerCube [1] = 1;
				PlayerScript.Instance._DeInfection (_Type, 1);
			} else if (_TempInfectionCube [1] == -1) {
				_Cube [1].SetActive (true);
				PlayerScript.Instance._DeInfection (_Type, 1);
				_InfectionTypePerCube [1] = 0;
			}

			if (_TempInfectionCube [2] == 1) {
				_Cube [2].SetActive (true);
				_InfectionTypePerCube [2] = 1;
				PlayerScript.Instance._DeInfection (_Type, 1);
			} else if (_TempInfectionCube [2] == -1) {
				_Cube [2].SetActive (true);
				_InfectionTypePerCube [2] = 0;
				PlayerScript.Instance._DeInfection (_Type, 1);
			}


		}
	}

	bool _isClearNode;
	public void _ClearNode(){
		
		_SaveBeforeCure ();

		_TreatCount = 0;

		if (PlayerScript.Instance._isCureType [0] ) {

			if (_Cube [0].activeSelf) {
				if (_InfectionTypePerCube [0] == 0) {
					_Cube [0].SetActive (false);
                    _CubeStore.transform.GetChild(0).transform.GetComponent<SpriteRenderer>().sprite = null;
                    PlayerScript.Instance._InInfection (_Type, 1);
					_TreatCount++;
					_isCured = true;
					_isTreat = true;
				}
			}


			if (_Cube [1].activeSelf) {
				if (_InfectionTypePerCube [1] == 0) {
					_Cube [1].SetActive (false);
                    _CubeStore.transform.GetChild(1).transform.GetComponent<SpriteRenderer>().sprite = null;
                    PlayerScript.Instance._InInfection (_Type, 1);
					_TreatCount++;
					_isCured = true;
					_isTreat = true;
				}
			}

			if (_Cube [2].activeSelf) {
				if (_InfectionTypePerCube [2] == 0) {
					_Cube [2].SetActive (false);
                    _CubeStore.transform.GetChild(2).transform.GetComponent<SpriteRenderer>().sprite = null;
                    PlayerScript.Instance._InInfection (_Type, 1);
					_TreatCount++;
					_isCured = true;
					_isTreat = true;
				}
			}

				_Level -= _TreatCount;


		}else if(PlayerScript.Instance._isCureType [1]){

			if (_Cube[0].activeSelf)
			{
				if (_InfectionTypePerCube[0] == 1)
				{
					_Cube[0].SetActive(false);
                    _CubeStore.transform.GetChild(0).transform.GetComponent<SpriteRenderer>().sprite = null;
                    PlayerScript.Instance._InInfection(_Type, 1);
					_TreatCount++;
					_isCured = true;
					_isTreat = true;
				}
			}

			if (_Cube[1].activeSelf)
			{

				if (_InfectionTypePerCube[1] == 1)
				{
					_Cube[1].SetActive(false);
                    _CubeStore.transform.GetChild(1).transform.GetComponent<SpriteRenderer>().sprite = null;
                    PlayerScript.Instance._InInfection(_Type, 1);
					_TreatCount++;
					_isCured = true;
					_isTreat = true;
				}
			}

			if (_Cube[2].activeSelf)
			{

				if (_InfectionTypePerCube[2] == 1)
				{
					_Cube[2].SetActive(false);
                    _CubeStore.transform.GetChild(2).transform.GetComponent<SpriteRenderer>().sprite = null;
                    PlayerScript.Instance._InInfection(_Type, 1);
					_TreatCount++;
					_isCured = true;
					_isTreat = true;
				}
			}

				_Level -= _TreatCount;

		}
	}
}
