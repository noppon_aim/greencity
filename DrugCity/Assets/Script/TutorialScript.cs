﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TutorialScript : MonoBehaviour {

    public bool _isOnTutorial;
    public int _Step;
    public GameObject[] _PopUp;
    

    public GameObject[] _PlayerA_StarterNode;
    public GameObject[] _PlayerB_StarterNode;

	public GameObject[] _ZeroWave;

    public GameObject[] _FirstWave;

    public GameObject[] _SecondWave;

	public GameObject[] _19WaveSlot_InfectCard;

	public GameObject[] _19WaveSlot_CityCard;

	public GameObject[] _16WaveSlot;

	public GameObject[] _15WaveSlot;

	public GameObject[] _12WaveSlot;

	public GameObject[] _7WaveSlot;

	public GameObject[] _3WaveSlot;

    public GameObject _EpidemicsNode;

	public GameObject _EpidemicsNode02;

	public GameObject _Node04;
	public GameObject _Node05;
	public GameObject _Node06;
	public GameObject _Node08;
	public GameObject _Node02;
	public GameObject _Node09;

	public GameObject _Node21;
	public GameObject _Node01;
	public GameObject _Node10;
	public GameObject _Node11;
	public GameObject _Node14;
	public GameObject _Node17;
	public GameObject _Node15;
	public GameObject _Node16;
	public GameObject _Node18;
	public GameObject _Node19;
	public GameObject _Node20;
	public GameObject _Node22;
	public GameObject _Node23;

	public GameObject[] _ExtraPopUp;
	public GameObject[] _WarnningPopUp;

	public GameObject[] _Tutorial_MustShow;

    public void _ShowPopUp()
    {
        _PopUp[_Step].SetActive(true) ;
    }

    public void _StepUp()
    {
        _Step++;
    }

    public void _isTutorial()
    {
        _isOnTutorial = true;
		PlayerScript.Instance._ActionBtm [9].interactable = false;
        GetComponent<CardManager>()._StartGameSetup();
    }

	public GameObject _TutorialPanel;
	public GameObject _TutorialPanel01;
	public GameObject _TutorialPanel02;
	public GameObject _TutorialPanel03;
	public void _CloseTapStep3(){
		if (_Step == 3) {
			
			_TutorialPanel.SetActive (true);
			//GetComponent<TutorialScript> ()._Step++;
			_Step++;
		}

		if (_Step == 21) {
			_TutorialPanel01.SetActive (true);
		}

		if (_Step == 27) {
			//_TutorialPanel02.SetActive (true);
		}


	}
	int _count;
	bool _isShow;
	public GameObject[] _Node;
	public void _CheckNode(GameObject i){
		if (!_isShow) {
			if (i == _Node19 || i == _Node21 || i == _Node22) {
				_count++;

				if (i==_Node19) {
					_Node19.GetComponent<NodeScript> ()._isCheck = true;
					_Node [0].SetActive (true);
				}

				if (i==_Node21) {
					_Node21.GetComponent<NodeScript> ()._isCheck = true;
					_Node [1].SetActive (true);
				}

				if (i==_Node22) {
					_Node22.GetComponent<NodeScript> ()._isCheck = true;
					_Node [2].SetActive (true);
				}

				if (_count >= 3) {

					_ExtraPopUp [15].SetActive (true);
					_isShow = true;
				}
			} else {
				_ExtraPopUp [14].SetActive (true);
			}
		}

	}

	public void _ShowInfectionPhase(){
		if (GetComponent<TutorialScript> ()._Step == 9) {
			//_Tutorial_MustShow [4].SetActive (true);
		}
	}

	public void _ExtraMethod(){
		PlayerScript.Instance._CheckCard ();
		PlayerScript.Instance._PlayState = 0;
		PlayerScript.Instance._ChangeImage ();
	}
}
