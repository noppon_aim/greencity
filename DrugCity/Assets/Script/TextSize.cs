﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextSize : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (GetComponent<Text>().text == "WIHELM-KAISEN-BRUCKE")
        {
            GetComponent<Text>().fontSize = 9;
        }
        else
        {
            
            GetComponent<Text>().fontSize = 13;
        }
    
    }
}
