﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerScript : MonoBehaviour {

    public GameObject _PlayerOne;
    public GameObject _PlayerTwo;


    public int _PlayerOneClass;
    public int _PlayerTwoClass;

    public List<CardManager._Card> _PlayerOneCard;
    public List<CardManager._Card> _PlayerTwoCard;

    public GameObject _CurrectNodePlayerOne;
    public GameObject _CurrectNodePlayerTwo;

    public int _State;

    public bool _isMoveMode;

    public int _Action;

    private static PlayerScript _instance;

    public static PlayerScript Instance { get { return _instance; } }

    public Sprite[] _ClassPicture;

    public Sprite[] _PlayerOneClassPic;
    public Sprite[] _PlayerTwoClassPic;

    public int _PlayState;

    public List<GameObject> _ResearchNode;

    public int _InfectionType01;
    public int _InfectionType02;
    public int _Building;

	public GameObject _PhasePanel;
	public GameObject[] _PhaseSubPanel;

    private void Awake() {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        } else {
            _instance = this;
        }
    }

	public Text _TeamCardPart_Player01;
	public Text _TeamCardPart_Player02;
    public void _StartGame() {
        _ActionText.text = "Action : " + _Action + " Lefts";


        //_Class.GetComponent<Image>().sprite = _ClassPicture[_State];
            switch (_PlayerTwoClass)
            {
                case 0:
                    _Class.GetComponent<Image>().sprite = _PlayerTwoClassPic[1];
                    _Class_P2.GetComponent<Image>().sprite = _PlayerTwoClassPic[1];
                    _Class_P2_Phase.GetComponent<Image>().sprite = _PlayerTwoClassPic[1];
                break;
                case 1:
                    _Class.GetComponent<Image>().sprite = _PlayerTwoClassPic[2];
                    _Class_P2.GetComponent<Image>().sprite = _PlayerTwoClassPic[2];
                    _Class_P2_Phase.GetComponent<Image>().sprite = _PlayerTwoClassPic[2];
                break;
                case 2:
                    _Class.GetComponent<Image>().sprite = _PlayerTwoClassPic[3];
                    _Class_P2.GetComponent<Image>().sprite = _PlayerTwoClassPic[3];
                    _Class_P2_Phase.GetComponent<Image>().sprite = _PlayerTwoClassPic[3];
                break;
                default:
                    _Class.GetComponent<Image>().sprite = _PlayerTwoClassPic[0]; // 1
                    _Class_P2.GetComponent<Image>().sprite = _PlayerTwoClassPic[0];
                    _Class_P2_Phase.GetComponent<Image>().sprite = _PlayerTwoClassPic[0];
                break;
            }

            switch (_PlayerOneClass)
            {
                case 0:
                    _Class.GetComponent<Image>().sprite = _PlayerOneClassPic[1];
                    _Class_P1.GetComponent<Image>().sprite = _PlayerOneClassPic[1];
                    _Class_P1_Phase.GetComponent<Image>().sprite = _PlayerOneClassPic[1];
                    break;
                case 1:
                    _Class.GetComponent<Image>().sprite = _PlayerOneClassPic[2];
                    _Class_P1.GetComponent<Image>().sprite = _PlayerOneClassPic[2];
                    _Class_P1_Phase.GetComponent<Image>().sprite = _PlayerOneClassPic[2];
                    break;
                case 2:
                    _Class.GetComponent<Image>().sprite = _PlayerOneClassPic[3];
                    _Class_P1.GetComponent<Image>().sprite = _PlayerOneClassPic[3];
                    _Class_P1_Phase.GetComponent<Image>().sprite = _PlayerOneClassPic[3];
                    break;
                default:
                    _Class.GetComponent<Image>().sprite = _PlayerOneClassPic[0]; // 0
                    _Class_P1.GetComponent<Image>().sprite = _PlayerOneClassPic[0];
                    _Class_P1_Phase.GetComponent<Image>().sprite = _PlayerOneClassPic[0];
                break;
            }

        _PlayState = 0;
        _Infection01OnBar.text = _InfectionType01 + "";
        _Infection02OnBar.text = _InfectionType02 + "";
        _BuildingOnBar.text = _Building + "";


		switch (_PlayerOneClass) {
		case 0:
			_TeamCardPart_Player01.text = "Retailer";
			break;
		case 1:
			_TeamCardPart_Player01.text = "Mayor";
			break;
		case 2:
			_TeamCardPart_Player01.text = "Logistics Service Provider"; 
			break;
		default :
			_TeamCardPart_Player01.text = "";
			break;
		}

		switch (_PlayerTwoClass) {
		case 0:
			_TeamCardPart_Player02.text = "Retailer";
			break;
		case 1:
			_TeamCardPart_Player02.text = "Mayor";
			break;
		case 2:
			_TeamCardPart_Player02.text = "Logistics Service Provider";
			break;
		default :
			_TeamCardPart_Player02.text = "";
			break;
		}

        _CheckCard();
        _CheckCure();
        _CheckPlayerCard();
        _CheckIfShares();
        _CheckIsCanMove();

    }

    public void _MovePlayer() {
        _isMoveMode = true;
        if (_State == 1) {
            foreach (GameObject i in _CurrectNodePlayerOne.GetComponent<NodeScript>()._ConnectNode) {
                if (GetComponent<TutorialScript>()._isOnTutorial)
                {
					

					if (GetComponent<TutorialScript> ()._Step == 1) {
						if (i == GetComponent<TutorialScript> ()._Node02) {
							//i.GetComponent<SpriteRenderer>().color = Color.yellow;
							GetComponent<TutorialScript>()._ExtraPopUp[0].SetActive(true);
						}
					}

					/*if (GetComponent<TutorialScript> ()._Step == 1) {
						if (i == GetComponent<TutorialScript> ()._Node01&&_CurrectNodePlayerOne==GetComponent<TutorialScript> ()._Node02) {
							i.GetComponent<SpriteRenderer>().color = Color.yellow;
						}
					}

                    //i.GetComponent<SpriteRenderer>().color = Color.yellow;
					if (GetComponent<TutorialScript> ()._Step == 3) {
						if (i == GetComponent<TutorialScript> ()._Node06) {
							i.GetComponent<SpriteRenderer>().color = Color.yellow;
						}
					}*/

					if (GetComponent<TutorialScript> ()._Step == 14) {
						if (i == GetComponent<TutorialScript> ()._Node19) {
							i.GetComponent<SpriteRenderer>().color = Color.yellow;
							GetComponent<TutorialScript> ()._Tutorial_MustShow [5].SetActive (false);
							GetComponent<TutorialScript> ()._Tutorial_MustShow [3].SetActive (true);
						}
					}
						

				/*	if (GetComponent<TutorialScript> ()._Step == 6) {
						if (_CurrectNodePlayerOne==GetComponent<TutorialScript> ()._Node08) {
							GetComponent<TutorialScript> ()._Node06.GetComponent<SpriteRenderer>().color = Color.yellow;
						}

						if (_CurrectNodePlayerOne==GetComponent<TutorialScript> ()._Node06) {
							GetComponent<TutorialScript> ()._Node05.GetComponent<SpriteRenderer>().color = Color.yellow;
						}

						if (_CurrectNodePlayerOne==GetComponent<TutorialScript> ()._Node05) {
							GetComponent<TutorialScript> ()._Node04.GetComponent<SpriteRenderer>().color = Color.yellow;
						}
					}



					if (GetComponent<TutorialScript> ()._Step == 13) {
						if (_CurrectNodePlayerOne==GetComponent<TutorialScript> ()._Node21) {
							GetComponent<TutorialScript> ()._PlayerB_StarterNode[2].GetComponent<SpriteRenderer>().color = Color.yellow;
						}

						if (_CurrectNodePlayerOne==GetComponent<TutorialScript> ()._PlayerB_StarterNode[2]) {
							GetComponent<TutorialScript> ()._FirstWave[0].GetComponent<SpriteRenderer>().color = Color.yellow;

						}
							
					}

					if (GetComponent<TutorialScript> ()._Step == 21) {
						if (_CurrectNodePlayerOne==GetComponent<TutorialScript> ()._Node19) {
							GetComponent<TutorialScript> ()._Node17.GetComponent<SpriteRenderer>().color = Color.yellow;
						}
					}

					if (GetComponent<TutorialScript> ()._Step == 22) {
						if (_CurrectNodePlayerOne==GetComponent<TutorialScript> ()._Node17) {
							GetComponent<TutorialScript> ()._Node14.GetComponent<SpriteRenderer>().color = Color.yellow;
						}
					}

					if (GetComponent<TutorialScript> ()._Step == 23) {
						if (_CurrectNodePlayerOne==GetComponent<TutorialScript> ()._Node14) {
							GetComponent<TutorialScript> ()._Node10.GetComponent<SpriteRenderer>().color = Color.yellow;
						}
					}

					if (GetComponent<TutorialScript> ()._Step == 29) {
						if (_CurrectNodePlayerOne==GetComponent<TutorialScript> ()._Node10) {
							GetComponent<TutorialScript> ()._Node09.GetComponent<SpriteRenderer>().color = Color.yellow;
						}
					}

					if (GetComponent<TutorialScript> ()._Step == 31) {
						if (_CurrectNodePlayerOne==GetComponent<TutorialScript> ()._Node09) {
							GetComponent<TutorialScript> ()._Node08.GetComponent<SpriteRenderer>().color = Color.yellow;
						}
					}*/



					if (GetComponent<TutorialScript> ()._Step == 21) {
						GetComponent<TutorialScript> ()._Tutorial_MustShow [11].SetActive (false);
						GetComponent<TutorialScript> ()._Tutorial_MustShow [12].SetActive (true);
					}

					if (GetComponent<TutorialScript> ()._Step == 22) {
						GetComponent<TutorialScript> ()._Tutorial_MustShow [11].SetActive (false);
						GetComponent<TutorialScript> ()._Tutorial_MustShow [13].SetActive (true);
					}

					if (GetComponent<TutorialScript> ()._Step == 23) {
						GetComponent<TutorialScript> ()._Tutorial_MustShow [11].SetActive (false);
						GetComponent<TutorialScript> ()._Tutorial_MustShow [14].SetActive (true);
					}

					if (GetComponent<TutorialScript> ()._Step == 28) {
						GetComponent<TutorialScript> ()._Tutorial_MustShow [19].SetActive (false);
						GetComponent<TutorialScript> ()._Tutorial_MustShow [21].SetActive (true);
					}

					/*if (GetComponent<TutorialScript> ()._Step == 29) {
						GetComponent<TutorialScript> ()._Tutorial_MustShow [18].SetActive (false);
						GetComponent<TutorialScript> ()._Tutorial_MustShow [21].SetActive (true);
					}*/

					if (GetComponent<TutorialScript> ()._Step == 31) {
						GetComponent<TutorialScript> ()._Tutorial_MustShow [18].SetActive (false);
						GetComponent<TutorialScript> ()._Tutorial_MustShow [20].SetActive (true);
					}



				i.GetComponent<SpriteRenderer>().color = Color.yellow;


				}else if (GetComponent<Tutorial_Section02> ()._Section02) {
					
					i.GetComponent<SpriteRenderer>().color = Color.yellow;

					if (GetComponent<Tutorial_Section02> ()._Step == 1) {
						GetComponent<Tutorial_Section02> ()._HideArrow [0].SetActive (false);
						GetComponent<Tutorial_Section02> ()._HideArrow [1].SetActive (true);
					}

					if (GetComponent<Tutorial_Section02> ()._Step == 3) {
						GetComponent<Tutorial_Section02> ()._HideArrow [2].SetActive (false);
						GetComponent<Tutorial_Section02> ()._HideArrow [3].SetActive (true);
					}

					if (GetComponent<Tutorial_Section02> ()._Step == 8) {
						GetComponent<Tutorial_Section02> ()._HideArrow [9].SetActive (false);
						GetComponent<Tutorial_Section02> ()._HideArrow [10].SetActive (true);
					}

					if (GetComponent<Tutorial_Section02> ()._Step == 10) {
						GetComponent<Tutorial_Section02> ()._HideArrow [9].SetActive (false);
						GetComponent<Tutorial_Section02> ()._HideArrow [12].SetActive (true);
					}


					if (GetComponent<Tutorial_Section02> ()._Step == 11) {
						GetComponent<Tutorial_Section02> ()._HideArrow [9].SetActive (false);
						GetComponent<Tutorial_Section02> ()._HideArrow [11].SetActive (true);
					}

					if (GetComponent<Tutorial_Section02> ()._Step == 15) {
						GetComponent<Tutorial_Section02> ()._HideArrow [20].SetActive (false);
						GetComponent<Tutorial_Section02> ()._HideArrow [21].SetActive (true);
					}

					if (GetComponent<Tutorial_Section02> ()._Step == 17) {
						GetComponent<Tutorial_Section02> ()._HideArrow [23].SetActive (false);
						GetComponent<Tutorial_Section02> ()._HideArrow [25].SetActive (true);
					}

					if (GetComponent<Tutorial_Section02> ()._Step == 18) {
						GetComponent<Tutorial_Section02> ()._HideArrow [23].SetActive (false);
						GetComponent<Tutorial_Section02> ()._HideArrow [24].SetActive (true);
					}

					if (GetComponent<Tutorial_Section02> ()._Step == 23) {
						GetComponent<Tutorial_Section02> ()._HideArrow [30].SetActive (false);
						GetComponent<Tutorial_Section02> ()._HideArrow [31].SetActive (true);
					}

					if (GetComponent<Tutorial_Section02> ()._Step == 24) {
						GetComponent<Tutorial_Section02> ()._HideArrow [30].SetActive (false);
						GetComponent<Tutorial_Section02> ()._HideArrow [32].SetActive (true);
					}

				}
                else
                {
                    i.GetComponent<SpriteRenderer>().color = Color.yellow;
                }

            }

			if (GetComponent<TutorialScript> ()._isOnTutorial) {

			} else if (GetComponent<Tutorial_Section02> ()._Section02) {
				
			}else{
				if (_CurrectNodePlayerOne.GetComponent<NodeScript> ()._isBuild) {
					foreach (GameObject i in _ResearchNode) {
						if (i != _CurrectNodePlayerOne) {
							i.GetComponent<SpriteRenderer> ().color = Color.yellow;
						}

					}
				}
			}

            if (_isCharter) {

					foreach (CardManager._Card C in GetComponent<CardManager>()._AllCardStore) {
						if (!C._isEpidemics) {
							if (C._Node.GetComponent<SpriteRenderer> ().color != Color.yellow) {
								if (C._Node != _CurrectNodePlayerOne) {
									C._Node.GetComponent<SpriteRenderer> ().color = Color.green;
								}
							}
						}
					}

            }

        } else {
            foreach (GameObject i in _CurrectNodePlayerTwo.GetComponent<NodeScript>()._ConnectNode) {


				if (GetComponent<TutorialScript> ()._isOnTutorial) {

					i.GetComponent<SpriteRenderer>().color = Color.yellow;

					/// - AAA

					if (GetComponent<TutorialScript> ()._Step == 9) {
						GetComponent<TutorialScript> ()._Tutorial_MustShow [1].SetActive (false);
						GetComponent<TutorialScript> ()._Tutorial_MustShow [2].SetActive (true);
					}

					if (GetComponent<TutorialScript> ()._Step == 16) {
						GetComponent<TutorialScript> ()._Tutorial_MustShow [10].SetActive (false);
						GetComponent<TutorialScript> ()._Tutorial_MustShow [9].SetActive (true);
					}

					if (GetComponent<TutorialScript> ()._Step == 17) {
						GetComponent<TutorialScript> ()._Tutorial_MustShow [10].SetActive (false);
						GetComponent<TutorialScript> ()._Tutorial_MustShow [8].SetActive (true);
					}

					if (GetComponent<TutorialScript> ()._Step == 25) {
						GetComponent<TutorialScript> ()._Tutorial_MustShow [16].SetActive (false);
						GetComponent<TutorialScript> ()._Tutorial_MustShow [17].SetActive (true);
					}



				} else if(GetComponent<Tutorial_Section02>()._Section02){

					i.GetComponent<SpriteRenderer>().color = Color.yellow;

					if (GetComponent<Tutorial_Section02> ()._Step == 4) {
						GetComponent<Tutorial_Section02> ()._HideArrow [4].SetActive (false);
						GetComponent<Tutorial_Section02> ()._HideArrow [5].SetActive (true);
					}

					if (GetComponent<Tutorial_Section02> ()._Step == 5) {
						GetComponent<Tutorial_Section02> ()._HideArrow [6].SetActive (false);
						GetComponent<Tutorial_Section02> ()._HideArrow [7].SetActive (true);
					}

					if (GetComponent<Tutorial_Section02> ()._Step == 6) {
						GetComponent<Tutorial_Section02> ()._HideArrow [6].SetActive (false);
						GetComponent<Tutorial_Section02> ()._HideArrow [8].SetActive (true);
					}

					if (GetComponent<Tutorial_Section02> ()._Step == 11) {
						GetComponent<Tutorial_Section02> ()._HideArrow [14].SetActive (false);
						GetComponent<Tutorial_Section02> ()._HideArrow [15].SetActive (true);
					}

					if (GetComponent<Tutorial_Section02> ()._Step == 13) {
						GetComponent<Tutorial_Section02> ()._HideArrow [17].SetActive (false);
						GetComponent<Tutorial_Section02> ()._HideArrow [18].SetActive (true);
					}

					if (GetComponent<Tutorial_Section02> ()._Step == 14) {
						GetComponent<Tutorial_Section02> ()._HideArrow [17].SetActive (false);
						GetComponent<Tutorial_Section02> ()._HideArrow [19].SetActive (true);
					}

					if (GetComponent<Tutorial_Section02> ()._Step == 19) {
						GetComponent<Tutorial_Section02> ()._HideArrow [26].SetActive (false);
						GetComponent<Tutorial_Section02> ()._HideArrow [27].SetActive (true);
					}

					if (GetComponent<Tutorial_Section02> ()._Step == 20) {
						GetComponent<Tutorial_Section02> ()._HideArrow [26].SetActive (false);
						GetComponent<Tutorial_Section02> ()._HideArrow [28].SetActive (true);
					}

					
				}
                else
                {
                    i.GetComponent<SpriteRenderer>().color = Color.yellow;
                }

            }

	/// Shutter Player 2
			if (GetComponent<TutorialScript> ()._isOnTutorial) {

				if (_CurrectNodePlayerTwo.GetComponent<NodeScript> ()._isBuild) {
					if (GetComponent<TutorialScript> ()._Step != 16) {
						foreach (GameObject i in _ResearchNode) {
							if (i != _CurrectNodePlayerTwo) {
								i.GetComponent<SpriteRenderer> ().color = Color.yellow;
							}

						}
					}
				}
			} else if (GetComponent<Tutorial_Section02> ()._Section02) {
				if (GetComponent<Tutorial_Section02> ()._Step == 4) {
					GetComponent<Tutorial_Section02> ()._Node [9].GetComponent<SpriteRenderer> ().color = Color.yellow;
				}

				if (GetComponent<Tutorial_Section02> ()._Step == 19) {
					GetComponent<Tutorial_Section02> ()._Node [3].GetComponent<SpriteRenderer> ().color = Color.yellow;
				}

			}else{
				if (_CurrectNodePlayerTwo.GetComponent<NodeScript> ()._isBuild) {
					foreach (GameObject i in _ResearchNode) {
						if (i != _CurrectNodePlayerTwo) {
							i.GetComponent<SpriteRenderer> ().color = Color.yellow;
						}

					}
				}
			}

	/// Shutter Player 2 

            if (_isCharter)
            {
                foreach (CardManager._Card C in GetComponent<CardManager>()._AllCardStore)
                {
                    if (!C._isEpidemics)
                    {
                        if (C._Node.GetComponent<SpriteRenderer>().color != Color.yellow)
                        {
                            if (C._Node != _CurrectNodePlayerTwo) {
								
									C._Node.GetComponent<SpriteRenderer> ().color = Color.green;
							
									if (GetComponent<TutorialScript> ()._Step == 5) {
										GetComponent<TutorialScript> ()._ExtraPopUp [9].SetActive (false);
										GetComponent<TutorialScript> ()._ExtraPopUp [10].SetActive (true);
									}

                            }
                        }
                    }
                }
            }
        }





        //Toast.instance.ShowMessage ("Select City To Move", 1f);
    }


	public GameObject _ShowPanel;

    bool _isDirectFilght;
    public void _DirectFilght(int i) {
		if (!_isCanMove || _isDispatcher_Ability) {
			_isMoveMode = true;
			foreach (CardManager._Card C in GetComponent<CardManager>()._AllCardStore) {
				if (!C._isEpidemics) {

					C._Node.GetComponent<NodeScript> ()._ResetCode ();

				}
			}
			if (_State == 1) {
				_PlayerOneCard [i]._Node.GetComponent<SpriteRenderer> ().color = Color.blue;
			} else {
				_PlayerTwoCard [i]._Node.GetComponent<SpriteRenderer> ().color = Color.blue;
			}
			_isDirectFilght = true;
		} else {
			_isMoveMode = true;
			foreach (CardManager._Card C in GetComponent<CardManager>()._AllCardStore) {
				if (!C._isEpidemics) {

					C._Node.GetComponent<NodeScript> ()._ResetCode ();

				}
			}
			if (_State == 1) {
				_PlayerOneCard [i]._Node.GetComponent<SpriteRenderer> ().color = Color.blue;
			} else {
				_PlayerTwoCard [i]._Node.GetComponent<SpriteRenderer> ().color = Color.blue;
			}
			_isDirectFilght = true;
		}

		if (GetComponent<TutorialScript> ()._Step == 12) {
			GetComponent<TutorialScript> ()._Tutorial_MustShow [7].SetActive (false);
			GetComponent<TutorialScript> ()._Tutorial_MustShow [6].SetActive (true);
		}

		if (GetComponent<TutorialScript> ()._Step == 8) {
			GetComponent<TutorialScript> ()._Step++;
		}

		if (GetComponent<TutorialScript> ()._isOnTutorial) {
			if (GetComponent<TutorialScript> ()._Step == 3) {
				GetComponent<TutorialScript> ()._ExtraPopUp [3].SetActive (false);
				GetComponent<TutorialScript> ()._ExtraPopUp [4].SetActive (true);
			}
		}
		
	}

	public GameObject _TutorialPanel;
	public GameObject _TutorialPanel01;
	public GameObject _TutorialPanel02;
	public GameObject _TutorialPanel03;
	public GameObject _TutorialPanel04;
	public GameObject _TutorialPanel05;
	public void _Move(GameObject i){

		if (GetComponent<TutorialScript> ()._isOnTutorial) {
			

			if (_CurrectNodePlayerOne == GetComponent<TutorialScript> ()._Node04) {
				if (i != GetComponent<TutorialScript> ()._Node02) {
					GetComponent<TutorialScript> ()._WarnningPopUp [0].SetActive (true);
					//Debug.Log ("SSD01");
					return;
				}
			}
	
			

			if (_CurrectNodePlayerOne == GetComponent<TutorialScript> ()._Node02) {
				if (i != GetComponent<TutorialScript> ()._Node01) {
					GetComponent<TutorialScript> ()._WarnningPopUp [1].SetActive (true);
					//Debug.Log ("SSD01");
					return;
				}
			}

			if (_CurrectNodePlayerTwo == GetComponent<TutorialScript> ()._Node04 && GetComponent<TutorialScript> ()._Step == 5) {
				if (i != GetComponent<TutorialScript> ()._Node14) {
					GetComponent<TutorialScript> ()._WarnningPopUp [2].SetActive (true);
					//Debug.Log ("SSD01");
					return;
				} else {
					GetComponent<TutorialScript> ()._Tutorial_MustShow [0].SetActive (true);
				}
			}

			if (_CurrectNodePlayerTwo == GetComponent<TutorialScript> ()._Node14 && GetComponent<TutorialScript> ()._Step == 9) {
				if (i != GetComponent<TutorialScript> ()._Node17) {
					GetComponent<TutorialScript> ()._WarnningPopUp [3].SetActive (true);
					//Debug.Log ("SSD01");
					return;
				}
			}

			if (_CurrectNodePlayerOne == GetComponent<TutorialScript> ()._Node21 && GetComponent<TutorialScript> ()._Step == 14) {
				if (i != GetComponent<TutorialScript> ()._Node19) {
					GetComponent<TutorialScript> ()._WarnningPopUp [4].SetActive (true);
					//Debug.Log ("SSD01");
					return;
				}
			}

			if (_CurrectNodePlayerTwo == GetComponent<TutorialScript> ()._Node17 && GetComponent<TutorialScript> ()._Step == 16) {
				if (i != GetComponent<TutorialScript> ()._Node14) {
					GetComponent<TutorialScript> ()._WarnningPopUp [5].SetActive (true);
					//Debug.Log ("SSD01");
					return;
				}
			}

			if (_CurrectNodePlayerTwo == GetComponent<TutorialScript> ()._Node14 && GetComponent<TutorialScript> ()._Step == 17) {
				if (i != GetComponent<TutorialScript> ()._Node10) {
					GetComponent<TutorialScript> ()._WarnningPopUp [6].SetActive (true);
					//Debug.Log ("SSD01");
					return;
				}
			}

			if (_CurrectNodePlayerOne == GetComponent<TutorialScript> ()._Node19 && GetComponent<TutorialScript> ()._Step == 21) {
				if (i != GetComponent<TutorialScript> ()._Node17) {
					GetComponent<TutorialScript> ()._WarnningPopUp [7].SetActive (true);
					//Debug.Log ("SSD01");
					return;
				}
			}

			if (_CurrectNodePlayerOne == GetComponent<TutorialScript> ()._Node17 && GetComponent<TutorialScript> ()._Step == 22) {
				if (i != GetComponent<TutorialScript> ()._Node14) {
					GetComponent<TutorialScript> ()._WarnningPopUp [8].SetActive (true);
					//Debug.Log ("SSD01");
					return;
				}
			}

			if (_CurrectNodePlayerOne == GetComponent<TutorialScript> ()._Node14 && GetComponent<TutorialScript> ()._Step == 23) {
				if (i != GetComponent<TutorialScript> ()._Node10) {
					GetComponent<TutorialScript> ()._WarnningPopUp [9].SetActive (true);
					//Debug.Log ("SSD01");
					return;
				}
			}

			if (_CurrectNodePlayerTwo == GetComponent<TutorialScript> ()._Node10 && GetComponent<TutorialScript> ()._Step == 25) {
				if (i != GetComponent<TutorialScript> ()._Node09) {
					GetComponent<TutorialScript> ()._WarnningPopUp [10].SetActive (true);
					//Debug.Log ("SSD01");
					return;
				}
			}

			if (_CurrectNodePlayerOne == GetComponent<TutorialScript> ()._Node10 && GetComponent<TutorialScript> ()._Step == 28) {
				if (i != GetComponent<TutorialScript> ()._Node09) {
					GetComponent<TutorialScript> ()._WarnningPopUp [11].SetActive (true);
					//Debug.Log ("SSD01");
					return;
				}
			}

			/*if (_CurrectNodePlayerOne == GetComponent<TutorialScript> ()._Node10 && GetComponent<TutorialScript> ()._Step == 29) {
				if (i != GetComponent<TutorialScript> ()._Node09) {
					GetComponent<TutorialScript> ()._WarnningPopUp [11].SetActive (true);
					//Debug.Log ("SSD01");
					return;
				}
			}*/

			if (_CurrectNodePlayerOne == GetComponent<TutorialScript> ()._Node09 && GetComponent<TutorialScript> ()._Step == 31) {
				if (i != GetComponent<TutorialScript> ()._Node08) {
					GetComponent<TutorialScript> ()._WarnningPopUp [12].SetActive (true);
					//Debug.Log ("SSD01");
					return;
				}
			}

		}
			

		if (GetComponent<Tutorial_Section02> ()._Section02) {

			if (_State == 1) {
				if (_CurrectNodePlayerOne == GetComponent<Tutorial_Section02> ()._Node [3]) {
					if (i != GetComponent<Tutorial_Section02> ()._Node [9]) {
						GetComponent<Tutorial_Section02> ()._WarnningPopUp [0].SetActive (true);
							
						//Debug.Log ("SSD01");
						return;
					} else {
						GetComponent<Tutorial_Section02> ()._HideArrow [6].SetActive (true);
						GetComponent<Tutorial_Section02> ()._HideArrow [7].SetActive (false);
					}
				}

				if (_CurrectNodePlayerOne == GetComponent<Tutorial_Section02> ()._Node [9]) {
					if (i != GetComponent<Tutorial_Section02> ()._Node [13]) {
						GetComponent<Tutorial_Section02> ()._WarnningPopUp [1].SetActive (true);
						if (GetComponent<Tutorial_Section02> ()._Step == 5) {
							GetComponent<Tutorial_Section02> ()._HideArrow [6].SetActive (true);
							GetComponent<Tutorial_Section02> ()._HideArrow [7].SetActive (false);
						}

						return;
					}
				}


				if (GetComponent<Tutorial_Section02> ()._Step == 8) {
					if (_CurrectNodePlayerOne == GetComponent<Tutorial_Section02> ()._Node [13]) {
						if (i != GetComponent<Tutorial_Section02> ()._Node [9]) {
							GetComponent<Tutorial_Section02> ()._WarnningPopUp [5].SetActive (true);
							//Debug.Log ("SSD01");
							return;
						} else {
								GetComponent<Tutorial_Section02> ()._HideArrow [10].SetActive (false);
								GetComponent<Tutorial_Section02> ()._HideArrow [13].SetActive (true);
						}
					}
				}

				if (GetComponent<Tutorial_Section02> ()._Step == 10) {
					if (_CurrectNodePlayerOne == GetComponent<Tutorial_Section02> ()._Node [9]) {
						if (i != GetComponent<Tutorial_Section02> ()._Node [13]) {
							GetComponent<Tutorial_Section02> ()._WarnningPopUp [6].SetActive (true);
							//Debug.Log ("SSD01");
							return;
						} else {
								GetComponent<Tutorial_Section02> ()._HideArrow [12].SetActive (false);
								GetComponent<Tutorial_Section02> ()._HideArrow [9].SetActive (true);
						}
					}
				}

				if (GetComponent<Tutorial_Section02> ()._Step == 11) {
					if (_CurrectNodePlayerOne == GetComponent<Tutorial_Section02> ()._Node [13]) {
						if (i != GetComponent<Tutorial_Section02> ()._Node [15]) {
							GetComponent<Tutorial_Section02> ()._WarnningPopUp [7].SetActive (true);
							//Debug.Log ("SSD01");
							return;
						}
					}
				}

				if (GetComponent<Tutorial_Section02> ()._Step == 15) {
					if (_CurrectNodePlayerOne == GetComponent<Tutorial_Section02> ()._Node [15]) {
						if (i != GetComponent<Tutorial_Section02> ()._Node [16]) {
							GetComponent<Tutorial_Section02> ()._WarnningPopUp [11].SetActive (true);
							//Debug.Log ("SSD01");
							return;
						}
					}
				}

				if (GetComponent<Tutorial_Section02> ()._Step == 17) {
					if (_CurrectNodePlayerOne == GetComponent<Tutorial_Section02> ()._Node [16]) {
						if (i != GetComponent<Tutorial_Section02> ()._Node [13]) {
							GetComponent<Tutorial_Section02> ()._WarnningPopUp [12].SetActive (true);
							//Debug.Log ("SSD01");
							return;
						} else {
							GetComponent<Tutorial_Section02> ()._HideArrow [25].SetActive (false);
							GetComponent<Tutorial_Section02> ()._HideArrow [23].SetActive (true);
						}
					}
				}

				if (GetComponent<Tutorial_Section02> ()._Step == 18) {
					if (_CurrectNodePlayerOne == GetComponent<Tutorial_Section02> ()._Node [13]) {
						if (i != GetComponent<Tutorial_Section02> ()._Node [11]) {
							GetComponent<Tutorial_Section02> ()._WarnningPopUp [13].SetActive (true);
							//Debug.Log ("SSD01");
							return;
						}
					}
				}

				if (GetComponent<Tutorial_Section02> ()._Step == 23) {
					if (_CurrectNodePlayerOne == GetComponent<Tutorial_Section02> ()._Node [11]) {
						if (i != GetComponent<Tutorial_Section02> ()._Node [10]) {
							GetComponent<Tutorial_Section02> ()._WarnningPopUp [16].SetActive (true);
							//Debug.Log ("SSD01");
							return;
						} else {
							GetComponent<Tutorial_Section02> ()._HideArrow [31].SetActive (false);
							GetComponent<Tutorial_Section02> ()._HideArrow [30].SetActive (true);
						}
					}
				}

				if (GetComponent<Tutorial_Section02> ()._Step == 24) {
					if (_CurrectNodePlayerOne == GetComponent<Tutorial_Section02> ()._Node [10]) {
						if (i != GetComponent<Tutorial_Section02> ()._Node [9]) {
							GetComponent<Tutorial_Section02> ()._WarnningPopUp [17].SetActive (true);
							//Debug.Log ("SSD01");
							return;
						}else {
							GetComponent<Tutorial_Section02> ()._HideArrow [32].SetActive (false);
							GetComponent<Tutorial_Section02> ()._HideArrow [33].SetActive (true);
						}
					}
				}

			} else {


				if (GetComponent<Tutorial_Section02> ()._Step == 4) {

					if (_CurrectNodePlayerTwo == GetComponent<Tutorial_Section02> ()._Node [3]) {
						if (i != GetComponent<Tutorial_Section02> ()._Node [9]) {
							GetComponent<Tutorial_Section02> ()._WarnningPopUp [2].SetActive (true);
							//Debug.Log ("SSD01");
							return;
						}
					}
				}

				if (GetComponent<Tutorial_Section02> ()._Step == 5) {

					if (_CurrectNodePlayerTwo == GetComponent<Tutorial_Section02> ()._Node [9]) {
						if (i != GetComponent<Tutorial_Section02> ()._Node [13]) {
							GetComponent<Tutorial_Section02> ()._WarnningPopUp [3].SetActive (true);
							//Debug.Log ("SSD01");
							return;
						} else {
							GetComponent<Tutorial_Section02> ()._HideArrow [7].SetActive (false);
							GetComponent<Tutorial_Section02> ()._HideArrow [6].SetActive (true);
						}
					}
				}

				if (GetComponent<Tutorial_Section02> ()._Step == 6) {

					if (_CurrectNodePlayerTwo == GetComponent<Tutorial_Section02> ()._Node [13]) {
						if (i != GetComponent<Tutorial_Section02> ()._Node [16]) {
							GetComponent<Tutorial_Section02> ()._WarnningPopUp [4].SetActive (true);
							//Debug.Log ("SSD01");
							return;
						} else {
								GetComponent<Tutorial_Section02> ()._HideArrow [6].SetActive (false);
								GetComponent<Tutorial_Section02> ()._HideArrow [8].SetActive (true);
						}
					}
				}

				if (GetComponent<Tutorial_Section02> ()._Step == 11) {

					if (_CurrectNodePlayerTwo == GetComponent<Tutorial_Section02> ()._Node [16]) {
						if (i != GetComponent<Tutorial_Section02> ()._Node [15]) {
							GetComponent<Tutorial_Section02> ()._WarnningPopUp [8].SetActive (true);
							//Debug.Log ("SSD01");
							return;
						} else {
								GetComponent<Tutorial_Section02> ()._HideArrow [15].SetActive (false);
								GetComponent<Tutorial_Section02> ()._HideArrow [16].SetActive (true);
						}
					}
				}

				if (GetComponent<Tutorial_Section02> ()._Step == 13) {

					if (_CurrectNodePlayerTwo == GetComponent<Tutorial_Section02> ()._Node [15]) {
						if (i != GetComponent<Tutorial_Section02> ()._Node [13]) {
							GetComponent<Tutorial_Section02> ()._WarnningPopUp [9].SetActive (true);
							//Debug.Log ("SSD01");
							return;
						} else {
							GetComponent<Tutorial_Section02> ()._HideArrow [18].SetActive (false);
							GetComponent<Tutorial_Section02> ()._HideArrow [17].SetActive (true);
						}
					}
				}

				if (GetComponent<Tutorial_Section02> ()._Step == 14) {

					if (_CurrectNodePlayerTwo == GetComponent<Tutorial_Section02> ()._Node [13]) {
						if (i != GetComponent<Tutorial_Section02> ()._Node [9]) {
							GetComponent<Tutorial_Section02> ()._WarnningPopUp [10].SetActive (true);
							//Debug.Log ("SSD01");
							return;
						}
					}
				}

				if (GetComponent<Tutorial_Section02> ()._Step == 19) {

					if (_CurrectNodePlayerTwo == GetComponent<Tutorial_Section02> ()._Node [9]) {
						if (i != GetComponent<Tutorial_Section02> ()._Node [3]) {
							GetComponent<Tutorial_Section02> ()._WarnningPopUp [14].SetActive (true);
							//Debug.Log ("SSD01");
							return;
						}else {
							GetComponent<Tutorial_Section02> ()._HideArrow [27].SetActive (false);
							GetComponent<Tutorial_Section02> ()._HideArrow [26].SetActive (true);
						}
					}
				}

				if (GetComponent<Tutorial_Section02> ()._Step == 20) {

					if (_CurrectNodePlayerTwo == GetComponent<Tutorial_Section02> ()._Node [3]) {
						if (i != GetComponent<Tutorial_Section02> ()._Node [1]) {
							GetComponent<Tutorial_Section02> ()._WarnningPopUp [15].SetActive (true);
							//Debug.Log ("SSD01");
							return;
						}else {
							GetComponent<Tutorial_Section02> ()._HideArrow [28].SetActive (false);
							GetComponent<Tutorial_Section02> ()._HideArrow [29].SetActive (true);
						}
					}
				}


			}


		}

		//Debug.Log ("SSD01");

        _SaveAction(0);
		GetComponent<LogFilesScript> ()._CheckActionById.Add (0);

        if (_State==1) {
			

            if (i.GetComponent<SpriteRenderer>().color ==Color.green)
            {

                if (GetComponent<TutorialScript>()._isOnTutorial)
                {
                    if (i != GetComponent<TutorialScript>()._FirstWave[0] && GetComponent<TutorialScript>()._Step == 9)
                    {
                        Debug.Log("Return" + i.name);
                        return;
                    }

                    if (i != GetComponent<TutorialScript>()._PlayerB_StarterNode[2]&&GetComponent<TutorialScript>()._Step == 23 )
                    {
                        Debug.Log("Return X" + i.name);
                        return;
                    }

                    //Debug.Log("Pass" + i.name);
                    if (GetComponent<TutorialScript>()._isOnTutorial && GetComponent<TutorialScript>()._Step == 9)
                    {
                        GetComponent<TutorialScript>()._ShowPopUp();
                    }


                }

				if (GetComponent<Tutorial_Section02>()._Section02 && GetComponent<Tutorial_Section02>()._Step == 1)
				{
					GetComponent<Tutorial_Section02> ()._Tu [2].SetActive (true);
					GetComponent<Tutorial_Section02> ()._HideTu [1].SetActive (false);
					GetComponent<Tutorial_Section02> ()._Step++;
				}



                foreach (CardManager._Card C in _PlayerOneCard)
                {
                    
                    if (C._Node==_CurrectNodePlayerOne){
                        
                        _PlayerOne.transform.position = Vector3.MoveTowards(_PlayerOne.transform.position, i.transform.position, 100f);
						_PlayerOne.transform.position = new Vector3 (_PlayerOne.transform.position.x - 0.1f, _PlayerOne.transform.position.y, _PlayerOne.transform.position.z+0.1f);
                        GetComponent<CardManager>()._PLayerDiscardplie.Add(C);
                        _PlayerOneCard.Remove(C);
                        break;
                    }

                }

                foreach (CardManager._Card C in GetComponent<CardManager>()._AllCardStore)
                {
                    if (!C._isEpidemics)
                    {

                        C._Node.GetComponent<NodeScript>()._ResetCode();
                        
                    }
                }
                _CurrectNodePlayerOne = i;
                _isCharter = false;
               // _CheckPlayerCard();
			}else if(_isDirectFilght){
				_PlayerOne.transform.position = Vector3.MoveTowards(_PlayerOne.transform.position, i.transform.position, 100f);
				_PlayerOne.transform.position = new Vector3 (_PlayerOne.transform.position.x - 0.1f, _PlayerOne.transform.position.y, _PlayerOne.transform.position.z+0.1f);
				_CurrectNodePlayerOne = i;
				foreach (CardManager._Card C in _PlayerOneCard)
				{

					if (C._Node==_CurrectNodePlayerOne){
						GetComponent<CardManager>()._PLayerDiscardplie.Add(C);
						_PlayerOneCard.Remove(C);
						break;
					}

				}

				_isDirectFilght = false;
				if (GetComponent<TutorialScript> ()._Step == 3) {
					if (_CurrectNodePlayerOne == GetComponent<TutorialScript> ()._Node11) {
						GetComponent<TutorialScript> ()._ExtraPopUp [4].SetActive (false);
						_TutorialPanel01.SetActive (true);
					}
				}

				if (GetComponent<TutorialScript> ()._Step == 12) {
					
					GetComponent<TutorialScript> ()._Step++;
					GetComponent<TutorialScript> ()._ExtraPopUp [19].SetActive (false);
					GetComponent<TutorialScript> ()._ExtraPopUp [20].SetActive (true);
				}



			}else{
				

                if (GetComponent<TutorialScript>()._isOnTutorial && GetComponent<TutorialScript>()._Step == 13)
                {
                    GetComponent<TutorialScript>()._Step++;
                }

                if (GetComponent<TutorialScript>()._isOnTutorial && GetComponent<TutorialScript>()._Step == 23)
                {
                    GetComponent<TutorialScript>()._Step++;
                }

				if (GetComponent<TutorialScript> ()._isOnTutorial && GetComponent<TutorialScript> ()._Step == 13&&_CurrectNodePlayerOne==GetComponent<TutorialScript>()._PlayerB_StarterNode[2]) {
					GetComponent<TutorialScript> ()._Step++;
				}

				if (GetComponent<TutorialScript>()._isOnTutorial && GetComponent<TutorialScript>()._Step == 21&&_CurrectNodePlayerOne==GetComponent<TutorialScript>()._PlayerB_StarterNode[1])
				{
					GetComponent<TutorialScript>()._Step++;
				}


				/// - BBB


              		_PlayerOne.transform.position = Vector3.MoveTowards(_PlayerOne.transform.position, i.transform.position, 100f);
                    _PlayerOne.transform.position = new Vector3(_PlayerOne.transform.position.x - 0.1f, _PlayerOne.transform.position.y, _PlayerOne.transform.position.z + 0.1f);
                    foreach (GameObject x in _CurrectNodePlayerOne.GetComponent<NodeScript>()._ConnectNode)
                    {
                        x.GetComponent<NodeScript>()._ResetCode();
                    }
                    _CurrectNodePlayerOne = i;

				if (GetComponent<TutorialScript> ()._isOnTutorial) {

					if (GetComponent<TutorialScript> ()._Step == 1) {
						if (_CurrectNodePlayerOne == GetComponent<TutorialScript> ()._Node01) {
							_TutorialPanel05.SetActive (true);
						}

						if (_CurrectNodePlayerOne == GetComponent<TutorialScript> ()._Node02) {
							GetComponent<TutorialScript> ()._ExtraPopUp [1].SetActive (true);
						}
					}

					if (GetComponent<TutorialScript> ()._Step == 3) {


						if (_CurrectNodePlayerOne == GetComponent<TutorialScript> ()._Node06) {
							_TutorialPanel.SetActive (true);
						}

						if (_CurrectNodePlayerOne == GetComponent<TutorialScript> ()._Node08) {
							_TutorialPanel01.SetActive (true);
						}

						if (_CurrectNodePlayerOne == GetComponent<TutorialScript> ()._Node04) {
							_TutorialPanel03.SetActive (true);
						}
					}

					if (GetComponent<TutorialScript> ()._Step == 6) {
						if (_CurrectNodePlayerOne == GetComponent<TutorialScript> ()._Node04) {
							_TutorialPanel04.SetActive (true);
							GetComponent<TutorialScript> ()._Step++;
						}
					}

					if (GetComponent<TutorialScript> ()._Step == 14) {
						if (_CurrectNodePlayerOne == GetComponent<TutorialScript> ()._Node19) {
							GetComponent<TutorialScript> ()._Step++;
							GetComponent<TutorialScript> ()._Tutorial_MustShow [3].SetActive (false);
							GetComponent<TutorialScript> ()._Tutorial_MustShow [4].SetActive (true);

						}
					}

					if (GetComponent<TutorialScript> ()._Step == 21) {
						if (_CurrectNodePlayerOne == GetComponent<TutorialScript> ()._Node17) {
							GetComponent<TutorialScript> ()._Step++;
							GetComponent<TutorialScript> ()._Tutorial_MustShow [12].SetActive (false);
							GetComponent<TutorialScript> ()._Tutorial_MustShow [11].SetActive (true);
						}
					}

					if (GetComponent<TutorialScript> ()._Step == 22) {
						if (_CurrectNodePlayerOne == GetComponent<TutorialScript> ()._Node14) {
							GetComponent<TutorialScript> ()._Step++;
							GetComponent<TutorialScript> ()._Tutorial_MustShow [13].SetActive (false);
							GetComponent<TutorialScript> ()._Tutorial_MustShow [11].SetActive (true);
						}
					}

					if (GetComponent<TutorialScript> ()._Step == 24) {
						if (_CurrectNodePlayerOne == GetComponent<TutorialScript> ()._Node10) {
							GetComponent<TutorialScript> ()._ExtraPopUp [26].SetActive (false);
							_Pass ();
							_PhaseSubPanel[2].SetActive(true);
						}
					}

					if (GetComponent<TutorialScript> ()._Step == 28) {
						if (_CurrectNodePlayerOne == GetComponent<TutorialScript> ()._Node09) {
							GetComponent<TutorialScript> ()._Step++;
							GetComponent<TutorialScript> ()._Tutorial_MustShow [21].SetActive (false);
							GetComponent<TutorialScript> ()._Tutorial_MustShow [18].SetActive (true);
						}
					}

					if (GetComponent<TutorialScript> ()._Step == 31) {
						if (_CurrectNodePlayerOne == GetComponent<TutorialScript> ()._Node08) {
							GetComponent<TutorialScript> ()._ExtraPopUp [28].SetActive (false);
							_Pass ();
							_PhaseSubPanel[2].SetActive(true);
						}
					}


				} else if (GetComponent<Tutorial_Section02> ()._Section02) {
					if (GetComponent<Tutorial_Section02> ()._Step == 3) {
						if (_CurrectNodePlayerOne == GetComponent<Tutorial_Section02> ()._Node[13]) {
							GetComponent<Tutorial_Section02> ()._HideTu [3].SetActive (false);
							_PhaseSubPanel [2].SetActive (true);
							_Pass ();
						}
					}

					if (GetComponent<Tutorial_Section02> ()._Step == 8) {
						if (_CurrectNodePlayerOne == GetComponent<Tutorial_Section02> ()._Node[9]) {
							//GetComponent<Tutorial_Section02> ()._Tu [6].SetActive (true);
							GetComponent<Tutorial_Section02> ()._Step++;
						}
					}

					if (GetComponent<Tutorial_Section02> ()._Step == 10) {
						if (_CurrectNodePlayerOne == GetComponent<Tutorial_Section02> ()._Node[13]) {
							//GetComponent<Tutorial_Section02> ()._Tu [6].SetActive (true);
							GetComponent<Tutorial_Section02> ()._Step++;
						}
					}

					if (GetComponent<Tutorial_Section02> ()._Step == 11) {
						if (_CurrectNodePlayerOne == GetComponent<Tutorial_Section02> ()._Node[15]) {
							_PhaseSubPanel [2].SetActive (true);
							GetComponent<Tutorial_Section02> ()._HideTu [7].SetActive (false);
							_Pass();
						}
					}

					if (GetComponent<Tutorial_Section02> ()._Step == 15) {
						if (_CurrectNodePlayerOne == GetComponent<Tutorial_Section02> ()._Node[16]) {
							GetComponent<Tutorial_Section02> ()._Step++;
						}
					}

					if (GetComponent<Tutorial_Section02> ()._Step == 17) {
						if (_CurrectNodePlayerOne == GetComponent<Tutorial_Section02> ()._Node[13]) {
							GetComponent<Tutorial_Section02> ()._Step++;
						}
					}

					if (GetComponent<Tutorial_Section02> ()._Step == 18) {
						if (_CurrectNodePlayerOne == GetComponent<Tutorial_Section02> ()._Node[11]) {
							_PhaseSubPanel [2].SetActive (true);
							GetComponent<Tutorial_Section02> ()._HideTu [11].SetActive (false);
							_Pass();

						}
					}

					if (GetComponent<Tutorial_Section02> ()._Step == 23) {
						if (_CurrectNodePlayerOne == GetComponent<Tutorial_Section02> ()._Node[10]) {
							GetComponent<Tutorial_Section02> ()._Step++;
						}
					}

					if (GetComponent<Tutorial_Section02> ()._Step == 24) {
						if (_CurrectNodePlayerOne == GetComponent<Tutorial_Section02> ()._Node[9]) {
							GetComponent<Tutorial_Section02> ()._Step++;
						}
					}
				}


                    Debug.Log(i.name);

                    foreach (CardManager._Card C in GetComponent<CardManager>()._AllCardStore)
                    {
                        if (!C._isEpidemics)
                        {

                            C._Node.GetComponent<NodeScript>()._ResetCode();

                        }
                    }
                
               
            }
            //Toast.instance.ShowMessage ("Player 1 move to "+_CurrectNodePlayerTwo.GetComponent<NodeScript>().name,1);
            
            foreach (GameObject z in _ResearchNode)
            {
                z.GetComponent<NodeScript>()._ResetCode();
            }

            _DecreseAction();
            _isMoveMode = false;

			//GetComponent<LogFilesScript> ()._CheckActionById = 1;

        }
        else{
            if (i.GetComponent<SpriteRenderer>().color == Color.green)
            {



                foreach (CardManager._Card C in _PlayerTwoCard)
                {

                    if (C._Node == _CurrectNodePlayerTwo)
                    {
                        _PlayerTwo.transform.position = Vector3.MoveTowards(_PlayerTwo.transform.position, i.transform.position, 100f);
						_PlayerTwo.transform.position = new Vector3 (_PlayerTwo.transform.position.x + 0.1f, _PlayerTwo.transform.position.y, _PlayerTwo.transform.position.z+0.1f);
                        GetComponent<CardManager>()._PLayerDiscardplie.Add(C);
                        _PlayerTwoCard.Remove(C);
                        break;
                    }

                }

                foreach (CardManager._Card C in GetComponent<CardManager>()._AllCardStore)
                {
                    if (!C._isEpidemics)
                    {

                        C._Node.GetComponent<NodeScript>()._ResetCode();

                    }
                }
               // _CheckPlayerCard();
                _CurrectNodePlayerTwo = i;
                _isCharter = false;

				if (GetComponent<TutorialScript>()._Step == 17)
				{
					GetComponent<TutorialScript> ()._Step++;

				}

				if (GetComponent<TutorialScript>()._Step == 5)
				{
					if (GetComponent<TutorialScript> ()._Node14 == _CurrectNodePlayerTwo) {
						GetComponent<TutorialScript> ()._ExtraPopUp [10].SetActive (false);
						//GetComponent<TutorialScript> ()._ExtraPopUp [11].SetActive (true);
						GetComponent<TutorialScript> ()._Step++;
					} else {
						//GetComponent<TutorialScript> ()._ExtraPopUp [12].SetActive (true);
					}
				}

			}else if(_isDirectFilght){

               

                _PlayerTwo.transform.position = Vector3.MoveTowards(_PlayerTwo.transform.position, i.transform.position, 100f);
				_PlayerTwo.transform.position = new Vector3 (_PlayerTwo.transform.position.x + 0.1f, _PlayerTwo.transform.position.y, _PlayerTwo.transform.position.z+0.1f);
				_CurrectNodePlayerTwo = i;
				foreach (CardManager._Card C in _PlayerTwoCard)
				{

					if (C._Node==_CurrectNodePlayerTwo){
						GetComponent<CardManager>()._PLayerDiscardplie.Add(C);
						_PlayerTwoCard.Remove(C);
						break;
					}

				}

				_isDirectFilght = false;

				if (GetComponent<TutorialScript>()._isOnTutorial && GetComponent<TutorialScript>()._Step == 6)
				{
					GetComponent<TutorialScript>()._ShowPopUp();
				}



			}
            else
            {

                _PlayerTwo.transform.position = Vector3.MoveTowards(_PlayerTwo.transform.position, i.transform.position, 100f);
				_PlayerTwo.transform.position = new Vector3 (_PlayerTwo.transform.position.x + 0.1f, _PlayerTwo.transform.position.y, _PlayerTwo.transform.position.z+0.1f);
                foreach (GameObject x in _CurrectNodePlayerTwo.GetComponent<NodeScript>()._ConnectNode)
                {
                    x.GetComponent<NodeScript>()._ResetCode();
                }
                _CurrectNodePlayerTwo = i;

                foreach (CardManager._Card C in GetComponent<CardManager>()._AllCardStore)
                {
                    if (!C._isEpidemics)
                    {

                        C._Node.GetComponent<NodeScript>()._ResetCode();

                    }
                }

				if (GetComponent<TutorialScript> ()._isOnTutorial) {

					if (GetComponent<TutorialScript> ()._Step == 9) {
						if (_CurrectNodePlayerTwo == GetComponent<TutorialScript> ()._Node17) {
							//GetComponent<TutorialScript> ()._ExtraPopUp [13].SetActive (true);
							GetComponent<TutorialScript> ()._ExtraPopUp [18].SetActive (false);
						_Pass ();
							_PhaseSubPanel[2].SetActive(true);
						}
					}

					if (GetComponent<TutorialScript> ()._Step == 16) {
						if (_CurrectNodePlayerTwo == GetComponent<TutorialScript> ()._Node14) {
							GetComponent<TutorialScript> ()._Step++;

							GetComponent<TutorialScript> ()._Tutorial_MustShow [9].SetActive (false);
							GetComponent<TutorialScript> ()._Tutorial_MustShow [10].SetActive (true);
						}
					}

					if (GetComponent<TutorialScript> ()._Step == 17) {
						if (_CurrectNodePlayerTwo == GetComponent<TutorialScript> ()._Node10) {
							GetComponent<TutorialScript> ()._ExtraPopUp [16].SetActive (true);
							GetComponent<TutorialScript> ()._ExtraPopUp [23].SetActive (false);
							//GetComponent<TutorialScript> ()._ExtraPopUp [24].SetActive (true);
							GetComponent<TutorialScript> ()._Step++;



						}
					}

					if (GetComponent<TutorialScript> ()._Step == 25) {
						if (_CurrectNodePlayerTwo == GetComponent<TutorialScript> ()._Node09) {
							GetComponent<TutorialScript> ()._Step++;

							GetComponent<TutorialScript> ()._Tutorial_MustShow [17].SetActive (false);
							GetComponent<TutorialScript> ()._Tutorial_MustShow [15].SetActive (true);
							
						}
					}

					if (GetComponent<TutorialScript> ()._Step == 32) {
						if (_CurrectNodePlayerTwo == GetComponent<TutorialScript> ()._Node09) {
							GetComponent<TutorialScript> ()._Step++;
						}
					}
				} else if (GetComponent<Tutorial_Section02> ()._Section02) {
					if (GetComponent<Tutorial_Section02> ()._Step == 4) {
						if (_CurrectNodePlayerTwo == GetComponent<Tutorial_Section02> ()._Node[9]) {
							GetComponent<Tutorial_Section02> ()._Tu [5].SetActive (true);
							GetComponent<Tutorial_Section02> ()._HideTu [4].SetActive (false);
							GetComponent<Tutorial_Section02> ()._Step++;
						}
					}

					if (GetComponent<Tutorial_Section02> ()._Step == 5) {
						if (_CurrectNodePlayerTwo == GetComponent<Tutorial_Section02> ()._Node[13]) {
							//GetComponent<Tutorial_Section02> ()._Tu [6].SetActive (true);
							GetComponent<Tutorial_Section02> ()._Step++;
						}
					}

					if (GetComponent<Tutorial_Section02> ()._Step == 6) {
						if (_CurrectNodePlayerTwo == GetComponent<Tutorial_Section02> ()._Node[16]) {
							GetComponent<Tutorial_Section02> ()._Tu [6].SetActive (true);
							GetComponent<Tutorial_Section02> ()._HideTu [5].SetActive (false);
							GetComponent<Tutorial_Section02> ()._Step++;
						}
					}

					if (GetComponent<Tutorial_Section02> ()._Step == 11) {
						if (_CurrectNodePlayerTwo == GetComponent<Tutorial_Section02> ()._Node[15]) {
							//GetComponent<Tutorial_Section02> ()._Tu [6].SetActive (true);
							//GetComponent<Tutorial_Section02> ()._HideTu [8].SetActive (false);
							// Hide Arrow in TU 03-03 Move and Node
							GetComponent<Tutorial_Section02> ()._Step++;
						}
					}

					if (GetComponent<Tutorial_Section02> ()._Step == 13) {
						if (_CurrectNodePlayerTwo == GetComponent<Tutorial_Section02> ()._Node[13]) {
							//GetComponent<Tutorial_Section02> ()._Tu [6].SetActive (true);
							GetComponent<Tutorial_Section02> ()._Step++;
						}
					}

					if (GetComponent<Tutorial_Section02> ()._Step == 14) {
						if (_CurrectNodePlayerTwo == GetComponent<Tutorial_Section02> ()._Node[9]) {
							//GetComponent<Tutorial_Section02> ()._Tu [6].SetActive (true);
							GetComponent<Tutorial_Section02> ()._HideTu [9].SetActive (false);
							_Pass();
							_PhaseSubPanel[2].SetActive(true);
						}
					}

					if (GetComponent<Tutorial_Section02> ()._Step == 19) {
						if (_CurrectNodePlayerTwo == GetComponent<Tutorial_Section02> ()._Node[3]) {
							//GetComponent<Tutorial_Section02> ()._Tu [6].SetActive (true);
							GetComponent<Tutorial_Section02> ()._Step++;
						}
					}

					if (GetComponent<Tutorial_Section02> ()._Step == 20) {
						if (_CurrectNodePlayerTwo == GetComponent<Tutorial_Section02> ()._Node[1]) {
							//GetComponent<Tutorial_Section02> ()._Tu [6].SetActive (true);
							GetComponent<Tutorial_Section02> ()._Step++;
						}
					}


				}


            }
            //Toast.instance.ShowMessage ("Player 2 move to "+_CurrectNodePlayerTwo.GetComponent<NodeScript>().name,1);

            
            foreach (GameObject z in _ResearchNode)
            {
                z.GetComponent<NodeScript>()._ResetCode();
            }

            _DecreseAction();
            _isMoveMode = false;

			if (GetComponent<TutorialScript> ()._Step == 4) {
				if (_CurrectNodePlayerTwo == GetComponent<TutorialScript> ()._Node08) {
					_TutorialPanel02.SetActive (true);
					GetComponent<TutorialScript> ()._Step++;
				}
			}
        }



       
	}

	public Text _StatePopUp;
	public GameObject TopStatBar;
    public void _DecreseAction()
    {
        _Action--;
		if (_Action <= 0) {
			_PhasePanel.SetActive (true);
			if (GetComponent<TutorialScript> ()._Step != 3&&GetComponent<TutorialScript> ()._Step != 15) {
				Debug.Log ("ASD");
				_PhaseSubPanel [2].SetActive (true);
			}
			_Action = 0;
		}

        _CheckIfShares();
        _CheckCard();
        _CheckCure();

      

        _ActionText.text = "Action : " + _Action + " Lefts";
        _CheckPlayerCard();
        _CheckIsCanMove();

        foreach (CardManager._Card C in GetComponent<CardManager>()._AllCardStore)
        {
            if (!C._isEpidemics)
            {

                C._Node.GetComponent<NodeScript>()._ResetCode();

            }
        }
       
    }

    public Text _ClassName;
    public void _ChangeImage()
    {

        if (_State == 1) // Player 2
        {
            _PhasePanel.SetActive(true);
            _PhaseSubPanel[1].SetActive(true);

            _State = 0;
            switch (_PlayerTwoClass)
            {
                case 0:
                    _ClassName.text = "Madic";
                    _Class.GetComponent<Image>().sprite = _PlayerTwoClassPic[1];
                    break;
                case 1:
                    _ClassName.text = "Dispacther";
                    _Class.GetComponent<Image>().sprite = _PlayerTwoClassPic[2];
                    break;
                case 2:
                    _ClassName.text = "Operational Expert";
                    _Class.GetComponent<Image>().sprite = _PlayerTwoClassPic[3];
                    break;
                default:
                    _Class.GetComponent<Image>().sprite = _PlayerTwoClassPic[0]; // 1
                    break;
            }
        }
        else // Player 2
        {
            _PhasePanel.SetActive(true);
            _PhaseSubPanel[0].SetActive(true);
            _State = 1;
            switch (_PlayerOneClass)
            {
                case 0:
                    _ClassName.text = "Madic";
                    _Class.GetComponent<Image>().sprite = _PlayerOneClassPic[1];
                    break;
                case 1:
                    _ClassName.text = "Dispacther";
                    _Class.GetComponent<Image>().sprite = _PlayerOneClassPic[2];
                    break;
                case 2:
                    _ClassName.text = "Operational Expert";
                    _Class.GetComponent<Image>().sprite = _PlayerOneClassPic[3];
                    break;
                default:
                    _Class.GetComponent<Image>().sprite = _PlayerOneClassPic[0]; // 0
                    break;
            }

            //GetComponent<LogFilesScript> ()._GameTurn++;
        }

        _Action = 4;
        _ActionText.text = "Action : " + _Action + " Lefts";
        _CheckCard();
        _CheckCure();
        _CheckPlayerCard();
        _CheckIfShares();
        _CheckIsCanMove();


    }

    public Text _ActionText;
	public Image _Class;
    public Image _Class_P1;
    public Image _Class_P2;

    public Image _Class_P1_Phase;
    public Image _Class_P2_Phase;
    public bool _isCure;
	public GameObject _BlockPlayerPart;

	private void _Btm(){

		if (_Action != 0) {
			_ActionBtm [0].interactable = true;
			_ActionBtm [8].interactable = true;
			_ActionBtm [5].interactable = false;
			_ActionBtm[6].interactable = false;
			if (_isShared) { // Shares
				_ActionBtm [4].interactable = true;
			} else {
				_ActionBtm [4].interactable = false;
			}


			if (_isCanBuild) {
				_ActionBtm [3].interactable = true;
			} else {
				_ActionBtm [3].interactable = false;
			}

			if (_isCanMove) {

				//_ActionBtm [9].interactable = true;
			} else {
				//_ActionBtm [9].interactable = false;
			}

			if (_State == 1) {

				if (_CurrectNodePlayerOne.GetComponent<NodeScript> ()._Level >= 1) {
					if (_Action != 0) {
						_ActionBtm [1].interactable = true;
					} else {
						_ActionBtm [1].interactable = false;
					}
				}else
				{
					_ActionBtm[1].interactable = false;
				}
			} else {
				if (_CurrectNodePlayerTwo.GetComponent<NodeScript> ()._Level >= 1) {
					if (_Action != 0) {
						_ActionBtm [1].interactable = true;
					} else {
						_ActionBtm [1].interactable = false;
					}


				}else
				{
					_ActionBtm[1].interactable = false;
				}
			}

		}
		else
		{
			_ActionBtm[1].interactable = false;
			_ActionBtm[2].interactable = false;
			_ActionBtm[3].interactable = false;
			_ActionBtm[4].interactable = false;
			_ActionBtm[5].interactable = false;
			_ActionBtm[6].interactable = true;
			//_ActionBtm[7].interactable = false;
			_ActionBtm[9].interactable = false;
			_ActionBtm[0].interactable = false;

		}

	}

	public GameObject[] _ClassText;

	public GameObject _RedTreat;
	public GameObject _BlueTreat;

	public Button _MedicAbiltyUse;
	void FixedUpdate(){
		if(Input.GetMouseButton(1)){
			if (_isMoveMode) {
				
				foreach (CardManager._Card C in GetComponent<CardManager>()._AllCardStore)
				{
					if (!C._isEpidemics)
					{
						C._Node.GetComponent<NodeScript>()._ResetCode();

					}
				}

				_isDirectFilght = false;
				_isMoveMode = false;
			}

			//Toast.instance.ShowMessage ("Cancel Move ode", 2f);
		}

		if (_PlayState==0) {
			if (_State == 1) {
				switch(_PlayerOneClass){
				case 0: // Medice
					_ClassText [0].SetActive (false);
					_ClassText [1].SetActive (false);
					_ClassText [2].SetActive (true);
					break;
				case 1: // Displacer
					_ClassText [0].SetActive (true);
					_ClassText [1].SetActive (false);
					_ClassText [2].SetActive (false);
					break;
				case 2: // OE
					_ClassText [0].SetActive (false);
					_ClassText [1].SetActive (true);
					_ClassText [2].SetActive (false);
					break;
				}
			} else {
				switch(_PlayerTwoClass){
				case 0: // Medice
					_ClassText [0].SetActive (false);
					_ClassText [1].SetActive (false);
					_ClassText [2].SetActive (true);
					break;
				case 1: // Displacer
					_ClassText [0].SetActive (true);
					_ClassText [1].SetActive (false);
					_ClassText [2].SetActive (false);
					break;
				case 2: // OE
					_ClassText [0].SetActive (false);
					_ClassText [1].SetActive (true);
					_ClassText [2].SetActive (false);
					break;
				}
			}
		}

		switch (_PlayState) {
		case 0:

			_BlockPlayerPart.SetActive (false);

			if (GetComponent<TutorialScript> ()._isOnTutorial) {

				//_ActionBtm [8].interactable = false; // Pass
				switch (GetComponent<TutorialScript> ()._Step) {

				case 0:
					_ActionBtm [0].interactable = false; // Move
					_ActionBtm [1].interactable = false; // Treat 
					_ActionBtm [2].interactable = false; // Cure
					_ActionBtm [3].interactable = false; // Build
					_ActionBtm [4].interactable = false; // Shares
					_ActionBtm [5].interactable = false; // Shares
					_ActionBtm [6].interactable = false; // Shares
					_ActionBtm [7].interactable = false; // Shares

					_CardObject [0].GetComponent<Button> ().interactable = false;
					_CardObject [1].GetComponent<Button> ().interactable = false;
					_CardObject [2].GetComponent<Button> ().interactable = false;
					_CardObject [3].GetComponent<Button> ().interactable = false;
					_CardObject [4].GetComponent<Button> ().interactable = false;


					_Btm ();

					break;

				case 1:
					_ActionBtm [0].interactable = true; // Move
					_ActionBtm [1].interactable = false; // Treat 
					_ActionBtm [2].interactable = false; // Cure
					_ActionBtm [3].interactable = false; // Build
					_ActionBtm [4].interactable = false; // Shares
					_ActionBtm [5].interactable = false; // Shares
					_ActionBtm [6].interactable = false; // Shares
					_ActionBtm [7].interactable = false; // Shares

					_CardObject [0].GetComponent<Button> ().interactable = false;
					_CardObject [1].GetComponent<Button> ().interactable = false;
					_CardObject [2].GetComponent<Button> ().interactable = false;
					_CardObject [3].GetComponent<Button> ().interactable = false;
					_CardObject [4].GetComponent<Button> ().interactable = false;

					//_Btm ();
					break;

				case 2:
					_ActionBtm [0].interactable = false; // Move
					_ActionBtm [1].interactable = true; // Treat 
					_ActionBtm [2].interactable = false; // Cure
					_ActionBtm [3].interactable = false; // Build
					_ActionBtm [4].interactable = false; // Shares
					_ActionBtm [5].interactable = false; // Shares
					_ActionBtm [6].interactable = false; // Shares
					_ActionBtm [7].interactable = false; // Shares
					//_Btm ();
					break;

				case 3:
					_ActionBtm [0].interactable = false; // Move
					_ActionBtm [1].interactable = false; // Treat 
					_ActionBtm [2].interactable = false; // Cure
					_ActionBtm [3].interactable = false; // Build
					_ActionBtm [4].interactable = false; // Shares
					_ActionBtm [5].interactable = false; // Shares
					_ActionBtm [6].interactable = false; // Shares
					_ActionBtm [7].interactable = false; // Shares

					_CardObject [0].GetComponent<Button> ().interactable = true;

					//_Btm ();
					break;

				case 4:
					_ActionBtm [0].interactable = false; // Move
					_ActionBtm [1].interactable = false; // Treat 
					_ActionBtm [2].interactable = false; // Cure
					_ActionBtm [3].interactable = false; // Build
					_ActionBtm [4].interactable = false; // Shares
					_ActionBtm [5].interactable = false; // Shares
					_ActionBtm [6].interactable = false; // Shares
					_ActionBtm [7].interactable = false; // Shares

					_CardObject [0].GetComponent<Button> ().interactable = false;

				//_Btm ();
					break;

				case 5:
					_ActionBtm [0].interactable = true; // Move
					_ActionBtm [1].interactable = false; // Treat 
					_ActionBtm [2].interactable = false; // Cure
					_ActionBtm [3].interactable = false; // Build
					_ActionBtm [4].interactable = false; // Shares
					_ActionBtm [6].interactable = false; // Shares
					_ActionBtm [7].interactable = false; // Shares

					_CardObject [0].GetComponent<Button> ().interactable = false;
					_CardObject [1].GetComponent<Button> ().interactable = false;
					_CardObject [2].GetComponent<Button> ().interactable = false;
					_CardObject [3].GetComponent<Button> ().interactable = false;
					_CardObject [4].GetComponent<Button> ().interactable = false;

					//_Btm ();
					break;

				case 6:
					_ActionBtm [0].interactable = false; // Move
					_ActionBtm [1].interactable = true; // Treat 
					_ActionBtm [2].interactable = false; // Cure
					_ActionBtm [3].interactable = false; // Build
					_ActionBtm [4].interactable = false; // Shares
					_ActionBtm [5].interactable = false; // Shares
					_ActionBtm [6].interactable = false; // Shares
					_ActionBtm [7].interactable = false; // Shares

					_CardObject [0].GetComponent<Button> ().interactable = false;
					_CardObject [1].GetComponent<Button> ().interactable = false;
					_CardObject [2].GetComponent<Button> ().interactable = false;
					_CardObject [3].GetComponent<Button> ().interactable = false;
					_CardObject [4].GetComponent<Button> ().interactable = false;
					//_Btm ();
					break;
				case 7:
					_ActionBtm [0].interactable = false; // Move
					_ActionBtm [1].interactable = true; // Treat 
					_ActionBtm [2].interactable = false; // Cure
					_ActionBtm [3].interactable = false; // Build
					_ActionBtm [4].interactable = false; // Shares
					_ActionBtm [5].interactable = false; // Shares
					_ActionBtm [6].interactable = false; // Shares
					_ActionBtm [7].interactable = false; // Shares
					//_ActionBtm [7].interactable = false; // Shares

					_CardObject [0].GetComponent<Button> ().interactable = false;
					_CardObject [1].GetComponent<Button> ().interactable = false;
					_CardObject [2].GetComponent<Button> ().interactable = false;
					_CardObject [3].GetComponent<Button> ().interactable = false;
					_CardObject [4].GetComponent<Button> ().interactable = false;
					//_Btm ();
					break;

				case 8:
					_ActionBtm [0].interactable = true; // Move
					_ActionBtm [1].interactable = false; // Treat 
					_ActionBtm [2].interactable = false; // Cure
					_ActionBtm [3].interactable = false; // Build
					_ActionBtm [4].interactable = false; // Shares
					_ActionBtm [5].interactable = false; // Shares
					_ActionBtm [6].interactable = false; // Shares
					_ActionBtm [7].interactable = false; // Shares

					_CardObject [0].GetComponent<Button> ().interactable = false;
					_CardObject [1].GetComponent<Button> ().interactable = false;
					_CardObject [2].GetComponent<Button> ().interactable = false;
					_CardObject [3].GetComponent<Button> ().interactable = false;
					_CardObject [4].GetComponent<Button> ().interactable = false;
					//_Btm ();
					break;

				case 9:
					_ActionBtm [0].interactable = true; // Move
					_ActionBtm [1].interactable = false; // Treat 
					_ActionBtm [2].interactable = false; // Cure
					_ActionBtm [3].interactable = false; // Build
					_ActionBtm [4].interactable = false; // Shares
					_ActionBtm [5].interactable = false; // Shares
					_ActionBtm [6].interactable = false; // Shares
					_ActionBtm [7].interactable = false; // Shares

					_CardObject [0].GetComponent<Button> ().interactable = false;
					_CardObject [1].GetComponent<Button> ().interactable = false;
					_CardObject [2].GetComponent<Button> ().interactable = false;
					_CardObject [3].GetComponent<Button> ().interactable = false;
					_CardObject [4].GetComponent<Button> ().interactable = false;
					//_Btm ();
					break;

				case 10:
				_ActionBtm [0].interactable = false; // Move
					_ActionBtm [1].interactable = true; // Treat 
					_ActionBtm [2].interactable = false; // Cure
					_ActionBtm [3].interactable = false; // Build
					_ActionBtm [4].interactable = false; // Shares
					_ActionBtm [5].interactable = false; // Shares
					_ActionBtm [6].interactable = false; // Shares
					_ActionBtm [7].interactable = false; // Shares

					_CardObject [0].GetComponent<Button> ().interactable = false;
					_CardObject [1].GetComponent<Button> ().interactable = false;
					_CardObject [2].GetComponent<Button> ().interactable = false;
					_CardObject [3].GetComponent<Button> ().interactable = false;
					_CardObject [4].GetComponent<Button> ().interactable = false;

					break;

				case 11:
					_ActionBtm [0].interactable = false; // Move
					_ActionBtm [1].interactable = false; // Treat 
					_ActionBtm [2].interactable = false; // Cure
					_ActionBtm [3].interactable = true; // Build
					_ActionBtm [4].interactable = false; // Shares
					_ActionBtm [5].interactable = false; // Shares
					_ActionBtm [6].interactable = false; // Shares
					_ActionBtm [7].interactable = false; // Shares

					_CardObject [0].GetComponent<Button> ().interactable = false;
					_CardObject [1].GetComponent<Button> ().interactable = false;
					_CardObject [2].GetComponent<Button> ().interactable = false;
					_CardObject [3].GetComponent<Button> ().interactable = false;
					_CardObject [4].GetComponent<Button> ().interactable = false;

					break;

				case 12:
					_ActionBtm [0].interactable = false; // Move
					_ActionBtm [1].interactable = false; // Treat 
					_ActionBtm [2].interactable = false; // Cure
					_ActionBtm [3].interactable = false; // Build
					_ActionBtm [4].interactable = false; // Shares
					_ActionBtm [5].interactable = false; // Shares
					_ActionBtm [6].interactable = false; // Shares
					_ActionBtm [7].interactable = false; // Shares

					_CardObject [0].GetComponent<Button> ().interactable = false;
					_CardObject [1].GetComponent<Button> ().interactable = false;
					_CardObject [2].GetComponent<Button> ().interactable = true;
					_CardObject [3].GetComponent<Button> ().interactable = false;
					_CardObject [4].GetComponent<Button> ().interactable = false;

					break;

				case 13:
					_ActionBtm [0].interactable = false; // Move
					_ActionBtm [1].interactable = true; // Treat 
					_ActionBtm [2].interactable = false; // Cure
					_ActionBtm [3].interactable = false; // Build
					_ActionBtm [4].interactable = false; // Shares
					_ActionBtm [5].interactable = false; // Shares
					_ActionBtm [6].interactable = false; // Shares
					_ActionBtm [7].interactable = false; // Shares

					_CardObject [0].GetComponent<Button> ().interactable = false;
					_CardObject [1].GetComponent<Button> ().interactable = false;
					_CardObject [2].GetComponent<Button> ().interactable = false;
					_CardObject [3].GetComponent<Button> ().interactable = false;
					_CardObject [4].GetComponent<Button> ().interactable = false;
					//_Btm ();
					break;

				case 14:
					_ActionBtm [0].interactable = true; // Move
					_ActionBtm [1].interactable = false; // Treat 
					_ActionBtm [2].interactable = false; // Cure
					//_ActionBtm [3].interactable = false; // Build
					_ActionBtm [4].interactable = false; // Shares
					_ActionBtm [5].interactable = false; // Shares
					_ActionBtm [6].interactable = false; // Shares
					_ActionBtm [7].interactable = false; // Shares

					_CardObject [0].GetComponent<Button> ().interactable = false;
					_CardObject [1].GetComponent<Button> ().interactable = false;
					_CardObject [2].GetComponent<Button> ().interactable = false;
					_CardObject [3].GetComponent<Button> ().interactable = false;
					_CardObject [4].GetComponent<Button> ().interactable = false;
					//_Btm ();
					break;

				case 15:
					_ActionBtm [0].interactable = false; // Move
					_ActionBtm [1].interactable = false; // Treat 
					_ActionBtm [2].interactable = false; // Cure
					_ActionBtm [3].interactable = true; // Build
					_ActionBtm [4].interactable = false; // Shares
					_ActionBtm [5].interactable = false; // Shares
					_ActionBtm [6].interactable = false; // Shares
					_ActionBtm [7].interactable = false; // Shares

					_CardObject [0].GetComponent<Button> ().interactable = false;
					_CardObject [1].GetComponent<Button> ().interactable = false;
					_CardObject [2].GetComponent<Button> ().interactable = false;
					_CardObject [3].GetComponent<Button> ().interactable = false;
					_CardObject [4].GetComponent<Button> ().interactable = false;
					//_Btm ();
					break;

				case 16:
					_ActionBtm [0].interactable = true; // Move
					_ActionBtm [1].interactable = false; // Treat 
					_ActionBtm [2].interactable = false; // Cure
					_ActionBtm [3].interactable = false; // Build
					_ActionBtm [4].interactable = false; // Shares
					_ActionBtm [5].interactable = false; // Shares
					_ActionBtm [6].interactable = false; // Shares
					_ActionBtm [7].interactable = false; // Shares

					_CardObject [0].GetComponent<Button> ().interactable = false;
					_CardObject [1].GetComponent<Button> ().interactable = false;
					_CardObject [2].GetComponent<Button> ().interactable = false;
					_CardObject [3].GetComponent<Button> ().interactable = false;
					_CardObject [4].GetComponent<Button> ().interactable = false;
					//_Btm ();
					break;

				case 17:
					_ActionBtm [0].interactable = true; // Move
					_ActionBtm [1].interactable = false; // Treat 
					_ActionBtm [2].interactable = false; // Cure
					_ActionBtm [3].interactable = false; // Build
					_ActionBtm [4].interactable = false; // Shares
					_ActionBtm [5].interactable = false; // Shares
					_ActionBtm [6].interactable = false; // Shares
					_ActionBtm [7].interactable = false; // Shares

					_CardObject [0].GetComponent<Button> ().interactable = false;
					_CardObject [1].GetComponent<Button> ().interactable = false;
					_CardObject [2].GetComponent<Button> ().interactable = false;
					_CardObject [3].GetComponent<Button> ().interactable = false;
					_CardObject [4].GetComponent<Button> ().interactable = false;
					//_Btm ();
					break;

				case 18:
					_ActionBtm [0].interactable = false; // Move
					_ActionBtm [1].interactable = true; // Treat 
					_ActionBtm [2].interactable = false; // Cure
					_ActionBtm [3].interactable = false; // Build
					_ActionBtm [4].interactable = false; // Shares
					_ActionBtm [5].interactable = false; // Shares
					_ActionBtm [6].interactable = false; // Shares
					_ActionBtm [7].interactable = false; // Shares

					_CardObject [0].GetComponent<Button> ().interactable = false;
					_CardObject [1].GetComponent<Button> ().interactable = false;
					_CardObject [2].GetComponent<Button> ().interactable = false;
					_CardObject [3].GetComponent<Button> ().interactable = false;
					_CardObject [4].GetComponent<Button> ().interactable = false;
					//_Btm ();
					break;

				case 20:
					_ActionBtm [0].interactable = false; // Move
					_ActionBtm [1].interactable = false; // Treat 
					_ActionBtm [2].interactable = true; // Cure
					_ActionBtm [3].interactable = false; // Build
					_ActionBtm [4].interactable = false; // Shares
					_ActionBtm [5].interactable = false; // Shares
					_ActionBtm [6].interactable = false; // Shares
					_ActionBtm [7].interactable = false; // Shares

					_CardObject [0].GetComponent<Button> ().interactable = false;
					_CardObject [1].GetComponent<Button> ().interactable = false;
					_CardObject [2].GetComponent<Button> ().interactable = false;
					_CardObject [3].GetComponent<Button> ().interactable = false;
					_CardObject [4].GetComponent<Button> ().interactable = false;
					//_Btm ();
					break;

				case 21:
					_ActionBtm [0].interactable = true; // Move
					_ActionBtm [1].interactable = false; // Treat 
					_ActionBtm [2].interactable = false; // Cure
					_ActionBtm [3].interactable = false; // Build
					_ActionBtm [4].interactable = false; // Shares
					_ActionBtm [5].interactable = false; // Shares
					_ActionBtm [6].interactable = false; // Shares
					_ActionBtm [7].interactable = false; // Shares

					_CardObject [0].GetComponent<Button> ().interactable = false;
					_CardObject [1].GetComponent<Button> ().interactable = false;
					_CardObject [2].GetComponent<Button> ().interactable = false;
					_CardObject [3].GetComponent<Button> ().interactable = false;
					_CardObject [4].GetComponent<Button> ().interactable = false;
					//_Btm ();
					break;

				case 22:
					_ActionBtm [0].interactable = true; // Move
					_ActionBtm [1].interactable = false; // Treat 
					_ActionBtm [2].interactable = false; // Cure
					_ActionBtm [3].interactable = false; // Build
					_ActionBtm [4].interactable = false; // Shares
					_ActionBtm [5].interactable = false; // Shares
					_ActionBtm [6].interactable = false; // Shares
					_ActionBtm [7].interactable = false; // Shares

					_CardObject [0].GetComponent<Button> ().interactable = false;
					_CardObject [1].GetComponent<Button> ().interactable = false;
					_CardObject [2].GetComponent<Button> ().interactable = false;
					_CardObject [3].GetComponent<Button> ().interactable = false;
					_CardObject [4].GetComponent<Button> ().interactable = false;

					break;

				case 24:
					_ActionBtm [0].interactable = false; // Move
					_ActionBtm [1].interactable = true; // Treat 
					_ActionBtm [2].interactable = false; // Cure
					_ActionBtm [3].interactable = false; // Build
					_ActionBtm [4].interactable = false; // Shares
					_ActionBtm [5].interactable = false; // Shares
					_ActionBtm [6].interactable = false; // Shares
					_ActionBtm [7].interactable = false; // Shares

					_CardObject [0].GetComponent<Button> ().interactable = false;
					_CardObject [1].GetComponent<Button> ().interactable = false;
					_CardObject [2].GetComponent<Button> ().interactable = false;
					_CardObject [3].GetComponent<Button> ().interactable = false;
					_CardObject [4].GetComponent<Button> ().interactable = false;

					break;

				case 25:
					_ActionBtm [0].interactable = true; // Move
					_ActionBtm [1].interactable = false; // Treat 
					_ActionBtm [2].interactable = false; // Cure
					_ActionBtm [3].interactable = false; // Build
					_ActionBtm [4].interactable = false; // Shares
					_ActionBtm [5].interactable = false; // Shares
					_ActionBtm [6].interactable = false; // Shares
					_ActionBtm [7].interactable = false; // Shares

					_CardObject [0].GetComponent<Button> ().interactable = false;
					_CardObject [1].GetComponent<Button> ().interactable = false;
					_CardObject [2].GetComponent<Button> ().interactable = false;
					_CardObject [3].GetComponent<Button> ().interactable = false;
					_CardObject [4].GetComponent<Button> ().interactable = false;

					break;

				case 26:
					_ActionBtm [0].interactable = false; // Move
					_ActionBtm [1].interactable = true; // Treat 
					_ActionBtm [2].interactable = false; // Cure
					_ActionBtm [3].interactable = false; // Build
					_ActionBtm [4].interactable = false; // Shares
					_ActionBtm [5].interactable = false; // Shares
					_ActionBtm [6].interactable = false; // Shares
					_ActionBtm [7].interactable = false; // Shares

					_CardObject [0].GetComponent<Button> ().interactable = false;
					_CardObject [1].GetComponent<Button> ().interactable = false;
					_CardObject [2].GetComponent<Button> ().interactable = false;
					_CardObject [3].GetComponent<Button> ().interactable = false;
					_CardObject [4].GetComponent<Button> ().interactable = false;

					break;

				case 27:
					_ActionBtm [0].interactable = false; // Move
					_ActionBtm [1].interactable = true; // Treat 
					_ActionBtm [2].interactable = false; // Cure
					_ActionBtm [3].interactable = false; // Build
					_ActionBtm [4].interactable = false; // Shares
					_ActionBtm [5].interactable = false; // Shares
					_ActionBtm [6].interactable = false; // Shares
					_ActionBtm [7].interactable = false; // Shares

					_CardObject [0].GetComponent<Button> ().interactable = false;
					_CardObject [1].GetComponent<Button> ().interactable = false;
					_CardObject [2].GetComponent<Button> ().interactable = false;
					_CardObject [3].GetComponent<Button> ().interactable = false;
					_CardObject [4].GetComponent<Button> ().interactable = false;

					break;

				case 28:
					_ActionBtm [0].interactable = true; // Move
					_ActionBtm [1].interactable = false; // Treat 
					_ActionBtm [2].interactable = false; // Cure
					_ActionBtm [3].interactable = false; // Build
					_ActionBtm [4].interactable = false; // Shares
					_ActionBtm [5].interactable = false; // Shares
					_ActionBtm [6].interactable = false; // Shares
					_ActionBtm [7].interactable = false; // Shares

					_CardObject [0].GetComponent<Button> ().interactable = false;
					_CardObject [1].GetComponent<Button> ().interactable = false;
					_CardObject [2].GetComponent<Button> ().interactable = false;
					_CardObject [3].GetComponent<Button> ().interactable = false;
					_CardObject [4].GetComponent<Button> ().interactable = false;

					break;

				case 29:
					_ActionBtm [0].interactable = false; // Move
					_ActionBtm [1].interactable = true; // Treat 
					_ActionBtm [2].interactable = false; // Cure
					_ActionBtm [3].interactable = false; // Build
					_ActionBtm [4].interactable = false; // Shares
					_ActionBtm [5].interactable = false; // Shares
					_ActionBtm [6].interactable = false; // Shares
					_ActionBtm [7].interactable = false; // Shares

					_CardObject [0].GetComponent<Button> ().interactable = false;
					_CardObject [1].GetComponent<Button> ().interactable = false;
					_CardObject [2].GetComponent<Button> ().interactable = false;
					_CardObject [3].GetComponent<Button> ().interactable = false;
					_CardObject [4].GetComponent<Button> ().interactable = false;

					break;

				case 30:
					_ActionBtm [0].interactable = false; // Move
					_ActionBtm [1].interactable = true; // Treat 
					_ActionBtm [2].interactable = false; // Cure
					_ActionBtm [3].interactable = false; // Build
					_ActionBtm [4].interactable = false; // Shares
					_ActionBtm [5].interactable = false; // Shares
					_ActionBtm [6].interactable = false; // Shares
					_ActionBtm [7].interactable = false; // Shares

					_CardObject [0].GetComponent<Button> ().interactable = false;
					_CardObject [1].GetComponent<Button> ().interactable = false;
					_CardObject [2].GetComponent<Button> ().interactable = false;
					_CardObject [3].GetComponent<Button> ().interactable = false;
					_CardObject [4].GetComponent<Button> ().interactable = false;

					break;

				case 31:
					_ActionBtm [0].interactable = true; // Move
					_ActionBtm [1].interactable = false; // Treat 
					_ActionBtm [2].interactable = false; // Cure
					_ActionBtm [3].interactable = false; // Build
					_ActionBtm [4].interactable = false; // Shares
					_ActionBtm [5].interactable = false; // Shares
					_ActionBtm [6].interactable = false; // Shares
					_ActionBtm [7].interactable = false; // Shares

					_CardObject [0].GetComponent<Button> ().interactable = false;
					_CardObject [1].GetComponent<Button> ().interactable = false;
					_CardObject [2].GetComponent<Button> ().interactable = false;
					_CardObject [3].GetComponent<Button> ().interactable = false;
					_CardObject [4].GetComponent<Button> ().interactable = false;

					break;

				case 32:
					_ActionBtm [0].interactable = true; // Move
					_ActionBtm [1].interactable = false; // Treat 
					_ActionBtm [2].interactable = false; // Cure
					_ActionBtm [3].interactable = false; // Build
					_ActionBtm [4].interactable = false; // Shares
					_ActionBtm [5].interactable = false; // Shares
					_ActionBtm [6].interactable = false; // Shares
					_ActionBtm [7].interactable = false; // Shares

					_CardObject [0].GetComponent<Button> ().interactable = false;
					_CardObject [1].GetComponent<Button> ().interactable = false;
					_CardObject [2].GetComponent<Button> ().interactable = false;
					_CardObject [3].GetComponent<Button> ().interactable = false;
					_CardObject [4].GetComponent<Button> ().interactable = false;

					break;

				case 33:
					_ActionBtm [0].interactable = false; // Move
					_ActionBtm [1].interactable = false; // Treat 
					_ActionBtm [2].interactable = false; // Cure
					_ActionBtm [3].interactable = true; // Build
					_ActionBtm [4].interactable = false; // Shares
					_ActionBtm [5].interactable = false; // Shares
					_ActionBtm [6].interactable = false; // Shares
					_ActionBtm [7].interactable = false; // Shares

					_CardObject [0].GetComponent<Button> ().interactable = false;
					_CardObject [1].GetComponent<Button> ().interactable = false;
					_CardObject [2].GetComponent<Button> ().interactable = false;
					_CardObject [3].GetComponent<Button> ().interactable = false;
					_CardObject [4].GetComponent<Button> ().interactable = false;

					break;


				case 34:
					_ActionBtm [0].interactable = false; // Move
					_ActionBtm [1].interactable = false; // Treat 
					_ActionBtm [2].interactable = true; // Cure
					_ActionBtm [3].interactable = false; // Build
					_ActionBtm [4].interactable = false; // Shares
					_ActionBtm [5].interactable = false; // Shares
					_ActionBtm [6].interactable = false; // Shares
					_ActionBtm [7].interactable = false; // Shares

					_CardObject [0].GetComponent<Button> ().interactable = false;
					_CardObject [1].GetComponent<Button> ().interactable = false;
					_CardObject [2].GetComponent<Button> ().interactable = false;
					_CardObject [3].GetComponent<Button> ().interactable = false;
					_CardObject [4].GetComponent<Button> ().interactable = false;

					break;



				}
			} else if(GetComponent<Tutorial_Section02>()._Section02){
				switch (GetComponent<Tutorial_Section02> ()._Step) {
				case 0:
					_ActionBtm [0].interactable = false; // Move
					_ActionBtm [1].interactable = true; // Treat 
					_ActionBtm [2].interactable = false; // Cure
					_ActionBtm [3].interactable = false; // Build
					_ActionBtm [4].interactable = false; // Shares
					_ActionBtm [5].interactable = false; // Shares
					_ActionBtm [6].interactable = false; // Shares
					_ActionBtm [7].interactable = false; // Shares

					_CardObject [0].GetComponent<Button> ().interactable = false;
					_CardObject [1].GetComponent<Button> ().interactable = false;
					_CardObject [2].GetComponent<Button> ().interactable = false;
					_CardObject [3].GetComponent<Button> ().interactable = false;
					_CardObject [4].GetComponent<Button> ().interactable = false;

					break;

				case 1:
					_ActionBtm [0].interactable = true; // Move
					_ActionBtm [1].interactable = false; // Treat 
					_ActionBtm [2].interactable = false; // Cure
					_ActionBtm [3].interactable = false; // Build
					_ActionBtm [4].interactable = false; // Shares
					_ActionBtm [5].interactable = false; // Shares
					_ActionBtm [6].interactable = false; // Shares
					_ActionBtm [7].interactable = false; // Shares

					_CardObject [0].GetComponent<Button> ().interactable = false;
					_CardObject [1].GetComponent<Button> ().interactable = false;
					_CardObject [2].GetComponent<Button> ().interactable = false;
					_CardObject [3].GetComponent<Button> ().interactable = false;
					_CardObject [4].GetComponent<Button> ().interactable = false;

					break;

				case 2:
					_ActionBtm [0].interactable = false; // Move
					_ActionBtm [1].interactable = false; // Treat 
					_ActionBtm [2].interactable = false; // Cure
					_ActionBtm [3].interactable = true; // Build
					_ActionBtm [4].interactable = false; // Shares
					_ActionBtm [5].interactable = false; // Shares
					_ActionBtm [6].interactable = false; // Shares
					_ActionBtm [7].interactable = false; // Shares

					_CardObject [0].GetComponent<Button> ().interactable = false;
					_CardObject [1].GetComponent<Button> ().interactable = false;
					_CardObject [2].GetComponent<Button> ().interactable = false;
					_CardObject [3].GetComponent<Button> ().interactable = false;
					_CardObject [4].GetComponent<Button> ().interactable = false;

					break;

				case 3:
					_ActionBtm [0].interactable = true; // Move
					_ActionBtm [1].interactable = false; // Treat 
					_ActionBtm [2].interactable = false; // Cure
					_ActionBtm [3].interactable = false; // Build
					_ActionBtm [4].interactable = false; // Shares
					_ActionBtm [5].interactable = false; // Shares
					_ActionBtm [6].interactable = false; // Shares
					_ActionBtm [7].interactable = false; // Shares

					_CardObject [0].GetComponent<Button> ().interactable = false;
					_CardObject [1].GetComponent<Button> ().interactable = false;
					_CardObject [2].GetComponent<Button> ().interactable = false;
					_CardObject [3].GetComponent<Button> ().interactable = false;
					_CardObject [4].GetComponent<Button> ().interactable = false;

					break;

				case 4:
					_ActionBtm [0].interactable = true; // Move
					_ActionBtm [1].interactable = false; // Treat 
					_ActionBtm [2].interactable = false; // Cure
					_ActionBtm [3].interactable = false; // Build
					_ActionBtm [4].interactable = false; // Shares
					_ActionBtm [5].interactable = false; // Shares
					_ActionBtm [6].interactable = false; // Shares
					_ActionBtm [7].interactable = false; // Shares

					_CardObject [0].GetComponent<Button> ().interactable = false;
					_CardObject [1].GetComponent<Button> ().interactable = false;
					_CardObject [2].GetComponent<Button> ().interactable = false;
					_CardObject [3].GetComponent<Button> ().interactable = false;
					_CardObject [4].GetComponent<Button> ().interactable = false;

					break;

				case 7:
					_ActionBtm [0].interactable = false; // Move
					_ActionBtm [1].interactable = false; // Treat 
					_ActionBtm [2].interactable = false; // Cure
					_ActionBtm [3].interactable = true; // Build
					_ActionBtm [4].interactable = false; // Shares
					_ActionBtm [5].interactable = false; // Shares
					_ActionBtm [6].interactable = false; // Shares
					_ActionBtm [7].interactable = false; // Shares

					_CardObject [0].GetComponent<Button> ().interactable = false;
					_CardObject [1].GetComponent<Button> ().interactable = false;
					_CardObject [2].GetComponent<Button> ().interactable = false;
					_CardObject [3].GetComponent<Button> ().interactable = false;
					_CardObject [4].GetComponent<Button> ().interactable = false;

					break;

				case 8:
					_ActionBtm [0].interactable = true; // Move
					_ActionBtm [1].interactable = false; // Treat 
					_ActionBtm [2].interactable = false; // Cure
					_ActionBtm [3].interactable = false; // Build
					_ActionBtm [4].interactable = false; // Shares
					_ActionBtm [5].interactable = false; // Shares
					_ActionBtm [6].interactable = false; // Shares
					_ActionBtm [7].interactable = false; // Shares

					_CardObject [0].GetComponent<Button> ().interactable = false;
					_CardObject [1].GetComponent<Button> ().interactable = false;
					_CardObject [2].GetComponent<Button> ().interactable = false;
					_CardObject [3].GetComponent<Button> ().interactable = false;
					_CardObject [4].GetComponent<Button> ().interactable = false;

					break;

				case 9:
					_ActionBtm [0].interactable = false; // Move
					_ActionBtm [1].interactable = true; // Treat 
					_ActionBtm [2].interactable = false; // Cure
					_ActionBtm [3].interactable = false; // Build
					_ActionBtm [4].interactable = false; // Shares
					_ActionBtm [5].interactable = false; // Shares
					_ActionBtm [6].interactable = false; // Shares
					_ActionBtm [7].interactable = false; // Shares

					_CardObject [0].GetComponent<Button> ().interactable = false;
					_CardObject [1].GetComponent<Button> ().interactable = false;
					_CardObject [2].GetComponent<Button> ().interactable = false;
					_CardObject [3].GetComponent<Button> ().interactable = false;
					_CardObject [4].GetComponent<Button> ().interactable = false;

					break;

				case 10:
					_ActionBtm [0].interactable = true; // Move
					_ActionBtm [1].interactable = false; // Treat 
					_ActionBtm [2].interactable = false; // Cure
					_ActionBtm [3].interactable = false; // Build
					_ActionBtm [4].interactable = false; // Shares
					_ActionBtm [5].interactable = false; // Shares
					_ActionBtm [6].interactable = false; // Shares
					_ActionBtm [7].interactable = false; // Shares

					_CardObject [0].GetComponent<Button> ().interactable = false;
					_CardObject [1].GetComponent<Button> ().interactable = false;
					_CardObject [2].GetComponent<Button> ().interactable = false;
					_CardObject [3].GetComponent<Button> ().interactable = false;
					_CardObject [4].GetComponent<Button> ().interactable = false;

					break;

				case 11:
					_ActionBtm [0].interactable = true; // Move
					_ActionBtm [1].interactable = false; // Treat 
					_ActionBtm [2].interactable = false; // Cure
					_ActionBtm [3].interactable = false; // Build
					_ActionBtm [4].interactable = false; // Shares
					_ActionBtm [5].interactable = false; // Shares
					_ActionBtm [6].interactable = false; // Shares
					_ActionBtm [7].interactable = false; // Shares

					_CardObject [0].GetComponent<Button> ().interactable = false;
					_CardObject [1].GetComponent<Button> ().interactable = false;
					_CardObject [2].GetComponent<Button> ().interactable = false;
					_CardObject [3].GetComponent<Button> ().interactable = false;
					_CardObject [4].GetComponent<Button> ().interactable = false;

					break;

				case 12:
					_ActionBtm [0].interactable = false; // Move
					_ActionBtm [1].interactable = false; // Treat 
					_ActionBtm [2].interactable = false; // Cure
					_ActionBtm [3].interactable = false; // Build
					_ActionBtm [4].interactable = true; // Shares
					_ActionBtm [5].interactable = false; // Shares
					_ActionBtm [6].interactable = false; // Shares
					_ActionBtm [7].interactable = false; // Shares

					_CardObject [0].GetComponent<Button> ().interactable = false;
					_CardObject [1].GetComponent<Button> ().interactable = false;
					_CardObject [2].GetComponent<Button> ().interactable = false;
					_CardObject [3].GetComponent<Button> ().interactable = false;
					_CardObject [4].GetComponent<Button> ().interactable = false;

					break;

				case 13:
					_ActionBtm [0].interactable = true; // Move
					_ActionBtm [1].interactable = false; // Treat 
					_ActionBtm [2].interactable = false; // Cure
					_ActionBtm [3].interactable = false; // Build
					_ActionBtm [4].interactable = false; // Shares
					_ActionBtm [5].interactable = false; // Shares
					_ActionBtm [6].interactable = false; // Shares
					_ActionBtm [7].interactable = false; // Shares

					_CardObject [0].GetComponent<Button> ().interactable = false;
					_CardObject [1].GetComponent<Button> ().interactable = false;
					_CardObject [2].GetComponent<Button> ().interactable = false;
					_CardObject [3].GetComponent<Button> ().interactable = false;
					_CardObject [4].GetComponent<Button> ().interactable = false;

					break;

				case 14:
					_ActionBtm [0].interactable = true; // Move
					_ActionBtm [1].interactable = false; // Treat 
					_ActionBtm [2].interactable = false; // Cure
					_ActionBtm [3].interactable = false; // Build
					_ActionBtm [4].interactable = false; // Shares
					_ActionBtm [5].interactable = false; // Shares
					_ActionBtm [6].interactable = false; // Shares
					_ActionBtm [7].interactable = false; // Shares

					_CardObject [0].GetComponent<Button> ().interactable = false;
					_CardObject [1].GetComponent<Button> ().interactable = false;
					_CardObject [2].GetComponent<Button> ().interactable = false;
					_CardObject [3].GetComponent<Button> ().interactable = false;
					_CardObject [4].GetComponent<Button> ().interactable = false;

					break;


				case 15:
					_ActionBtm [0].interactable = true; // Move
					_ActionBtm [1].interactable = false; // Treat 
					_ActionBtm [2].interactable = false; // Cure
					_ActionBtm [3].interactable = false; // Build
					_ActionBtm [4].interactable = false; // Shares
					_ActionBtm [5].interactable = false; // Shares
					_ActionBtm [6].interactable = false; // Shares
					_ActionBtm [7].interactable = false; // Shares

					_CardObject [0].GetComponent<Button> ().interactable = false;
					_CardObject [1].GetComponent<Button> ().interactable = false;
					_CardObject [2].GetComponent<Button> ().interactable = false;
					_CardObject [3].GetComponent<Button> ().interactable = false;
					_CardObject [4].GetComponent<Button> ().interactable = false;

					break;

				case 16:
					_ActionBtm [0].interactable = false; // Move
					_ActionBtm [1].interactable = false; // Treat 
					_ActionBtm [2].interactable = true; // Cure
					_ActionBtm [3].interactable = false; // Build
					_ActionBtm [4].interactable = false; // Shares
					_ActionBtm [5].interactable = false; // Shares
					_ActionBtm [6].interactable = false; // Shares
					_ActionBtm [7].interactable = false; // Shares

					_CardObject [0].GetComponent<Button> ().interactable = false;
					_CardObject [1].GetComponent<Button> ().interactable = false;
					_CardObject [2].GetComponent<Button> ().interactable = false;
					_CardObject [3].GetComponent<Button> ().interactable = false;
					_CardObject [4].GetComponent<Button> ().interactable = false;

					break;

				case 17:
					_ActionBtm [0].interactable = true; // Move
					_ActionBtm [1].interactable = false; // Treat 
					_ActionBtm [2].interactable = false; // Cure
					_ActionBtm [3].interactable = false; // Build
					_ActionBtm [4].interactable = false; // Shares
					_ActionBtm [5].interactable = false; // Shares
					_ActionBtm [6].interactable = false; // Shares
					_ActionBtm [7].interactable = false; // Shares

					_CardObject [0].GetComponent<Button> ().interactable = false;
					_CardObject [1].GetComponent<Button> ().interactable = false;
					_CardObject [2].GetComponent<Button> ().interactable = false;
					_CardObject [3].GetComponent<Button> ().interactable = false;
					_CardObject [4].GetComponent<Button> ().interactable = false;

					break;

				case 19:
					_ActionBtm [0].interactable = true; // Move
					_ActionBtm [1].interactable = false; // Treat 
					_ActionBtm [2].interactable = false; // Cure
					_ActionBtm [3].interactable = false; // Build
					_ActionBtm [4].interactable = false; // Shares
					_ActionBtm [5].interactable = false; // Shares
					_ActionBtm [6].interactable = false; // Shares
					_ActionBtm [7].interactable = false; // Shares

					_CardObject [0].GetComponent<Button> ().interactable = false;
					_CardObject [1].GetComponent<Button> ().interactable = false;
					_CardObject [2].GetComponent<Button> ().interactable = false;
					_CardObject [3].GetComponent<Button> ().interactable = false;
					_CardObject [4].GetComponent<Button> ().interactable = false;

					break;

				case 20:
					_ActionBtm [0].interactable = true; // Move
					_ActionBtm [1].interactable = false; // Treat 
					_ActionBtm [2].interactable = false; // Cure
					_ActionBtm [3].interactable = false; // Build
					_ActionBtm [4].interactable = false; // Shares
					_ActionBtm [5].interactable = false; // Shares
					_ActionBtm [6].interactable = false; // Shares
					_ActionBtm [7].interactable = false; // Shares

					_CardObject [0].GetComponent<Button> ().interactable = false;
					_CardObject [1].GetComponent<Button> ().interactable = false;
					_CardObject [2].GetComponent<Button> ().interactable = false;
					_CardObject [3].GetComponent<Button> ().interactable = false;
					_CardObject [4].GetComponent<Button> ().interactable = false;

					break;

				case 21:
					_ActionBtm [0].interactable = false; // Move
					_ActionBtm [1].interactable = true; // Treat 
					_ActionBtm [2].interactable = false; // Cure
					_ActionBtm [3].interactable = false; // Build
					_ActionBtm [4].interactable = false; // Shares
					_ActionBtm [5].interactable = false; // Shares
					_ActionBtm [6].interactable = false; // Shares
					_ActionBtm [7].interactable = false; // Shares

					_CardObject [0].GetComponent<Button> ().interactable = false;
					_CardObject [1].GetComponent<Button> ().interactable = false;
					_CardObject [2].GetComponent<Button> ().interactable = false;
					_CardObject [3].GetComponent<Button> ().interactable = false;
					_CardObject [4].GetComponent<Button> ().interactable = false;

					break;

				case 23:
					_ActionBtm [0].interactable = true; // Move
					_ActionBtm [1].interactable = false; // Treat 
					_ActionBtm [2].interactable = false; // Cure
					_ActionBtm [3].interactable = false; // Build
					_ActionBtm [4].interactable = false; // Shares
					_ActionBtm [5].interactable = false; // Shares
					_ActionBtm [6].interactable = false; // Shares
					_ActionBtm [7].interactable = false; // Shares

					_CardObject [0].GetComponent<Button> ().interactable = false;
					_CardObject [1].GetComponent<Button> ().interactable = false;
					_CardObject [2].GetComponent<Button> ().interactable = false;
					_CardObject [3].GetComponent<Button> ().interactable = false;
					_CardObject [4].GetComponent<Button> ().interactable = false;

					break;

				case 25:
					_ActionBtm [0].interactable = false; // Move
					_ActionBtm [1].interactable = false; // Treat 
					_ActionBtm [2].interactable = true; // Cure
					_ActionBtm [3].interactable = false; // Build
					_ActionBtm [4].interactable = false; // Shares
					_ActionBtm [5].interactable = false; // Shares
					_ActionBtm [6].interactable = false; // Shares
					_ActionBtm [7].interactable = false; // Shares

					_CardObject [0].GetComponent<Button> ().interactable = false;
					_CardObject [1].GetComponent<Button> ().interactable = false;
					_CardObject [2].GetComponent<Button> ().interactable = false;
					_CardObject [3].GetComponent<Button> ().interactable = false;
					_CardObject [4].GetComponent<Button> ().interactable = false;

					break;
				}
			}
                else
                {
                    if (_State == 1)
                    {
                        if (_Action != 0)
                        {
                            _ActionBtm[0].interactable = true;
                            _ActionBtm[5].interactable = true;

							if (_isShared)
							{ // Shares
								_ActionBtm[4].interactable = true;
							}
							else
							{
								_ActionBtm[4].interactable = false;
							}


							if (_isCanBuild)
							{
								_ActionBtm[3].interactable = true;
							}
							else
							{
								_ActionBtm[3].interactable = false;
							}

							if (_isCanMove)
							{
							_ActionBtm [9].interactable = true;
								/*if (_PlayerOneClass != 0) {
									
								}*/
							}
							else
							{
								//_ActionBtm[9].interactable = false;
							}

							if (_CurrectNodePlayerOne.GetComponent<NodeScript>()._Level >= 1)
							{
								if (_Action != 0)
								{
									_ActionBtm[1].interactable = true;
									if (_PlayerOneClass == 0) {
									_MedicAbiltyUse.interactable = true;
									}
									_CurrectNodePlayerOne.GetComponent<NodeScript>()._CheckById ();
								}
								else
								{
									if (_PlayerOneClass == 0) {
									_MedicAbiltyUse.interactable = false;
									}
									_ActionBtm[1].interactable = false;
									_CurrectNodePlayerOne.GetComponent<NodeScript>()._CheckById ();
								}


							}
							else
							{
								if (_PlayerOneClass == 0) {
								_MedicAbiltyUse.interactable = false;
								}
								_ActionBtm[1].interactable = false;
								_CurrectNodePlayerOne.GetComponent<NodeScript>()._CheckById ();
							}
                        }
                        else
                        {
                            _ActionBtm[0].interactable = false;
                            _ActionBtm[5].interactable = false;
                        }



                       

                       


                    }
                    else
                    {
                        if (_Action != 0)
                        {
                            _ActionBtm[0].interactable = true;
                            _ActionBtm[5].interactable = true;


                        }
                        else
                        {
                            _ActionBtm[0].interactable = false;
                            _ActionBtm[5].interactable = false;
                        }


                        if (_CurrectNodePlayerTwo.GetComponent<NodeScript>()._Level >= 1)
                        {
                            if (_Action != 0)
                            {
                                _ActionBtm[1].interactable = true;
								if (_PlayerTwoClass == 0) {
								_MedicAbiltyUse.interactable = true;
								}
								_CurrectNodePlayerTwo.GetComponent<NodeScript>()._CheckById ();
                            }
                            else
                            {
								if (_PlayerTwoClass == 0) {
								_MedicAbiltyUse.interactable = false;
								}
                                _ActionBtm[1].interactable = false;
								_CurrectNodePlayerTwo.GetComponent<NodeScript>()._CheckById ();
                            }
                      

                        }
                        else
                        {
							if (_PlayerTwoClass == 0) {
							_MedicAbiltyUse.interactable = false;
							}
                            _ActionBtm[1].interactable = false;
							_CurrectNodePlayerTwo.GetComponent<NodeScript>()._CheckById ();
							//_CurrectNodePlayerTwo.GetComponent<NodeScript>()._CheckById ();
                            //_ActionBtm [2].interactable = false;
                        }

                        if (_isShared)
                        { // Shares
                            _ActionBtm[4].interactable = true;
                        }
                        else
                        {
                            _ActionBtm[4].interactable = false;
                        }

                        if (_isCanBuild)
                        {
                            _ActionBtm[3].interactable = true;
                        }
                        else
                        {
                            _ActionBtm[3].interactable = false;
                        }

                    }

                    //_ActionBtm[6].interactable = false;
                    _ActionBtm[7].interactable = false;


                    if (_Action >= 4)
                    {
                        _ActionBtm[8].interactable = false;
                    }
                    else
                    {
                        if (_Action != 4)
                        {
                            _ActionBtm[8].interactable = true;
                        }
                        else
                        {
                            _ActionBtm[8].interactable = false;
                        }
                    }


                    if (_Action <= 0)
                    {
                        _ActionBtm[6].interactable = true;

                    }
                    else
                    {
                        _ActionBtm[6].interactable = false;
                    }

                    

                    if (_Action <= 0)
                    {
                        TopStatBar.SetActive(true);
                        _StatePopUp.text = "Please Select City Cards.";
                    }
                    else
                    {
                        TopStatBar.SetActive(false);
                        _StatePopUp.text = "Please Select City Cards.";
                    }

                    if (_isCanMove)
                    {
						/*if (_PlayerTwoClass != 0) {
							_ActionBtm [9].interactable = true;
						}*/
					_ActionBtm [9].interactable = true;
                    }
                    else
                    {
                      //  _ActionBtm[9].interactable = false;
                    }

				_CardObject [0].GetComponent<Button> ().interactable = true;
				_CardObject [1].GetComponent<Button> ().interactable = true;
				_CardObject [2].GetComponent<Button> ().interactable = true;
				_CardObject [3].GetComponent<Button> ().interactable = true;
				_CardObject [4].GetComponent<Button> ().interactable = true;
					
                }
			



			break;
		case 1:

			if (_Action <= 0) {
				TopStatBar.SetActive (true);
				_StatePopUp.text = "Please Select City Cards.";
			} else {
				TopStatBar.SetActive (false);
				_StatePopUp.text = "Please Select City Cards.";
			}

			//_ActionBtm [8].interactable = true;
			_BlockPlayerPart.SetActive (true);
			//_BlockPlayerPart.SetActive (true);
			for (int i = 0; i <= 5; i++) {
				_ActionBtm [i].interactable = false;
			}

			_ActionBtm [6].interactable = true;
			_ActionBtm [7].interactable = false;
			_ActionBtm [8].interactable = false;
			// Draw

			_ActionBtm [0].interactable = false; // Move
			_ActionBtm [1].interactable = false; // Treat 
			_ActionBtm [2].interactable = false; // Cure
			_ActionBtm [3].interactable = false; // Build
			_ActionBtm [4].interactable = false; // Shares
			_ActionBtm [5].interactable = false; // Shares
			//_ActionBtm[6].interactable = false; // Shares
			//_ActionBtm[7].interactable = false; // Shares

			_CardObject [0].GetComponent<Button> ().interactable = false;
			_CardObject [1].GetComponent<Button> ().interactable = false;
			_CardObject [2].GetComponent<Button> ().interactable = false;
			_CardObject [3].GetComponent<Button> ().interactable = false;
			_CardObject [4].GetComponent<Button> ().interactable = false;

			if (GetComponent<TutorialScript> ()._Step == 27) {
				_CardOnPopUp [0].transform.GetChild (1).GetComponent<Button> ().interactable = false;
				_CardOnPopUp [1].transform.GetChild (1).GetComponent<Button> ().interactable = false;
				_CardOnPopUp [2].transform.GetChild (1).GetComponent<Button> ().interactable = true;
				_CardOnPopUp [3].transform.GetChild (1).GetComponent<Button> ().interactable = true;
				_CardOnPopUp [4].transform.GetChild (1).GetComponent<Button> ().interactable = false;
				_CardOnPopUp [5].transform.GetChild (1).GetComponent<Button> ().interactable = false;
				_CardOnPopUp [6].transform.GetChild (1).GetComponent<Button> ().interactable = false;
			} else {
				_CardOnPopUp [0].transform.GetChild (1).GetComponent<Button> ().interactable = true;
				_CardOnPopUp [1].transform.GetChild (1).GetComponent<Button> ().interactable = true;
				_CardOnPopUp [2].transform.GetChild (1).GetComponent<Button> ().interactable = true;
				_CardOnPopUp [3].transform.GetChild (1).GetComponent<Button> ().interactable = true;
				_CardOnPopUp [4].transform.GetChild (1).GetComponent<Button> ().interactable = true;
				_CardOnPopUp [5].transform.GetChild (1).GetComponent<Button> ().interactable = true;
				_CardOnPopUp [6].transform.GetChild (1).GetComponent<Button> ().interactable = true;
			}
			break;
		case 2:

			_BlockPlayerPart.SetActive (true);

			for (int i = 0; i <= 5; i++) {
				_ActionBtm [i].interactable = false;
			}

			_ActionBtm [6].interactable = false;

			if (_State == 1) {
				if (_PlayerOneCard.Count <= 5) {
					_ActionBtm [7].interactable = true;
				}
			} else {
				if (_PlayerTwoCard.Count <= 5) {
					_ActionBtm [7].interactable = true;
				}
			}
			_ActionBtm [8].interactable = false;
			// Infection
			_ActionBtm[0].interactable = false; // Move
			_ActionBtm[1].interactable = false; // Treat 
			_ActionBtm[2].interactable = false; // Cure
			_ActionBtm[3].interactable = false; // Build
			_ActionBtm[4].interactable = false; // Shares
			_ActionBtm[5].interactable = false; // Shares
			_ActionBtm[6].interactable = false; // Shares
			//_ActionBtm[7].interactable = false; // Shares

			_CardObject[0].GetComponent<Button>().interactable = false;
			_CardObject[1].GetComponent<Button>().interactable = false;
			_CardObject[2].GetComponent<Button>().interactable = false;
			_CardObject[3].GetComponent<Button>().interactable = false;
			_CardObject[4].GetComponent<Button>().interactable = false;

			break;
		}
			
	}

	public Button[] _ActionBtm;

    public bool _isCharter;

	public bool _isCanBuild;

	public void _CheckCard(){
        _isCharter = false;

		if (_Building != 0) {
			if (_State == 1) {
               
                if (_PlayerOneClass == 2)
                {
                    if (!_CurrectNodePlayerOne.GetComponent<NodeScript>()._isBuild)
                    {
                        _isCanBuild = true;
                    }
                    else
                    {
                        _isCanBuild = false;
                    }
                }
                else
                {

                    //Debug.Log("SSS");
                    foreach (CardManager._Card x in _PlayerOneCard)
                    {
                        if (x._Node == _CurrectNodePlayerOne)
                        {
                            _isCharter = true;
                            if (!_CurrectNodePlayerOne.GetComponent<NodeScript>()._isBuild)
                            {
                                _isCanBuild = true;
                                break;
                            }
                            else
                            {
                                _isCanBuild = false;
                            }

                        }
                        else
                        {
                            _isCanBuild = false;
                        }

                    }


                }
				
			} else {

                if (_PlayerTwoClass == 2)
                {
                    if (!_CurrectNodePlayerTwo.GetComponent<NodeScript>()._isBuild)
                    {
                        _isCanBuild = true;
                    }
                    else
                    {
                        _isCanBuild = false;
                    }
                }
                else
                {

                    foreach (CardManager._Card x in _PlayerTwoCard)
                    {
                        if (x._Node == _CurrectNodePlayerTwo)
                        {
                            _isCharter = true;
                            if (!_CurrectNodePlayerTwo.GetComponent<NodeScript>()._isBuild)
                            {
                                _isCanBuild = true;
                                break;
                            }
                            else
                            {
                                _isCanBuild = false;
                            }
                        }
                        else
                        {
                            _isCanBuild = false;
                        }
                    }
                }
			}
		} else {
			_isCanBuild = false;
		}

	}

	public void _TreatNode(){

		_SaveAction (1);
		
        if (_State == 1)
        {
            if (_PlayerOneClass == 0)
            {
                _CurrectNodePlayerOne.GetComponent<NodeScript>()._Treat(true);
				GetComponent<LogFilesScript> ()._MedicSkill ();
            }
            else
            {
                _CurrectNodePlayerOne.GetComponent<NodeScript>()._Treat(false);


            }
        }
        else
        {
            if (_PlayerTwoClass == 0)
            {
                _CurrectNodePlayerTwo.GetComponent<NodeScript>()._Treat(true);
				GetComponent<LogFilesScript> ()._MedicSkill ();
            }
            else
            {
                _CurrectNodePlayerTwo.GetComponent<NodeScript>()._Treat(false);
            }
        }

        _DecreseAction();

		if (GetComponent<TutorialScript> ()._isOnTutorial) {
			if (GetComponent<TutorialScript> ()._Step == 7) {
				GetComponent<TutorialScript> ()._Step++;


			}

			if (GetComponent<TutorialScript> ()._Step == 6) {
				GetComponent<TutorialScript> ()._Step++;
				GetComponent<TutorialScript> ()._ExtraPopUp [11].SetActive (false);
			}



			if (GetComponent<TutorialScript> ()._Step == 8) {
				GetComponent<TutorialScript> ()._ExtraPopUp [17].SetActive (false);
				GetComponent<TutorialScript> ()._ExtraPopUp [18].SetActive (true);
				//GetComponent<TutorialScript> ()._ExtraPopUp [16].SetActive (false);
				GetComponent<TutorialScript> ()._ShowPopUp ();
				GetComponent<TutorialScript> ()._Step++;

			}
				

			if (GetComponent<TutorialScript> ()._Step == 2) {
				GetComponent<TutorialScript> ()._ShowPopUp ();
				GetComponent<TutorialScript> ()._ExtraPopUp [2].SetActive (false);
			}

			if (GetComponent<TutorialScript> ()._Step == 13) {
				GetComponent<TutorialScript> ()._ShowPopUp ();
				GetComponent<TutorialScript> ()._ExtraPopUp [21].SetActive (false);
				GetComponent<TutorialScript> ()._Step++;
			}

			if (GetComponent<TutorialScript> ()._Step == 19) {
				GetComponent<TutorialScript> ()._ExtraPopUp [24].SetActive (false);
				_Pass ();
				_PhasePanel.SetActive (true);
				_PhaseSubPanel[2].SetActive(true);
			}

			if (GetComponent<TutorialScript> ()._Step == 18) {
				//GetComponent<TutorialScript>()._ShowPopUp();
				GetComponent<TutorialScript> ()._Step++;
			}

			if (GetComponent<TutorialScript> ()._Step == 24) {
				GetComponent<TutorialScript> ()._Tutorial_MustShow [15].SetActive (false);
				GetComponent<TutorialScript> ()._Tutorial_MustShow [16].SetActive (true);
				//GetComponent<TutorialScript>()._ShowPopUp();
				GetComponent<TutorialScript> ()._Step++;
			}

			if (GetComponent<TutorialScript> ()._Step == 27) {
				GetComponent<TutorialScript> ()._ExtraPopUp [27].SetActive (false);
				//GetComponent<TutorialScript>()._ShowPopUp();
				_Pass ();
				_PhasePanel.SetActive (true);
				_PhaseSubPanel[2].SetActive(true);
			}

			if (GetComponent<TutorialScript> ()._Step == 26) {
				//GetComponent<TutorialScript>()._ShowPopUp();
				GetComponent<TutorialScript> ()._Step++;
			}

			if (GetComponent<TutorialScript> ()._Step == 30) {
				//GetComponent<TutorialScript>()._ShowPopUp();
				GetComponent<TutorialScript> ()._Tutorial_MustShow [19].SetActive (false);
				GetComponent<TutorialScript> ()._Tutorial_MustShow [18].SetActive (false);
				GetComponent<TutorialScript> ()._Step++;
			}

			if (GetComponent<TutorialScript> ()._Step == 29) {
				//GetComponent<TutorialScript>()._ShowPopUp();
				GetComponent<TutorialScript> ()._Tutorial_MustShow [19].SetActive (false);
				GetComponent<TutorialScript> ()._Tutorial_MustShow [18].SetActive (true);
				GetComponent<TutorialScript> ()._Step++;
			}

		} else if (GetComponent<Tutorial_Section02> ()._Section02) {
			if (GetComponent<Tutorial_Section02> ()._Step == 0) {
				GetComponent<Tutorial_Section02> ()._Tu [1].SetActive (true);
				GetComponent<Tutorial_Section02> ()._HideTu [0].SetActive (false);
				GetComponent<Tutorial_Section02> ()._Step++;
			}

			if (GetComponent<Tutorial_Section02> ()._Step == 9) {
				//GetComponent<Tutorial_Section02> ()._Tu [1].SetActive (true);
				GetComponent<Tutorial_Section02> ()._HideArrow [13].SetActive (false);
				GetComponent<Tutorial_Section02> ()._HideArrow [9].SetActive (true);
				GetComponent<Tutorial_Section02> ()._Step++;

			}

			if (GetComponent<Tutorial_Section02> ()._Step == 22) {
				//GetComponent<Tutorial_Section02> ()._Tu [1].SetActive (true);
				_PhasePanel.SetActive (true);
				_PhaseSubPanel[2].SetActive(true);
				GetComponent<Tutorial_Section02> ()._HideTu [12].SetActive (false);
				_Pass();
			}

			if (GetComponent<Tutorial_Section02> ()._Step == 21) {
				//GetComponent<Tutorial_Section02> ()._Tu [1].SetActive (true);
				GetComponent<Tutorial_Section02> ()._Step++;
			}
		}

    }

	public void _TreatById(int i){
		_SaveAction (1);
		GetComponent<LogFilesScript> ()._CheckActionById.Add (1);

		if (_State == 1)
		{
			if (_PlayerOneClass == 0)
			{
				_CurrectNodePlayerOne.GetComponent<NodeScript>()._TreatById(true,i);
			}
			else
			{
				_CurrectNodePlayerOne.GetComponent<NodeScript>()._TreatById(false,i);


			}
		}
		else
		{
			if (_PlayerTwoClass == 0)
			{
				_CurrectNodePlayerTwo.GetComponent<NodeScript>()._TreatById(true,i);
			}
			else
			{
				_CurrectNodePlayerTwo.GetComponent<NodeScript>()._TreatById(false,i);
			}
		}

		_DecreseAction();
	}

	public GameObject _TutorialPanel06;
	public GameObject _TutorialPanel07;

    public void _Build(){

		_SaveAction (3);
		GetComponent<LogFilesScript> ()._CheckActionById.Add (3);
        if(_State==1){

            if (PlayerScript.Instance._PlayerOneClass==2)
            {
                _Building--;
                _BuildingOnBar.text = _Building + "";
                _CurrectNodePlayerOne.GetComponent<NodeScript>()._Build();
                _ResearchNode.Add(_CurrectNodePlayerOne);
				GetComponent<LogFilesScript> ()._CheckActionById.Add (5);
            }
            else
            {
                foreach (CardManager._Card x in _PlayerOneCard)
                {
                    if (x._Node == _CurrectNodePlayerOne)
                    {
                        _Building--;
                        _BuildingOnBar.text = _Building + "";
                        x._Node.GetComponent<NodeScript>()._Build();
                        _ResearchNode.Add(x._Node);
                        GetComponent<CardManager>()._PLayerDiscardplie.Add(x);
                        _PlayerOneCard.Remove(x);

                        break;
                    }
                }

				if (GetComponent<TutorialScript> ()._Step == 15) {
					GetComponent<TutorialScript> ()._ShowPopUp ();
					GetComponent<TutorialScript> ()._ExtraPopUp [22].SetActive (false);
					//_PhaseSubPanel[2].SetActive(true);
					//GetComponent<TutorialScript> ()._Step++;
					//_Pass ();
				}

				if (GetComponent<Tutorial_Section02> ()._Step == 2) {
					GetComponent<Tutorial_Section02> ()._Tu [3].SetActive (true);
					GetComponent<Tutorial_Section02> ()._HideTu [2].SetActive (false);
					GetComponent<Tutorial_Section02> ()._Step++;
					//_Pass ();
				}
                
            }
        }else{

            if (PlayerScript.Instance._PlayerTwoClass == 2)
            {
                _Building--;
                _BuildingOnBar.text = _Building + "";
                _CurrectNodePlayerTwo.GetComponent<NodeScript>()._Build();
                _ResearchNode.Add(_CurrectNodePlayerTwo);
				GetComponent<LogFilesScript> ()._CheckActionById.Add (5);
            }
            else
            {
                foreach (CardManager._Card x in _PlayerTwoCard)
                {
                    if (x._Node == _CurrectNodePlayerTwo)
                    {
                        _Building--;
                        _BuildingOnBar.text = _Building + "";
                        x._Node.GetComponent<NodeScript>()._Build();
                        _ResearchNode.Add(x._Node);
                        GetComponent<CardManager>()._PLayerDiscardplie.Add(x);
                        _PlayerTwoCard.Remove(x);
                        break;
                    }
                }
            }

			if (GetComponent<TutorialScript> ()._isOnTutorial && GetComponent<TutorialScript> ()._Step == 11) {
				
				_TutorialPanel07.SetActive (true);
				GetComponent<CardManager> ()._ShowCardOnHand ();
				GetComponent<TutorialScript> ()._Step++;
			}

			if (GetComponent<Tutorial_Section02> ()._Section02 && GetComponent<Tutorial_Section02> ()._Step == 7) {
				GetComponent<Tutorial_Section02> ()._HideTu [6].SetActive (false);
				_Pass ();
			}
                
        }

        if (GetComponent<TutorialScript>()._isOnTutorial && GetComponent<TutorialScript>()._Step == 21)
        {
            GetComponent<TutorialScript>()._ShowPopUp();
        }

		if (GetComponent<TutorialScript>()._isOnTutorial && GetComponent<TutorialScript>()._Step == 33)
		{
			GetComponent<TutorialScript> ()._Step++;
			GetComponent<TutorialScript> ()._Tutorial_MustShow [23].SetActive (false);
			GetComponent<TutorialScript> ()._Tutorial_MustShow [22].SetActive (true);
			
		}

        _CheckCard();
		_CheckCure();
		_CheckPlayerCard();
		_CheckIfShares();
        _DecreseAction();
    }

    public int _Stack_Type01;
    public int _Stack_Type02;
    public void _CheckCure(){

			if (_State == 1) {
				if (_CurrectNodePlayerOne.GetComponent<NodeScript> ()._isBuild) {
					_Stack_Type01 = 0;
					_Stack_Type02 = 0;


					foreach (CardManager._Card x in _PlayerOneCard) {
						if (x._Node.GetComponent<NodeScript> ()._Type == 0) {
							if (_CurrectNodePlayerOne.GetComponent<NodeScript> ()._Type == 0) {
								if (!_isCureType [0]) {
									_Stack_Type01++;
								}
							}
						} else {
							if (_CurrectNodePlayerOne.GetComponent<NodeScript> ()._Type == 1) {
								if (!_isCureType [1]) {
									_Stack_Type02++;
								}
							}
						}

					}

					if (_Stack_Type01 >= 3 || _Stack_Type02 >= 3) {
						_ActionBtm [2].interactable = true;
						//Debug.Log ("A P1");
						//_isCure = true;
					} else {
						_ActionBtm [2].interactable = false;
						//Debug.Log ("B P1");
						//_isCure = false;
					}
				} else {
					_ActionBtm [2].interactable = false;
					//Debug.Log ("C P1");
					//_isCure = false;
				}

				//Debug.Log ("Check Player 1");

			} else {




				if (_CurrectNodePlayerTwo.GetComponent<NodeScript> ()._isBuild) {
					_Stack_Type01 = 0;
					_Stack_Type02 = 0;

					foreach (CardManager._Card x in _PlayerTwoCard) {
						if (x._Node.GetComponent<NodeScript> ()._Type == 0) {
							if (_CurrectNodePlayerTwo.GetComponent<NodeScript> ()._Type == 0) {
								if (!_isCureType [0]) {
									_Stack_Type01++;
								}
							}
						} else {
							if (_CurrectNodePlayerTwo.GetComponent<NodeScript> ()._Type == 1) {
								if (!_isCureType [1]) {
									_Stack_Type02++;
								}
							}
						}
					}

					if (_Stack_Type01 >= 3 || _Stack_Type02 >= 3) {
						//Debug.Log ("A P2");
						_ActionBtm [2].interactable = true;
						//_isCure = true;
					} else {
					//	Debug.Log ("B P2");
						_ActionBtm [2].interactable = false;
						//_isCure = false;
					}

				} else {
					//Debug.Log ("C P2");
					_ActionBtm [2].interactable = false;
					// _isCure = false;
				}

			}

			//Debug.Log ("Check Player 2");

    }
    int _count;

    public bool[] _isCureType;
    public Text[] _CuredText;
    public List<CardManager._Card> _Temp;
    public void _Cure(){

		_SaveAction (2);
		GetComponent<LogFilesScript> ()._CheckActionById.Add (2);

        if (_State == 1)
        {
            _count = 0;
            if(_Stack_Type01 >= 3){
                foreach (CardManager._Card x in _PlayerOneCard)
                {
                    if(_count<=2){
                        if(x._Node.GetComponent<NodeScript>()._Type==0){
                            GetComponent<CardManager>()._PLayerDiscardplie.Add(x);
                            _Temp.Add(x);
                            _count++;
                        }
                    }
                }

				_PhasePanel.SetActive (true);
				_PhaseSubPanel [4].SetActive (true);

                _CuredText[0].text = "1/1";
                _isCureType[0] = true;
                _ClearCube();
                foreach (CardManager._Card C in _Temp){
                    foreach(CardManager._Card C1 in _PlayerOneCard){
                        if(C._Node == C1._Node){
                            _PlayerOneCard.Remove(C1);
                            break;
                        }
                    }
                }

                _Temp.Clear();



            }else{
                foreach (CardManager._Card x in _PlayerOneCard)
                {
                    if (_count <= 2)
                    {
                        if (x._Node.GetComponent<NodeScript>()._Type == 1)
                        {
                            GetComponent<CardManager>()._PLayerDiscardplie.Add(x);
                            _Temp.Add(x);
                            _count++;
                        }
                    }
                } 

				_PhasePanel.SetActive (true);
				_PhaseSubPanel [5].SetActive (true);

                _CuredText[1].text = "1/1";
                _isCureType[1] = true;
                _ClearCube();


                foreach (CardManager._Card C in _Temp)
                {
                    foreach (CardManager._Card C1 in _PlayerOneCard)
                    {
                        if (C._Node == C1._Node)
                        {
                            _PlayerOneCard.Remove(C1);
                            break;
                        }
                    }
                }

                _Temp.Clear();

				if (GetComponent<TutorialScript> ()._isOnTutorial) {

					if (GetComponent<TutorialScript> ()._Step == 20) {
						GetComponent<TutorialScript> ()._Step++;
						GetComponent<TutorialScript> ()._ShowPopUp ();
						GetComponent<TutorialScript> ()._ExtraPopUp [25].SetActive (false);
						GetComponent<CardManager> ()._ShowCardOnHand ();
						GetComponent<CardManager> ()._BtmForToturial.SetActive (true);
					}
				}

				if (GetComponent<Tutorial_Section02> ()._Section02) {
					if (GetComponent<Tutorial_Section02> ()._Step == 16) {
						GetComponent<Tutorial_Section02> ()._Tu [11].SetActive (true);
						GetComponent<Tutorial_Section02> ()._HideTu [10].SetActive (false);
						GetComponent<Tutorial_Section02> ()._Step++;
					}


				}
            }


        }else{
            _count = 0;
			if (_Stack_Type01 >= 3)
            {
                foreach (CardManager._Card x in _PlayerTwoCard)
                {
                    if (_count <= 3)
                    {
                        if (x._Node.GetComponent<NodeScript>()._Type == 0)
                        {
                            GetComponent<CardManager>()._PLayerDiscardplie.Add(x);
                            _Temp.Add(x);
                            _count++;
                        }
                    }

                }

                _CuredText[0].text = "1/1";
                _isCureType[0] = true;
                _ClearCube();

                foreach (CardManager._Card C in _Temp)
                {
                    foreach (CardManager._Card C1 in _PlayerTwoCard)
                    {
                        if (C._Node == C1._Node)
                        {
                            _PlayerTwoCard.Remove(C1);
                            break;
                        }
                    }
                }

                _Temp.Clear();
            }else{
                foreach (CardManager._Card x in _PlayerTwoCard)
                {
                    if (_count <= 3)
                    {
                        if (x._Node.GetComponent<NodeScript>()._Type == 1)
                        {
                            GetComponent<CardManager>()._PLayerDiscardplie.Add(x);
                            _Temp.Add(x);
                            _count++;
                        }
                    }

                }

                _CuredText[1].text = "1/1";
                _isCureType[1] = true;
                _ClearCube();
                foreach (CardManager._Card C in _Temp)
                {
                    foreach (CardManager._Card C1 in _PlayerTwoCard)
                    {
                        if (C._Node == C1._Node)
                        {
                            _PlayerTwoCard.Remove(C1);
                            break;
                        }
                    }
                }
                _Temp.Clear();
            }
				
        }

		_DecreseAction();

        if(_isCureType[0]&&_isCureType[1]){
			GetComponent<CardManager> ()._EndGame (true,"By Cured");
			_PhasePanel.SetActive (false);
        }

		if (GetComponent<TutorialScript> ()._Step == 34) {
			GetComponent<TutorialScript> ()._ExtraPopUp [29].SetActive (false);
			_TutorialPanel08.SetActive (true);
		}

		if (GetComponent<Tutorial_Section02> ()._Step == 25) {
			GetComponent<Tutorial_Section02> ()._HideTu [13].SetActive (false);
		}


    }

	public GameObject _TutorialPanel08;

    public void _ClearCube()
    {
 
        foreach(GameObject i in GetComponent<CardManager>()._NodeManger.GetComponent<NodeManager>()._Node)
        {
			i.GetComponent<NodeScript> ()._ClearNode ();
        }
       
    }


     

    public void _Pass(){
        _Action = 0;
		_PlayState = 1;
		TopStatBar.SetActive (true);
		_StatePopUp.text = "Please Select City Cards.";

		foreach (CardManager._Card C in GetComponent<CardManager>()._AllCardStore) {
			if (!C._isEpidemics) {

				C._Node.GetComponent<NodeScript> ()._ResetCode ();

			}
		}

		_isDirectFilght = false;
		_isMoveMode = false;
		_isCanMove = false;

		_PhasePanel.SetActive (true);
		_PhaseSubPanel [2].SetActive (true);
       // _DecreseAction();
            }


    public GameObject[] _CardTapPlayerOne;
    public GameObject[] _CardTapPlayerTwo;
    // Team Info
    public void _CardOnHand(){
        for (int i = 0; i <= _CardTapPlayerOne.Length-1;i++){
            if(i<_PlayerOneCard.Count){
                _CardTapPlayerOne[i].SetActive(true);
                _CardTapPlayerOne[i].GetComponent<Image>().color = _PlayerOneCard[i]._Node.GetComponent<NodeScript>()._NodeColor[_PlayerOneCard[i]._Node.GetComponent<NodeScript>()._Type];
                _CardTapPlayerOne[i].transform.GetChild(0).gameObject.GetComponent<Text>().text = _PlayerOneCard[i]._CardName;
                _CardTapPlayerOne[i].GetComponent<SwapColour>()._GetImage();
            }else{
                _CardTapPlayerOne[i].SetActive(false);
            }
           
            if (i < _PlayerTwoCard.Count)
            {
                _CardTapPlayerTwo[i].SetActive(true);
                _CardTapPlayerTwo[i].GetComponent<Image>().color = _PlayerTwoCard[i]._Node.GetComponent<NodeScript>()._NodeColor[_PlayerTwoCard[i]._Node.GetComponent<NodeScript>()._Type];
                _CardTapPlayerTwo[i].transform.GetChild(0).gameObject.GetComponent<Text>().text = _PlayerTwoCard[i]._CardName;
                _CardTapPlayerTwo[i].GetComponent<SwapColour>()._GetImage();
            }
            else
            {
                _CardTapPlayerTwo[i].SetActive(false);
            }
        }
    }
    // Card On Hand
    public GameObject[] _CardObject;
    public void _CheckPlayerCard(){
        for (int i = 0; i <= _CardObject.Length - 1; i++)
        {
            if(_State==1){
                if (i < _PlayerOneCard.Count)
                {
                    _CardObject[i].SetActive(true);
                    _CardObject[i].GetComponent<Image>().color = _PlayerOneCard[i]._Node.GetComponent<NodeScript>()._NodeColor[_PlayerOneCard[i]._Node.GetComponent<NodeScript>()._Type];
                    _CardObject[i].transform.GetChild(0).gameObject.GetComponent<Text>().text = _PlayerOneCard[i]._CardName;
                    _CardObject[i].GetComponent<SwapColour_ChangeImage>()._GetImage();
                }
                else
                {
                    _CardObject[i].SetActive(false);
                }
            }else{
                if (i < _PlayerTwoCard.Count)
                {
                    _CardObject[i].SetActive(true);
                    _CardObject[i].GetComponent<Image>().color = _PlayerTwoCard[i]._Node.GetComponent<NodeScript>()._NodeColor[_PlayerTwoCard[i]._Node.GetComponent<NodeScript>()._Type];
                    _CardObject[i].transform.GetChild(0).gameObject.GetComponent<Text>().text = _PlayerTwoCard[i]._CardName;
                    _CardObject[i].GetComponent<SwapColour_ChangeImage>()._GetImage();
                }
                else
                {
                    _CardObject[i].SetActive(false);
                }
            }

        }
    }

    public GameObject _MoreCardPopUp;
    public GameObject[] _CardOnPopUp;


    public void _CheckCardOnHand(){ // More than 5 or not
        if (_State == 1)
        {

		

            if (_PlayerOneCard.Count <= 5)
            {
                _MoreCardPopUp.SetActive(false);
				_StatePopUp.text = "Please Select Infection Cards.";
                _CheckPlayerCard();

				//GetComponent<PlayerScript> ()._PhasePanel.SetActive (true);
				//GetComponent<PlayerScript> ()._PhaseSubPanel [3].SetActive (true);
            }
            else
            {
				_PlayerName.text = "Player 1";	
			
			
                _MoreCardPopUp.SetActive(true);
  
                for (int i = 0; i <= _CardOnPopUp.Length-1; i++)
                {
                    if (i < _PlayerOneCard.Count)
                    {
                        _CardOnPopUp[i].SetActive(true);
                        _CardOnPopUp[i].GetComponent<Image>().color = _PlayerOneCard[i]._Node.GetComponent<NodeScript>()._NodeColor[_PlayerOneCard[i]._Node.GetComponent<NodeScript>()._Type];
                        _CardOnPopUp[i].transform.GetChild(0).gameObject.GetComponent<Text>().text = _PlayerOneCard[i]._CardName;
                        _CardOnPopUp[i].GetComponent<SwapColour_ChangeImage>()._GetImage();
                    }
                    else
                    {
                        _CardOnPopUp[i].SetActive(false);
                    }
                }
            }

        }else{
            if (_PlayerTwoCard.Count <= 5)
            {
                _MoreCardPopUp.SetActive(false);
				_StatePopUp.text = "Please Select Infection Cards.";
                _CheckPlayerCard();

				//GetComponent<PlayerScript> ()._PhasePanel.SetActive (true);
				//GetComponent<PlayerScript> ()._PhaseSubPanel [3].SetActive (true);
            }
            else
            {
				_PlayerName.text = "Player 2";	
                _MoreCardPopUp.SetActive(true);

                for (int i = 0; i <= _CardOnPopUp.Length - 1; i++)
                {
                    if (i < _PlayerTwoCard.Count)
                    {
                        _CardOnPopUp[i].SetActive(true);
                        _CardOnPopUp[i].GetComponent<Image>().color = _PlayerTwoCard[i]._Node.GetComponent<NodeScript>()._NodeColor[_PlayerTwoCard[i]._Node.GetComponent<NodeScript>()._Type];
                        _CardOnPopUp[i].transform.GetChild(0).gameObject.GetComponent<Text>().text = _PlayerTwoCard[i]._CardName;
                        _CardOnPopUp[i].GetComponent<SwapColour_ChangeImage>()._GetImage();
                    }
                    else
                    {
                        _CardOnPopUp[i].SetActive(false);
                    }
                }


            }
        }
    }
	public Text _PlayerName;
    public void _DiscardOnHand(int i){
        if (_State == 1)
        {
            GetComponent<CardManager>()._PLayerDiscardplie.Add(_PlayerOneCard[i]);
            _PlayerOneCard.RemoveAt(i);

        }else{
            GetComponent<CardManager>()._PLayerDiscardplie.Add(_PlayerTwoCard[i]);
            _PlayerTwoCard.RemoveAt(i);
        }

		if (_PlayState == 0) {
			if (_isCanMove) {
				_MoveBuilding_Process ();
			}
		}

        _CheckCardOnHand();
		if (_PlayState == 2) {
			_ShowPhaseCard ();
		}
    }

	public void _ShowPhaseCard(){
		
		if (_State == 1) {
			if (_PlayerOneCard.Count <= 5) {
				GetComponent<PlayerScript> ()._PhasePanel.SetActive (true);
				GetComponent<PlayerScript> ()._PhaseSubPanel [3].SetActive (true);
			}
		} else {
			if (_PlayerTwoCard.Count <= 5) {
				GetComponent<PlayerScript> ()._PhasePanel.SetActive (true);
				GetComponent<PlayerScript> ()._PhaseSubPanel [3].SetActive (true);
			}
		}
	}

    public bool _isHaveCard;
    public bool _isShared;
    public void _CheckIfShares(){
        _isHaveCard = false;
        foreach(CardManager._Card Card in _PlayerOneCard){
            if(Card._Node == _CurrectNodePlayerOne){
                _isHaveCard = true;
            }
        }

        foreach (CardManager._Card Card in _PlayerTwoCard)
        {
            if (Card._Node == _CurrectNodePlayerTwo)
            {
                _isHaveCard = true;
            }
        }

        if(_CurrectNodePlayerOne == _CurrectNodePlayerTwo){
            if(_isHaveCard){
                _isShared = true;
            }else{
                _isShared = false;
            }
        }else{
            _isShared = false;
        }
    }

    public GameObject _SharesPopUp;
    public GameObject[] _CardOnPopUp_PlayerOne;
    public GameObject[] _CardOnPopUp_PlayerTwo;
    public Text[] _TextPlayer;
	public bool _isFromShares;
    public void _OnShares(){
		_isGive = false;
		_SaveAction (5);
		GetComponent<LogFilesScript> ()._CheckActionById.Add (4);

			 // Give
			foreach (CardManager._Card i in _PlayerOneCard) {
				if (i._Node == _CurrectNodePlayerOne) {
					_PlayerOneCard.Remove (i);
					_PlayerTwoCard.Add (i);
					GetComponent<CardManager> ()._Report (i, " - is given to Player 2");
					_isGive = true;
					GetComponent<CardManager>()._ReportTxt.text = "Share solution";
					break;
				}
			}

			if (!_isGive) { // Take
				foreach (CardManager._Card i in _PlayerTwoCard) {
					if (i._Node == _CurrectNodePlayerTwo) {
						_PlayerTwoCard.Remove (i);
						GetComponent<CardManager> ()._Report (i, "- is given to Player 1");
						_PlayerOneCard.Add (i);
						GetComponent<CardManager>()._ReportTxt.text = "Share solution";
                        //GetComponent<TutorialScript>()._ShowPopUp();
                         break;
					}
				}
			}

		if (GetComponent<Tutorial_Section02>()._Section02)
        {
			if (GetComponent<Tutorial_Section02> ()._Step == 12) {
				
				GetComponent<CardManager> ()._ShowCardOnHand ();
				GetComponent<Tutorial_Section02> ()._HideTu [8].SetActive (false);
			}
        }

		_isFromShares = true;


        _CheckCard();
		_CheckCure();
		_CheckPlayerCard();
		_CheckIfShares();
		_DecreseAction();

        }

	bool _isGive;
    public void _SharedCard_PlayerOneSide(int i){
        if(_State==1){ // Give
            _PlayerTwoCard.Add(_PlayerOneCard[i]);
            _PlayerOneCard.RemoveAt(i);
        }else{ // Take
            _PlayerTwoCard.Add(_PlayerOneCard[i]);
            _PlayerOneCard.RemoveAt(i);
        }

        _SharesPopUp.SetActive(false);

		_CheckCard();
		_CheckCure();
		_CheckPlayerCard();
		_CheckIfShares();
        _DecreseAction();
    }

    public void _SharedCard_PlayerTwoSide(int i)
    {
        if (_State == 1)
        { // Give
            _PlayerOneCard.Add(_PlayerTwoCard[i]);
            _PlayerTwoCard.RemoveAt(i);
        }
        else
        { // Take
            _PlayerOneCard.Add(_PlayerTwoCard[i]);
            _PlayerTwoCard.RemoveAt(i);
        }

        _SharesPopUp.SetActive(false);

		_CheckCard();
		_CheckCure();
		_CheckPlayerCard();
		_CheckIfShares();
        _DecreseAction();
    }


	public Text _Infection01OnBar;
	public Text _Infection02OnBar;

	public Text _BuildingOnBar;

	public void _DeInfection(int _type,int _Count){
		if (_type==0) {
			_InfectionType01-=_Count;
			_Infection01OnBar.text = _InfectionType01+"";
		} else {
			_InfectionType02-=_Count;
			_Infection02OnBar.text = _InfectionType02+"";
		}

		if (_InfectionType01 <= 0 || _InfectionType02 <= 0) {
			GetComponent<CardManager> ()._EndGame (false, "By Out of Cubes");
		}
	}

	public void _InInfection(int _type,int _Count){

		if (_type==0) {
			_InfectionType01+=_Count;
			_Infection01OnBar.text = _InfectionType01+"";
		} else {
			_InfectionType02+=_Count;
			_Infection02OnBar.text = _InfectionType02+"";
		}
	}
		
	public void _SaveAction(int _id){

		_ActionStore [_Action-1]._ActionId = _id;

		_ActionStore [_Action-1]._CardOnHandPlayerOne.Clear ();
		foreach(CardManager._Card _Card in _PlayerOneCard){
			_ActionStore [_Action-1]._CardOnHandPlayerOne.Add (_Card);
		}

		_ActionStore [_Action-1]._CardOnHandPlayerTwo.Clear ();
		foreach(CardManager._Card _Card in _PlayerTwoCard){
			_ActionStore [_Action-1]._CardOnHandPlayerTwo.Add (_Card);
		}

		_ActionStore [_Action-1]._PlayerDicardsPile.Clear ();
		foreach (CardManager._Card _Card in GetComponent<CardManager>()._PLayerDiscardplie) {
			_ActionStore [_Action-1]._PlayerDicardsPile.Add (_Card);
		}
			
		_ActionStore [_Action-1]._CurrentNodePlayer.Clear ();	
		_ActionStore [_Action-1]._CurrentNodePlayer.Add(_CurrectNodePlayerOne);
		_ActionStore [_Action-1]._CurrentNodePlayer.Add(_CurrectNodePlayerTwo);

		if (_id == 1) {

			if (_State == 1) {
				_ActionStore [_Action - 1]._InfectionLv = _CurrectNodePlayerOne.GetComponent<NodeScript> ()._Level;
				_ActionStore [_Action - 1]._InfectionType [0] = _CurrectNodePlayerOne.GetComponent<NodeScript> ()._InfectionTypePerCube [0];
				_ActionStore [_Action - 1]._InfectionType [1] = _CurrectNodePlayerOne.GetComponent<NodeScript> ()._InfectionTypePerCube [1];
				_ActionStore [_Action - 1]._InfectionType [2] = _CurrectNodePlayerOne.GetComponent<NodeScript> ()._InfectionTypePerCube [2];
			
			} else {
				_ActionStore [_Action - 1]._InfectionLv = _CurrectNodePlayerTwo.GetComponent<NodeScript> ()._Level;
				_ActionStore [_Action - 1]._InfectionType [0] = _CurrectNodePlayerTwo.GetComponent<NodeScript> ()._InfectionTypePerCube [0];
				_ActionStore [_Action - 1]._InfectionType [1] = _CurrectNodePlayerTwo.GetComponent<NodeScript> ()._InfectionTypePerCube [1];
				_ActionStore [_Action - 1]._InfectionType [2] = _CurrectNodePlayerTwo.GetComponent<NodeScript> ()._InfectionTypePerCube [2];
			}

			_ActionStore [_Action - 1]._OldInfectionCube [0] = _InfectionType01;
			_ActionStore [_Action - 1]._OldInfectionCube [1] = _InfectionType02;
		}

		if (_id == 2) {
			_ActionStore [_Action - 1]._OldCure [0] = _isCureType[0];
			_ActionStore [_Action - 1]._OldCure [1] = _isCureType[1];
		}

        if (_id == 4)
        {
            if (_State == 1)
            {
                _ActionStore[_Action - 1]._BuildNode[0] = _CurrectNodePlayerOne;
                
            }
            else
            {
                _ActionStore[_Action - 1]._BuildNode[0] = _CurrectNodePlayerTwo;
            }

            _ActionStore[_Action - 1]._BuildNode[1] = _ForMoveNode;

        }

	}

	public void _Undo(){

		_isFromShares = false;

		if (GetComponent<LogFilesScript> ()._CheckActionById.Count != 0) {
			GetComponent<LogFilesScript> ()._CheckActionById.RemoveAt (GetComponent<LogFilesScript> ()._CheckActionById.Count - 1);
		}

		if (_Action<=3) {

            foreach (CardManager._Card C in GetComponent<CardManager>()._AllCardStore)
            {
                if (!C._isEpidemics)
                {

                    C._Node.GetComponent<NodeScript>()._ResetCode();

                }
            }

            _PlayState = 0;
			_Action++;
			_ActionText.text = "Action : " + _Action + " Lefts";
			

			switch (_ActionStore [_Action - 1]._ActionId) {
			case 0:
                  
				_PlayerOne.transform.position = Vector3.MoveTowards (_PlayerOne.transform.position, _ActionStore [_Action - 1]._CurrentNodePlayer[0] .transform.position, 100f);
				_PlayerOne.transform.position = new Vector3 (_PlayerOne.transform.position.x - 0.1f, _PlayerOne.transform.position.y, _PlayerOne.transform.position.z + 0.1f);

				_PlayerTwo.transform.position = Vector3.MoveTowards (_PlayerTwo.transform.position, _ActionStore [_Action - 1]._CurrentNodePlayer[1].transform.position, 100f);
				_PlayerTwo.transform.position = new Vector3 (_PlayerTwo.transform.position.x + 0.1f, _PlayerTwo.transform.position.y, _PlayerTwo.transform.position.z + 0.1f);

				_CurrectNodePlayerOne=_ActionStore [_Action - 1]._CurrentNodePlayer[0];
				_CurrectNodePlayerTwo=_ActionStore [_Action - 1]._CurrentNodePlayer[1];


				break;
			case 1:
				if (_State == 1) {
					_ActionStore [_Action - 1]._CurrentNodePlayer [0].GetComponent<NodeScript> ()._Level = _ActionStore [_Action - 1]._InfectionLv;
					_ActionStore [_Action - 1]._CurrentNodePlayer [0].GetComponent<NodeScript> ()._InfectionTypePerCube [0] = _ActionStore [_Action - 1]._InfectionType [0];
					_ActionStore [_Action - 1]._CurrentNodePlayer [0].GetComponent<NodeScript> ()._InfectionTypePerCube [1] = _ActionStore [_Action - 1]._InfectionType [1];
					_ActionStore [_Action - 1]._CurrentNodePlayer [0].GetComponent<NodeScript> ()._InfectionTypePerCube [2] = _ActionStore [_Action - 1]._InfectionType [2];
					_ActionStore [_Action - 1]._CurrentNodePlayer [0].GetComponent<NodeScript> ()._InfectionOnly ();

				} else {
					_ActionStore [_Action - 1]._CurrentNodePlayer [1].GetComponent<NodeScript> ()._Level = _ActionStore [_Action - 1]._InfectionLv;
					_ActionStore [_Action - 1]._CurrentNodePlayer [1].GetComponent<NodeScript> ()._InfectionTypePerCube [0] = _ActionStore [_Action - 1]._InfectionType [0];
					_ActionStore [_Action - 1]._CurrentNodePlayer [1].GetComponent<NodeScript> ()._InfectionTypePerCube [1] = _ActionStore [_Action - 1]._InfectionType [1];
					_ActionStore [_Action - 1]._CurrentNodePlayer [1].GetComponent<NodeScript> ()._InfectionTypePerCube [2] = _ActionStore [_Action - 1]._InfectionType [2];
					_ActionStore [_Action - 1]._CurrentNodePlayer [1].GetComponent<NodeScript> ()._InfectionOnly ();
				}
					
					_InfectionType01 = _ActionStore [_Action - 1]._OldInfectionCube [0];
					_Infection01OnBar.text = _InfectionType01+"";
	
					_InfectionType02 = _ActionStore [_Action - 1]._OldInfectionCube [1];
					_Infection02OnBar.text = _InfectionType02+"";

				break;

			case 2:
				_isCureType [0] = _ActionStore [_Action - 1]._OldCure [0];
				_isCureType [1] = _ActionStore [_Action - 1]._OldCure [1];

				if (_isCureType [1]) {
					_CuredText [1].text = "1/1";
				} else {
					_CuredText [1].text = "0/1";
				}

				if (_isCureType [0]) {
					_CuredText [0].text = "1/1";
				} else {
					_CuredText [0].text = "0/1";
				}

				foreach(GameObject i in GetComponent<CardManager>()._NodeManger.GetComponent<NodeManager>()._Node)
				{
					i.GetComponent<NodeScript> ()._UndoNode ();
				}



				break;

			case 3:
				if (_State == 1) {
					_ActionStore [_Action - 1]._CurrentNodePlayer [0].GetComponent<NodeScript> ()._UnBuild ();
					foreach (GameObject Card in _ResearchNode) {
						if (Card == _ActionStore [_Action - 1]._CurrentNodePlayer [0]) {
							_ResearchNode.Remove (Card);
							break;
						}
					}

				} else {
					_ActionStore [_Action - 1]._CurrentNodePlayer [1].GetComponent<NodeScript> ()._UnBuild ();
					foreach (GameObject Card in _ResearchNode) {
						if (Card == _ActionStore [_Action - 1]._CurrentNodePlayer [1]) {
							_ResearchNode.Remove (Card);
							break;
						}
					}
				}

				_Building++;
				_BuildingOnBar.text = _Building+"";


				break;

                case 4:

                        _ActionStore[_Action - 1]._BuildNode[1].GetComponent<NodeScript>()._UnBuild();
                        foreach (GameObject Card in _ResearchNode)
                        {
                            if (Card == _ActionStore[_Action - 1]._BuildNode[1])
                            {
                                _ResearchNode.Remove(Card);
                                break;
                            }
                        }
                        _ActionStore[_Action - 1]._BuildNode[0].GetComponent<NodeScript>()._Build();

                        break;
            }

			GetComponent<CardManager> ()._PLayerDiscardplie.Clear ();

			foreach (CardManager._Card _Card in _ActionStore [_Action-1]._PlayerDicardsPile) {
				GetComponent<CardManager> ()._PLayerDiscardplie.Add (_Card);
			}
			_PlayerOneCard.Clear ();
			foreach (CardManager._Card _Card in _ActionStore [_Action-1]._CardOnHandPlayerOne) {
				_PlayerOneCard.Add (_Card);
			}
			_PlayerTwoCard.Clear ();
			foreach (CardManager._Card _Card in _ActionStore [_Action-1]._CardOnHandPlayerTwo) {
				_PlayerTwoCard.Add (_Card);
			}

			//_ActionStore.RemoveAt (_ActionStore.Count - 1);

			_CheckCard ();
			_CheckCure ();
			_CheckPlayerCard ();
			_CheckIfShares ();
            _CheckIsCanMove();

        }
	}

	public List<_ActionData> _ActionStore = new List<_ActionData>();
	[System.Serializable]
	public class _ActionData{
		public List<GameObject> _CurrentNodePlayer;
		public List<CardManager._Card> _CardOnHandPlayerOne = new List<CardManager._Card>();
		public List<CardManager._Card> _CardOnHandPlayerTwo = new List<CardManager._Card>();
		public List<CardManager._Card> _PlayerDicardsPile = new List<CardManager._Card>();
		public int _InfectionLv;
		public  List<int> _InfectionType;
		public List<int> _OldInfectionCube;
		public List<bool> _OldCure;
		public int _ActionId;
        public List<GameObject> _BuildNode = new List<GameObject>();
    }

    int _PlayerSelectedCount;


    public Text[] _PlayerText;
    public void _SelectedClass_P1(int i)
    {
        _PlayerOneClass = i;
        _CheckClass();
    }

    public void _SelectedClass_P2(int i)
    {
        _PlayerTwoClass = i;
        _CheckClass();
    }


    public Button _StartBtm;
    public void _CheckClass()
    {
        if (_PlayerTwoClass == _PlayerOneClass)
        {
            _StartBtm.interactable = false;
        }
        else
        {
            _StartBtm.interactable = true;
        }
    }

    public bool _isCanMove;
    public void _CheckIsCanMove()
    {
        if (_State == 1 && _PlayerOneClass == 2)
        {
            if (_CurrectNodePlayerOne.GetComponent<NodeScript>()._isBuild)
            {
                if (_Action < 0)
                {
                    _isCanMove = false;
                }
                else
                {
                    _isCanMove = true;
                }
              
            }
            else
            {
                _isCanMove = false;
            }
        }
        else if(_State== 1&& _PlayerOneClass==1)
        {
            if (_Action < 0)
            {
                _isCanMove = false;
            }
            else
            {
                _isCanMove = true;
            }
        }

        if (_State == 0 && _PlayerTwoClass == 2)
        {
            if (_CurrectNodePlayerTwo.GetComponent<NodeScript>()._isBuild)
            {
                if (_Action < 0)
                {
                    _isCanMove = false;
                }
                else
                {
                    _isCanMove = true;
                }

            }
            else
            {
                _isCanMove = false;
            }
        }
        else if (_State == 0 && _PlayerTwoClass == 1)
        {
            if (_Action < 0)
            {
                _isCanMove = false;
            }
            else
            {
                _isCanMove = true;
            }
        }
    }

    public void _MoveBuilding()
    {
        if (_State == 1)
        {
			if (_PlayerOneClass == 2) {
				foreach (GameObject _Node in GetComponent<CardManager>()._NodeManger.GetComponent<NodeManager>()._Node) {
					if (!_Node.GetComponent<NodeScript> ()._isBuild) {
						_Node.GetComponent<SpriteRenderer> ().color = Color.magenta;
					}
				}
			} else if (_PlayerOneClass == 1) {
				_Dispatcher_UseAbility ();
			} else if (_PlayerOneClass == 0) {
				//_TreatNode ();
			}
            
        }
        else
        {
            if (_PlayerTwoClass == 2)
            {
                foreach (GameObject _Node in GetComponent<CardManager>()._NodeManger.GetComponent<NodeManager>()._Node)
                {
                    if (!_Node.GetComponent<NodeScript>()._isBuild)
                    {
                        _Node.GetComponent<SpriteRenderer>().color = Color.magenta;
                    }
                }
            }
            else if (_PlayerTwoClass == 1)
            {
                _Dispatcher_UseAbility();
			}else if (_PlayerOneClass == 0) {
				//_TreatNode ();
			}
        }
            
    }

    public GameObject _ForMoveNode;
    public void _ConfirmMove(GameObject _Node)
    {
        _ForMoveNode = _Node;
        _SaveAction(0);
		GetComponent<LogFilesScript> ()._CheckActionById.Add (5);
        _MoreCardPopUp.SetActive(true);

        if (_State == 1)
        {
            for (int i = 0; i <= _CardOnPopUp.Length - 1; i++)
            {
                if (i < _PlayerOneCard.Count)
                {
                    _CardOnPopUp[i].SetActive(true);
                    _CardOnPopUp[i].GetComponent<Image>().color = _PlayerOneCard[i]._Node.GetComponent<NodeScript>()._NodeColor[_PlayerOneCard[i]._Node.GetComponent<NodeScript>()._Type];
                    _CardOnPopUp[i].transform.GetChild(0).gameObject.GetComponent<Text>().text = _PlayerOneCard[i]._CardName;
                    _CardOnPopUp[i].GetComponent<SwapColour_ChangeImage>()._GetImage();
                }
                else
                {
                    _CardOnPopUp[i].SetActive(false);
                }
            }
        }
        else
        {

            for (int i = 0; i <= _CardOnPopUp.Length - 1; i++)
            {
                if (i < _PlayerTwoCard.Count)
                {
                    _CardOnPopUp[i].SetActive(true);
                    _CardOnPopUp[i].GetComponent<Image>().color = _PlayerTwoCard[i]._Node.GetComponent<NodeScript>()._NodeColor[_PlayerTwoCard[i]._Node.GetComponent<NodeScript>()._Type];
                    _CardOnPopUp[i].transform.GetChild(0).gameObject.GetComponent<Text>().text = _PlayerTwoCard[i]._CardName;
                    _CardOnPopUp[i].GetComponent<SwapColour_ChangeImage>()._GetImage();
                }
                else
                {
                    _CardOnPopUp[i].SetActive(false);
                }
            }
        }

        
        

    }

    public void _MoveBuilding_Process()
    {
		//_ForMoveNode
		if (_State == 1)
		{
			_PlayerOne.transform.position = Vector3.MoveTowards(_PlayerOne.transform.position, _ForMoveNode.transform.position, 100f);
			_PlayerOne.transform.position = new Vector3 (_PlayerOne.transform.position.x - 0.1f, _PlayerOne.transform.position.y, _PlayerOne.transform.position.z+0.1f);
			_CurrectNodePlayerOne = _ForMoveNode;
		}
		else
		{
			_PlayerTwo.transform.position = Vector3.MoveTowards(_PlayerTwo.transform.position, _ForMoveNode.transform.position, 100f);
			_PlayerTwo.transform.position = new Vector3 (_PlayerTwo.transform.position.x + 0.1f, _PlayerTwo.transform.position.y, _PlayerTwo.transform.position.z+0.1f);
			_CurrectNodePlayerTwo = _ForMoveNode;
		}

         _isCanMove = false;

         _DecreseAction();

        _CancelAbility();
		_CheckIsCanMove ();
    }


    public bool _isDispatcher_Ability;
    public void _Dispatcher_UseAbility()
    {

        foreach (CardManager._Card C in GetComponent<CardManager>()._AllCardStore)
        {
            if (!C._isEpidemics)
            {

                C._Node.GetComponent<NodeScript>()._ResetCode();

            }
        }

        _isDispatcher_Ability = true;

        if (_State == 1) // Player 1
        {

			if (_CurrectNodePlayerOne != _CurrectNodePlayerTwo) {
				_CurrectNodePlayerTwo.GetComponent<SpriteRenderer> ().color = Color.yellow;
				_CurrectNodePlayerOne.GetComponent<SpriteRenderer> ().color = Color.yellow;
			}

             foreach (GameObject i in _CurrectNodePlayerTwo.GetComponent<NodeScript>()._ConnectNode)
             {
                 i.GetComponent<SpriteRenderer>().color = Color.yellow;
             }

            foreach (CardManager._Card x in _PlayerOneCard)
            {
                if (x._Node == _CurrectNodePlayerOne)
                {
                    foreach (CardManager._Card C in GetComponent<CardManager>()._AllCardStore)
                    {
                        if (!C._isEpidemics)
                        {
                            if (C._Node.GetComponent<SpriteRenderer>().color != Color.yellow)
                            {
                                if (C._Node != _CurrectNodePlayerOne)
                                {
                                    C._Node.GetComponent<SpriteRenderer>().color = Color.green;
                                }
                            }
                        }
                    }
                    break;
                }
            }

			if (_CurrectNodePlayerTwo.GetComponent<NodeScript>()._isBuild)
            {
                foreach (GameObject i in _ResearchNode)
                {
					if (i != _CurrectNodePlayerTwo)
                    {
                        i.GetComponent<SpriteRenderer>().color = Color.yellow;
                 	}
                }
            }




        }
		else // Player 2
        {
			if (_CurrectNodePlayerOne != _CurrectNodePlayerTwo) {
				_CurrectNodePlayerTwo.GetComponent<SpriteRenderer> ().color = Color.yellow;
				_CurrectNodePlayerOne.GetComponent<SpriteRenderer> ().color = Color.yellow;
			}


            foreach (GameObject i in _CurrectNodePlayerOne.GetComponent<NodeScript>()._ConnectNode)
            {
                i.GetComponent<SpriteRenderer>().color = Color.yellow;
            }

            foreach (CardManager._Card x in _PlayerTwoCard)
            {
                if (x._Node == _CurrectNodePlayerTwo)
                {
                    foreach (CardManager._Card C in GetComponent<CardManager>()._AllCardStore)
                    {
                        if (!C._isEpidemics)
                        {
                            if (C._Node.GetComponent<SpriteRenderer>().color != Color.yellow)
                            {
                                if (C._Node != _CurrectNodePlayerTwo)
                                {
                                    C._Node.GetComponent<SpriteRenderer>().color = Color.green;
                                }
                            }
                        }
                    }
                    break;
                }
            }

			if (_CurrectNodePlayerOne.GetComponent<NodeScript>()._isBuild)
            {
                foreach (GameObject i in _ResearchNode)
                {
					if (i != _CurrectNodePlayerOne)
                    {
                        i.GetComponent<SpriteRenderer>().color = Color.yellow;
                    }
                }
            }
        }
    }

    public void _MoveWithAbility(GameObject i)
    {

        _SaveAction(0);
		GetComponent<LogFilesScript> ()._CheckActionById.Add (5);

        if (_State == 1)
        {


            if (i.GetComponent<SpriteRenderer>().color == Color.green)
            {

                foreach (CardManager._Card C in _PlayerOneCard)
                {

                    if (C._Node == _CurrectNodePlayerOne)
                    {

                        _PlayerTwo.transform.position = Vector3.MoveTowards(_PlayerTwo.transform.position, i.transform.position, 100f);
                        _PlayerTwo.transform.position = new Vector3(_PlayerTwo.transform.position.x + 0.1f, _PlayerTwo.transform.position.y, _PlayerTwo.transform.position.z + 0.1f);
                        GetComponent<CardManager>()._PLayerDiscardplie.Add(C);
                        _PlayerOneCard.Remove(C);
                        break;
                    }

                }

                foreach (CardManager._Card C in GetComponent<CardManager>()._AllCardStore)
                {
                    if (!C._isEpidemics)
                    {

                        C._Node.GetComponent<NodeScript>()._ResetCode();

                    }
                }
                _CurrectNodePlayerTwo = i;
                _isCharter = false;
                // _CheckPlayerCard();
            }
            else if (_isDirectFilght)
            {
                _PlayerTwo.transform.position = Vector3.MoveTowards(_PlayerTwo.transform.position, i.transform.position, 100f);
                _PlayerTwo.transform.position = new Vector3(_PlayerTwo.transform.position.x + 0.1f, _PlayerTwo.transform.position.y, _PlayerTwo.transform.position.z + 0.1f);
                _CurrectNodePlayerTwo = i;
                foreach (CardManager._Card C in _PlayerOneCard)
                {

                    if (C._Node == _CurrectNodePlayerTwo)
                    {
                        GetComponent<CardManager>()._PLayerDiscardplie.Add(C);
                        _PlayerOneCard.Remove(C);
                        break;
                    }

                }

                _isDirectFilght = false;

            }
            else
            {
				if (i == _CurrectNodePlayerTwo) {
					_PlayerOne.transform.position = Vector3.MoveTowards (_PlayerOne.transform.position, i.transform.position, 100f);
					_PlayerOne.transform.position = new Vector3 (_PlayerOne.transform.position.x - 0.1f, _PlayerOne.transform.position.y, _PlayerOne.transform.position.z + 0.1f);
					foreach (GameObject x in _CurrectNodePlayerOne.GetComponent<NodeScript>()._ConnectNode) {
						x.GetComponent<NodeScript> ()._ResetCode ();
					}
					_CurrectNodePlayerOne = i;
					Debug.Log (i.name);

					foreach (CardManager._Card C in GetComponent<CardManager>()._AllCardStore) {
						if (!C._isEpidemics) {

							C._Node.GetComponent<NodeScript> ()._ResetCode ();

						}
					}
				} else {
					


					_PlayerTwo.transform.position = Vector3.MoveTowards (_PlayerTwo.transform.position, i.transform.position, 100f);
					_PlayerTwo.transform.position = new Vector3 (_PlayerTwo.transform.position.x + 0.1f, _PlayerTwo.transform.position.y, _PlayerTwo.transform.position.z + 0.1f);
					foreach (GameObject x in _CurrectNodePlayerOne.GetComponent<NodeScript>()._ConnectNode) {
						x.GetComponent<NodeScript> ()._ResetCode ();
					}
					_CurrectNodePlayerTwo = i;
					Debug.Log (i.name);

					foreach (CardManager._Card C in GetComponent<CardManager>()._AllCardStore) {
						if (!C._isEpidemics) {

							C._Node.GetComponent<NodeScript> ()._ResetCode ();

						}
					}
				}


            }
            //Toast.instance.ShowMessage ("Player 1 move to "+_CurrectNodePlayerTwo.GetComponent<NodeScript>().name,1);

            foreach (GameObject z in _ResearchNode)
            {
                z.GetComponent<NodeScript>()._ResetCode();
            }

            _DecreseAction();
            _isMoveMode = false;

        }
        else
        {



            if (i.GetComponent<SpriteRenderer>().color == Color.green)
            {
                foreach (CardManager._Card C in _PlayerTwoCard)
                {

                    if (C._Node == _CurrectNodePlayerTwo)
                    {
                        _PlayerOne.transform.position = Vector3.MoveTowards(_PlayerOne.transform.position, i.transform.position, 100f);
                        _PlayerOne.transform.position = new Vector3(_PlayerOne.transform.position.x - 0.1f, _PlayerOne.transform.position.y, _PlayerOne.transform.position.z + 0.1f);
                        GetComponent<CardManager>()._PLayerDiscardplie.Add(C);
                        _PlayerTwoCard.Remove(C);
                        break;
                    }

                }

                foreach (CardManager._Card C in GetComponent<CardManager>()._AllCardStore)
                {
                    if (!C._isEpidemics)
                    {

                        C._Node.GetComponent<NodeScript>()._ResetCode();

                    }
                }
                // _CheckPlayerCard();
                _CurrectNodePlayerOne = i;
                _isCharter = false;
            }
            else if (_isDirectFilght)
            {

                if (GetComponent<TutorialScript>()._isOnTutorial && GetComponent<TutorialScript>()._Step == 6)
                {
                    GetComponent<TutorialScript>()._ShowPopUp();
                }

                _PlayerOne.transform.position = Vector3.MoveTowards(_PlayerOne.transform.position, i.transform.position, 100f);
                _PlayerOne.transform.position = new Vector3(_PlayerOne.transform.position.x - 0.1f, _PlayerOne.transform.position.y, _PlayerOne.transform.position.z + 0.1f);
                _CurrectNodePlayerOne = i;
                foreach (CardManager._Card C in _PlayerTwoCard)
                {

                    if (C._Node == _CurrectNodePlayerOne)
                    {
                        GetComponent<CardManager>()._PLayerDiscardplie.Add(C);
                        _PlayerTwoCard.Remove(C);
                        break;
                    }

                }

                _isDirectFilght = false;

            }
            else
            {

                if (GetComponent<TutorialScript>()._isOnTutorial && GetComponent<TutorialScript>()._Step == 17)
                {
                    GetComponent<TutorialScript>()._Step++;
                }

                if (GetComponent<TutorialScript>()._isOnTutorial && GetComponent<TutorialScript>()._Step == 20)
                {
                    GetComponent<TutorialScript>()._Step++;
                }

				if (i == _CurrectNodePlayerOne) {
					_PlayerTwo.transform.position = Vector3.MoveTowards (_PlayerTwo.transform.position, i.transform.position, 100f);
					_PlayerTwo.transform.position = new Vector3 (_PlayerTwo.transform.position.x + 0.1f, _PlayerTwo.transform.position.y, _PlayerTwo.transform.position.z + 0.1f);
					foreach (GameObject x in _CurrectNodePlayerTwo.GetComponent<NodeScript>()._ConnectNode) {
						x.GetComponent<NodeScript> ()._ResetCode ();
					}
					_CurrectNodePlayerTwo = i;
					Debug.Log (i.name);

					foreach (CardManager._Card C in GetComponent<CardManager>()._AllCardStore) {
						if (!C._isEpidemics) {

							C._Node.GetComponent<NodeScript> ()._ResetCode ();

						}
					}
				} else {
					


					_PlayerOne.transform.position = Vector3.MoveTowards (_PlayerOne.transform.position, i.transform.position, 100f);
					_PlayerOne.transform.position = new Vector3 (_PlayerOne.transform.position.x - 0.1f, _PlayerOne.transform.position.y, _PlayerOne.transform.position.z + 0.1f);
					foreach (GameObject x in _CurrectNodePlayerTwo.GetComponent<NodeScript>()._ConnectNode) {
						x.GetComponent<NodeScript> ()._ResetCode ();
					}
					_CurrectNodePlayerOne = i;

					foreach (CardManager._Card C in GetComponent<CardManager>()._AllCardStore) {
						if (!C._isEpidemics) {

							C._Node.GetComponent<NodeScript> ()._ResetCode ();

						}
					}
				}
            }
            //Toast.instance.ShowMessage ("Player 2 move to "+_CurrectNodePlayerTwo.GetComponent<NodeScript>().name,1);


            foreach (GameObject z in _ResearchNode)
            {
                z.GetComponent<NodeScript>()._ResetCode();
            }

            _DecreseAction();
            _isMoveMode = false;
        }

        _isDispatcher_Ability = false;
        _CancelAbility();
    }

    public GameObject _BtmBlock;
    public void _CancelAbility()
    {

        _BtmBlock.SetActive(false);
        _isDispatcher_Ability = false;
        _isMoveMode = false;
        _isDirectFilght = false;
        _isCharter = false;

        foreach (CardManager._Card C in GetComponent<CardManager>()._AllCardStore)
        {
            if (!C._isEpidemics)
            {

                C._Node.GetComponent<NodeScript>()._ResetCode();

            }
        }
    }

}
