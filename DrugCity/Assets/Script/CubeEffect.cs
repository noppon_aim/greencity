﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CubeEffect : MonoBehaviour {

	// Update is called once per frame
	public GameObject _Obj;

	void FixedUpdate () {
		this.gameObject.transform.position = Vector3.MoveTowards (this.transform.position, _Obj.transform.position, 0.2f);
	}
}
