﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RedImage_SwapImage : MonoBehaviour
{
    public Image _UI;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    bool _isChange;
    void FixedUpdate()
    {
        if(GetComponent<Image>().color == Color.red)
        {
            _UI.enabled = true;
            GetComponent<Image>().color = Color.white;
            _isChange = true;
        }
        else
        {
            if (!_isChange)
            {
                _UI.enabled = false;
            }
            
        }
    }
}
